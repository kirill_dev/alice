<?php
if ( !class_exists('Search_Connections') ) {
	class Search_Connections
	{
		public function checkPretext( $query )
		{
			$pretexts = array('-','в', 'на', 'для', 'или', 'при', 'вы', 'к', 'и', 'его', 'их', 'не', 'из', 'от','как', 'с', 'что', 'о', 'у', 'какой', 'какие', 'какого', 'какую','кому', 'чем', 'кем', 'тем', 'ней', 'нем', 'кого', 'чего', 'об', 'без');
			if( in_array( $query, $pretexts ) ) {
				return false;
			} else {
				return true;
			}
		}

		public function setColl( $title, $stop = NULL )
		{
			global $wpdb;
			$table_config = $wpdb->prefix . "alice_filter_settings";
			$config = $wpdb->get_results( "SELECT `2word`,`3word`,`4word`,`cut_off_the_pretexts` FROM $table_config", ARRAY_A )[0];//получаем конфигурацию настроек
			$stop_keys = $wpdb->get_results( "SELECT `stop_keys` FROM $table_config", ARRAY_A )[0]['stop_keys'];
			$stop_pref = $wpdb->get_results( "SELECT `stop_pref` FROM $table_config", ARRAY_A )[0]['stop_pref'];

			if ( !empty( $stop_pref ) ) {
				$stop_pref = explode("\n", $stop_pref);
			}else{
				$stop_pref = array();
			}

			if ( !empty( $stop_keys ) ) {
				$stop_keys = explode("\n", $stop_keys);
			}else{
				$stop_keys = array();
			}

			// $config['2word']
			// $config['3word']
			// $config['4word']
			// $config['cut_off_the_pretexts']//отсечь предлоги
			
			// $config['direct_entry']//прямые вхождения
			// $config['end_selection']//с подбором окончания
			// $config['small_entry']//разбавленные вхождения(max 1 слово между )
			$title = html_entity_decode($title);
			$title = preg_replace('|[\s]+|s', ' ', $title); //убираем двойные проблелы
			
			$arrtitle = explode(" ", trim($title));
			//чистим от знаков препинания
			foreach ( $arrtitle as $key ) {
				// $key = preg_replace('/[^aA-zZаА-яЯёЁ0-9_\-]+/u', '', $key);
				$key = preg_replace("/(?![.=$'€%-])\p{P}/u", '', $key);
				// $key = preg_replace("/(?![.=$'€%])\p{P}/u", '', $key);
				if ( !empty($key) )
					$cleartitle[] = mb_strtolower($key);
			}
			
			$keywords_arr = array();
			
			if ( $config['2word'] == 'on' && $config['3word'] == 'off' &&  $config['4word'] == 'off' ) {
				if( $config['cut_off_the_pretexts'] == 'on' ) {
					for($i = 0; $i < count($cleartitle); $i++) {
						
						if( !empty( $cleartitle[$i+1] ) && $this->checkPretext( $cleartitle[$i+1] ) && $this->checkPretext( $cleartitle[$i] ) ){
							
							$keywords_arr[] = trim( $cleartitle[$i] . " " . $cleartitle[$i+1] );
						}
					}
				} else {
					for($i = 0; $i < count($cleartitle); $i++) {
						
						
						if(!empty($cleartitle[$i+1])){
							$keywords_arr[] = trim( $cleartitle[$i] . " " . $cleartitle[$i+1] );
						}
					}
				}

			}elseif ( $config['2word'] == 'off' && $config['3word'] == 'on' &&  $config['4word'] == 'off' ) {
				if($config['cut_off_the_pretexts'] == 'on'){
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i]) ){
							$keywords_arr[] = trim( $cleartitle[$i] . " " . $cleartitle[$i+1] . " " . $cleartitle[$i+2] );
						}
					}
				} else {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+2])){
							$keywords_arr[] = trim( $cleartitle[$i] . " " . $cleartitle[$i+1] . " " . $cleartitle[$i+2] );
						}
					}
				}
			}elseif ( $config['2word'] == 'off' && $config['3word'] == 'off' &&  $config['4word'] == 'on' ) {
				if($config['cut_off_the_pretexts'] == 'on') {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i] . " " . $cleartitle[$i+1] . " " . $cleartitle[$i+2] . " " . $cleartitle[$i+3]);
						}
					}
				} else {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+3])) {
							$keywords_arr[] = trim($cleartitle[$i] . " " . $cleartitle[$i+1] . " " . $cleartitle[$i+2] . " " . $cleartitle[$i+3]);
						}
					}
				}
			}elseif ( $config['2word'] == 'on' && $config['3word'] == 'on' &&  $config['4word'] == 'off' ) {
				if($config['cut_off_the_pretexts'] == 'on') {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+1]) && $this->checkPretext($cleartitle[$i+1]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]);
						}
						if(!empty($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]);
						}
					}
				} else {
					for($i = 0; $i < count($cleartitle); $i++) {
						if( !empty($cleartitle[$i+1]) ) {
							$keywords_arr[] = trim($cleartitle[$i] . " " . $cleartitle[$i+1]);
							// file_put_contents(__DIR__."/preg.txt", '1-loop - ' . var_export( $keywords_arr, true), FILE_APPEND);
						}
						if( !empty($cleartitle[$i+2]) ) {
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]);
							// file_put_contents(__DIR__."/preg.txt", '2-loop - ' . var_export( $keywords_arr, true), FILE_APPEND);
						}
					}
				}
			}elseif ( $config['2word'] == 'on' && $config['3word'] == 'off' &&  $config['4word'] == 'on' ) {
				if($config['cut_off_the_pretexts'] == 'on') {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+1]) && $this->checkPretext($cleartitle[$i+1]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]);
						}
						if(!empty($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i])){
						$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]." ".$cleartitle[$i+3]);
						}
					}
				} else {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+1])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]);
						}
						if(!empty($cleartitle[$i+3])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]." ".$cleartitle[$i+3]);
						}
					}
				}
			}elseif ( $config['2word'] == 'off' && $config['3word'] == 'on' &&  $config['4word'] == 'on' ) {
				if($config['cut_off_the_pretexts'] == 'on') {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]);
						}
						if(!empty($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]." ".$cleartitle[$i+3]);
						}
					}
				} else {
					for( $i = 0; $i < count($cleartitle); $i++ ) {
						if( !empty($cleartitle[$i+2]) ){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]);
						}
						if( !empty($cleartitle[$i+3]) ){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]." ".$cleartitle[$i+3]);
						}
					}
				}
			}elseif ( $config['2word'] == 'on' && $config['3word'] == 'on' &&  $config['4word'] == 'on' ) {
				if( $config['cut_off_the_pretexts'] == 'on' ) {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+1]) && $this->checkPretext($cleartitle[$i+1]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]);
						}
						if(!empty($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i+2]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]);
						}
						if(!empty($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i+3]) && $this->checkPretext($cleartitle[$i])){
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]." ".$cleartitle[$i+3]);
						}
					}
				} else {
					for($i = 0; $i < count($cleartitle); $i++) {
						if(!empty($cleartitle[$i+1])) {
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]);
						}
						if(!empty($cleartitle[$i+2])) {
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]);
						}
						if(!empty($cleartitle[$i+3])) {
							$keywords_arr[] = trim($cleartitle[$i]." ".$cleartitle[$i+1]." ".$cleartitle[$i+2]." ".$cleartitle[$i+3]);
						}
					}
				}
			}

			if ( !empty( $keywords_arr ) && isset( $stop_pref ) && !empty( $stop_pref ) ) {

				foreach ( $keywords_arr as $key => $keyword ) {
					$arrkeyword = explode(" ", trim($keyword) );
					
					foreach ($arrkeyword as $k) {
						$k = preg_replace('/[^aA-zZаА-яЯёЁ0-9_\-]+/u', '', $k);
						$cleartitle[] = mb_strtolower($k);
					}
					// file_put_contents(__DIR__."/log.txt", 'cleartitle 1 - ' . var_export( $cleartitle, true) . "\n", FILE_APPEND);
					
					$count = count( array_intersect($stop_pref, $cleartitle) );

					// file_put_contents(__DIR__."/log.txt", 'count - ' . var_export( $count, true) . "\n", FILE_APPEND);
					if ( $count === 0 ) {
						$keywords_arr_new[] = $keyword;
					}

				}

				// file_put_contents(__DIR__."/preg.txt", 'keywords_arr_new 1 - ' . var_export( $keywords_arr_new, true), FILE_APPEND);
			}
			
			if ( !isset($keywords_arr_new) || empty( $keywords_arr_new ) )
				$keywords_arr_new = $keywords_arr;

			// file_put_contents(__DIR__."/preg.txt", 'keywords_arr_new 2 - ' . var_export( $keywords_arr_new, true), FILE_APPEND);
	
			if( !empty( $keywords_arr_new ) && isset( $stop_keys ) && !empty( $stop_keys ) ) {

				$stoptitle = explode("\n", $stop_keys);

				foreach( $stoptitle as $stop ) {
					$stoplist[] = trim($stop);
				}

				$result = array_diff( $keywords_arr_new, $stoplist );
				
				return $result;
				
			}

			return $keywords_arr_new;
		}
	}
}