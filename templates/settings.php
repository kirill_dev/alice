<div class="page-wrap full-height">
	<?php $alice_access_token = get_option('alice_access_token'); ?>
	<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e('Соединение с сервисом', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span>
		</h1>
		<p>Если вы еще не создали <a target="_blank" href="//alice.2seo.pro/home">токен</a>, перейдите в <a target="_blank" href="//alice.2seo.pro">настройки соединения проекта <img src="<?php echo plugins_url('alice/img/new_window.png') ?>"></a></p>
	</div>
	<div class="alice_align-center">
		<!-- <form action="<?php //echo esc_url( admin_url('admin.php?page=wp-alice-token&noheader=true') ); ?>" method="POST"> -->
		<form action="<?php echo esc_url( admin_url('admin.php?page=wp-alice-token&noheader=true') ); ?>" method="POST">
			<p class="al-form-field">
				<label><strong>Вставьте токен доступа к проекту</strong></label>
			</p>
			<p>
				<label>
					<input type="text" name="alice-access-token" value="<?php echo $alice_access_token ?>">
				</label>
			</p>	
			<p>
				<label class="btn btn-warning save" for="alice-token-submit-button"><?php _e('Сохранить', 'alice') ?></label>
				<input style="display: none;" type="submit" id="alice-token-submit-button" name="alice-token-submit-button" value="">
			</p>
			<p class="al-btn-align-left">
			</p>
			<input type="hidden" name="action" value="contact_form">
			<?php wp_nonce_field('wp_nonce_alice-token-id', 'wp_nonce_alice-token'); ?>
		</form>

		<!-- <img style="max-width: 1000px;" src="<?php //echo plugins_url('alice/img/token.png') ?>"> -->
	</div>
</div>
<?php include_once 'footer.php' ?>