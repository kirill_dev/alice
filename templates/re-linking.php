<?php
	include_once 'class-alice-rest-api.php';

	global $wpdb;
	$r = WP_Alice::alice_get_instance();

	$table = $wpdb->prefix . "alice_connections_from_server";
	// $table_settings = $wpdb->prefix . "alice_filter_settings";

	$all_posts = $wpdb->get_results( "SELECT * FROM $table", ARRAY_A );

	// foreach ($all_posts as $key => $post) {
	// 	$new_posts[$post['output_article_id']][] = $post;
	// }

?>
<div class="page-wrap">

	<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e('Обработанные страницы', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span></h1>
		<p>
			Статус простановки ссылок вы можете просмотреть как в плагине, так и в <a href="https://alice.2seo.pro">сервисе <img src="http://ishtory.ru/wp-content/plugins/alice/img/new_window.png"></a>
		</p>
	</div>
	<div class="table-responsive">
		<div class="table-responsive-overlay"></div>
		<span class="spin-loader"></span>
		<table width="100%" class="alice-stat-table alice-stat-table-relink">
			<thead>
				<th class="success title" width="30%">Название текущей статьи</th>
				<th class="success" width="16%">Ключевое слово</th>
				<th class="success" width="30%">Статья назначения( адресат )</th>
				<th class="success" width="12%">Статус</th>
				<th class="success" width="12%">Состояние</th>
			</thead>
			<tbody>

				<?php foreach ( $all_posts as $key => $post ) : ?>
					<?php 
						$post_id = $post['output_article_id'];
						$post_type = get_post_type( $post_id );
						$title = get_the_title( $post_id );
						$url = get_permalink( $post_id );
						$term = get_term( $post_id );

						$destination_url = get_permalink( $post['input_article_id'] );
						$destination_title = get_the_title( $post['input_article_id'] );

						$status = (int)$post['status'];
						$state = (int)$post['processing'];

						if ( $status === 0 ) {
							// $message = '<span class="error">' . __( 'Непроставлен', 'alice' ) . '</span>';
							$message = '<span class="error" title="Непроставлен"><img src="'.plugins_url( '../img/not_sent.png', __FILE__ ).'"></span>';
						}elseif ( $status === 2 ){
							// $message = '<span class="success">' . __( 'Проставлен', 'alice' ) . '</span>';
							$message = '<span class="success" title="Проставлен"><img src="'.plugins_url( '../img/sent.png', __FILE__ ).'"></span>';
						}

						if ( $state === 0 ) {
							// $state_msg = '<span class="error">' . __( 'Неотправлен', 'alice' ) . '</span>';
							$state_msg = '<span class="error" title="Неотправлен"><img src="'.plugins_url( '../img/not_sent.png', __FILE__ ).'"></span>';
						}elseif ( $state === 1 ){
							// $state_msg = '<span class="success">' . __( 'Отправлен', 'alice' ) . '</span>';
							$state_msg = '<span class="success" title="Отправлен"><img src="'.plugins_url( '../img/sent.png', __FILE__ ).'"></span>';
						}

						if ( $term !== NULL ) {
						    
						    if ( isset( $term->taxonomy ) ) {
						        $post_type = $term->taxonomy;
						        $post_id = $term->term_id;
						        $url = get_term_link( $post_id );

						        if ( $post_type == 'post_tag' ) {
						            $url = get_tag_link( $post_id );
						        }
						    }
						    $title = $term->name;
						} 
						
					?>
					
					<?php if ( !empty( $title ) ) : ?>
						<?php //var_dump( $title ) ?>

						<tr data-id="<?php echo $post_id ?>">
							<td>
								<a target="_blank" href="<?php echo $url ?>">
									<?php echo wp_trim_words( $title, 7, ' ...' ); ?></td>
								</a>
							<td style="text-align: center;"><?php _e( $post['text'], 'alice' ) ?></td>
							<td style="text-align: center;">
								<a target="_blank" href="<?php echo $destination_url ?>">
									<?php echo wp_trim_words( $destination_title, 7, ' ...' ); ?></td>
								</a>
							</td>
							<td style="text-align: center;"><?php echo $message ?></td>
							<td style="text-align: center;"><?php echo $state_msg ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php include_once 'footer.php' ?>