<?php //echo '<pre>' ?>
<?php //var_dump( $general_classes ) ?>
<?php //echo '</pre>' ?>
<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e('Управление классами ссылок', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span></h1>
		<!-- <p>
			test content
		</p> -->
	</div>
<form action="<?php echo esc_url( admin_url('admin.php?page=wp-alice-import') ); ?>" method="POST"" class="find_links">
	<label class="btn btn-warning play" for="find_links_not_alice">
		<?php _e( 'Найти уже сужествующие ссылки', 'alice' ) ?>
	</label>
	<input style="display:none;" name="find_links" id="find_links_not_alice" type="submit" value="<?php _e( 'Найти уже сужествующие ссылки', 'alice' ) ?>">
</form>
<br>
<div class="page-wrap">

	<div class="table-responsive">
		<div class="table-responsive-overlay"></div>
		<span class="spin-loader"></span>
		
		<div class="progress-bar-parent-wrap">
			<div style="text-align: center; padding-bottom: 12px;">
				<img src="<?php echo plugins_url('alice/img/coffee.png') ?>">
			</div>
			<div class="progress-bar-wrap">
				<div class="custom-progress-bar"></div>
				<span class="progress-count"></span>
				<span><?php _e( 'Поиск связей и отправка в Alice сервис', 'alice') ?></span>
				<div style="text-align: center;">
					<?php _e( 'Осталось записей', 'alice') ?>		
					<i class="untreated_posts">0</i>
				</div>
			</div>
		</div>

		<table width="100%" class="alice-stat-table alice-stat-table-import">
			<thead>
				<th class="success" width="25%" style="position: relative;">Ссылка</th>
				<th class="success title" width="25%">Название статьи</th>
				<th class="success" width="25%">Классы</th>
				<th class="success" width="25%">ID</th>
			</thead>
			<thead class="fixed_head">
				<th class="success" width="25%" style="position: relative;">Ссылка</th>
				<th class="success title" width="25%">Название статьи</th>
				<th class="success" width="25%">Классы</th>
				<th class="success" width="25%">ID</th>
			</thead>
			<tbody>
			<?php foreach ( $links as $key => $link ) : ?>	
				<tr data-index="<?php echo $link['index_number'] ?>" data-post_id="<?php echo $link['post_id'] ?>" data-link_text="<?php echo $link['link_text'] ?>">
					<td><h2><a target="_blank" href="<?php echo $link['link_href'] ?>"><?php echo $link['link_text'] ?></a></h2></td> 
					<td><a target="_blank" href="<?php echo $link['post_url'] ?>"><?php echo $link['post_title'] ?></a></td>
					<td>
						<ol class="class_list_control">
							<?php if ( !empty($link['link_classes']) ) : ?>
								<?php foreach (unserialize( $link['link_classes'] ) as $key => $class) : ?>
									<li>
										<span>
											<?php echo $class[0] ?>
										</span> 
										<input value="<?php echo $class[0] ?>" data-index="<?php echo $link['index_number'] ?>" data-link_text="<?php echo $link['link_text'] ?>" data-post_id="<?php echo $link['post_id'] ?>" type="checkbox" name="on_off_class" class="working_class"<?php echo ($class[1] == 1) ? ' checked' : '' ?>>
									</li>
								<?php endforeach ?>
							<?php endif ?>
						</ol>
						<form class="add_class">
							<input type="text" name="class_name_to_add">
							<input class="btn btn-warning" type="submit" value="добавить класс к ссылке">
						</form>
					</td>
					<td><?php echo $link['link_id'] ?></td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>

	<div class="filters_for_class">
		<h3>Включить/отключить классы в ссылках глобально</h3>
		<div>
			<ul>
				<?php foreach ($general_classes as $key => $class) : ?>
					<li>
						<input type="checkbox" name="class_manage" value="<?php echo $class['class_name'] ?>"<?php echo ($class['class_status'] == 1) ? ' checked' : '' ?>> <span><?php echo $class['class_name'] ?></span>
						
						<!-- <form class="add_class global">
								<input type="text" name="class_name_to_add">
								<input class="btn btn-warning" type="submit" <?php //echo ($class['class_status'] == 1) ? '' : ' disabled' ?> value="добавить класс к ссылке глобально">
						</form> -->
					</li>
				<?php endforeach ?>
			</ul>	
		</div>
	</div>

	<div class="">
		<a href="#" class="class_name_to_add btn btn-warning">Проставить пользовательский класс всем ссылкам</a>
	</div>
</div>
<?php include_once 'footer.php' ?>