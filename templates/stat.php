<?php 
	global $wpdb;
	$table = $wpdb->prefix . "alice_page_stats";
	$table_settings = $wpdb->prefix . "alice_filter_settings";
	$min_distance_link = (int)$wpdb->get_results( "SELECT `min_distance_link` FROM $table_settings",ARRAY_A)[0]['min_distance_link'];
	
	$args = array(
		'type'         => 'post',
		'child_of'     => 0,
		'parent'       => '',
		'orderby'      => 'name',
		'order'        => 'ASC',
		'hide_empty'   => 1,
		'hierarchical' => 1,
		'exclude'      => '',
		'include'      => '',
		'number'       => 0,
		'taxonomy'     => 'category',
		'pad_counts'   => false,
	);

	$site_categories = get_categories( $args );

	$site_enabled_cat = $wpdb->get_results( "SELECT `categories_selected` FROM $table_settings", ARRAY_A)[0]['categories_selected'];

	if ( !empty($_POST) ) {
		if ( !isset( $_POST['alice_use_posts'] ) ) {
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_post` = 0");
		}else{
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_post` = 1");
		}

		if ( !isset( $_POST['alice_use_pages'] ) ) {
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_page` = 0");
		}else{
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_page` = 1");
		}

		if ( !isset( $_POST['alice_use_categories'] ) ) {
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_category` = 0");
		}else{
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_category` = 1");
		}

		if ( !isset( $_POST['alice_use_tags'] ) ) {
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_tag` = 0");
		}else{
			$wpdb->query("UPDATE $table_settings table_settings SET table_settings.`is_tag` = 1");
		}
	}

	$settings = $wpdb->get_results( "SELECT `is_post`,`is_page`,`is_category`,`is_tag` FROM $table_settings", ARRAY_A )[0];
	$categories_selected = $wpdb->get_results( "SELECT `categories_selected` FROM $table_settings", ARRAY_A )[0];

	$post_types = '';
	foreach ($settings as $key => $value) {
		if ($value == 1) {
			if ( $key == 'is_post' ) {
				$post_types .= "'post',";
			}
			if ( $key == 'is_page' ) {
				$post_types .= "'page',";
			}
			if ( $key == 'is_category' ) {
				$post_types .= "'category',";
			}
			if ( $key == 'is_tag' ) {
				$post_types .= "'post_tag',";
			}
		}
	}

	$post_types = trim($post_types, ",");
	$post_types = "(" . $post_types . ")";

	$all_posts = $wpdb->get_results( "SELECT `post_id`,`post_type`,`size_post`,`count_in_link`,`count_out_link`,`count_external`,`checkbox`,`is_save`,`state` FROM $table WHERE `post_type` IN $post_types", ARRAY_A );

	$checked_posts = $wpdb->get_results( "SELECT `post_id` FROM $table WHERE `checkbox` = 1 AND `post_type` IN $post_types", ARRAY_A );

	if (count($all_posts) == count($checked_posts)) {
		$check_state = ' checked';
		$class_check_all = ' class="selected_custom_check"';
	}else{
		$check_state = '';
		$class_check_all = '';
	}

	$anagrams = $wpdb->get_results( "SELECT `2word`, `3word`, `4word` FROM $table_settings", ARRAY_A )[0];

	if ( $anagrams['2word'] == 'off' && $anagrams['3word'] == 'off' && $anagrams['4word'] == 'off' ) {
		$setting_message = true;
		$form_class = ' not_send';
	}else{
		$setting_message = false;
		$form_class = ' send';
	}
?>
<div class="page-wrap stat-page">
	<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e('Выбор рубрик, записей, страниц для перелинковки и статистика', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span>
		</h1>
		<p><?php _e('Выберите статьи, для отправки в сервис, каторые могут участвовать в перелинковке', 'alice') ?></p>
	</div>
	
	<?php if ( empty( $site_enabled_cat ) || !is_array( unserialize($site_enabled_cat) ) ) : ?>
		<?php $site_enabled_cat = array() ?>
	<?php else : ?>
		<?php $site_enabled_cat = unserialize($site_enabled_cat); ?>
	<?php endif; ?>

	<div class="filters_wrap flex">
		<h3 class="open_choose"><?php _e('Отметить записи в рубриках', 'alice') ?></h3>
		<div data-name="select_cat" class="hidden select_list">
			<form class="settings_form_by_cat flex" method="POST" name="settings_form_by_cat">
				<?php $number = 1 ?>
				
				<?php foreach ( $site_categories as $key => $category ) : ?>
					
					<?php if ( $number === 1 || ( $number-1 )%5 == 0 ) : ?>
						<div class="columns">
					<?php endif; ?>

					<div>
						<label>
							<input <?php echo ( in_array($category->slug, $site_enabled_cat) ) ? 'checked' : '' ?> class="alice_use_cat" type="checkbox" name="<?php echo $category->slug ?>">
							<div class="cat_filter_checkbox"></div>
							<?php echo $category->name ?>
						</label>
					</div>

				<?php if ( $number === count( $site_categories ) || $number%5 === 0 ) : ?>
					</div>
				<?php endif; ?>

				<?php $number++ ?>
				<?php endforeach; ?>
			</form>
		</div>
		<h3 class="open_choose"><?php _e('Отобразить в таблице', 'alice') ?></h3>
		<div data-name="select_type" class="hidden select_list">
			<form class="settings_form flex" method="POST" style="height: 100%;">
				<div>
					<div>
						<div class="filter_wp_types flex">
							<div style="padding-right: 15px;">
								<div>
									<label>
										<input type="checkbox" <?php echo ($settings['is_post']) ? 'checked' : '' ?> id="alice_use_posts" name="alice_use_posts">
										<div class="cat_filter_checkbox"></div>
										<?php _e('Записи', 'alice') ?>
									</label>
								</div>
								<div>
									<label>
										<input type="checkbox" <?php echo ($settings['is_page']) ? 'checked' : '' ?> id="alice_use_pages" name="alice_use_pages">
										<div class="cat_filter_checkbox"></div>
										<?php _e('Страницы', 'alice') ?>
									</label>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</form>
		</div>	
	</div>

	<div class="table-responsive max_height">
		<div class="table-responsive-overlay"></div>
		<span class="spin-loader"></span>
		
		<div class="setting_message">
			<span class="close_setting_message">X</span>
			<div class="inner_setting_message">
				<?php _e('Добавте настройки в <a target="_blank" href="https://alice.2seo.pro/">сервисе</a>', 'alice') ?>
			</div>
		</div>

		<div class="progress-bar-parent-wrap">
			<div style="text-align: center; padding-bottom: 12px;">
				<img src="<?php echo plugins_url('alice/img/coffee.png') ?>">
			</div>
			<div class="progress-bar-wrap">
				<div class="custom-progress-bar"></div>
			<span><?php _e( 'Отправка записей в Alice сервис', 'alice') ?></span>
			</div>
		</div>

		<table width="100%" class="alice-stat-table alice-stat-table-stat">
			<thead class="not_fixed_head">
				<th width="3%" style="position: relative;">
					<label for="select_all_keywords"<?php echo $class_check_all ?>>
						<input id="select_all_keywords" type="checkbox" name=""<?php echo $check_state ?>>
					</label>
				</th> 
				<th class="success" width="3%"><?php _e('№', 'alice') ?></th>
				<th class="success title" width="16%"><?php _e('Название статьи', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Вид', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Рубрики', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во исходящих внешних ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во исходящих внутренних ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во входящих внутренних ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во символов', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Рекомендованное кол-во ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Дата публикации', 'alice') ?></th>
			</thead>
			<thead class="fixed_head">
				<th width="3%" style="position: relative;">
					<label for="select_all_keywords"<?php echo $class_check_all ?>>
						<input id="select_all_keywords" type="checkbox" name=""<?php echo $check_state ?>>
					</label>
				</th> 
				<th class="success" width="3%"><?php _e('№', 'alice') ?></th>
				<th class="success title" width="16%"><?php _e('Название статьи', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Вид', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Рубрики', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во исходящих внешних ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во исходящих внутренних ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во входящих внутренних ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Кол-во символов', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Рекомендованное кол-во ссылок', 'alice') ?></th>
				<th class="success" width="11%"><?php _e('Дата публикации', 'alice') ?></th>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				<?php foreach ( $all_posts as $key => $post ) : ?>
					<?php $permalink = get_permalink( $post['post_id'] ) ?>
					<?php if ( $permalink !== false ) : ?>
						<?php (  (int)$post['checkbox'] === 1 ) ? $class = ' class="custom-selected"' :  $class = '' ; ?>
						<?php
							$taxonomies = get_post_taxonomies( $post['post_id'] );

							if ( ($key = array_search('post_tag', $taxonomies) ) !== false ) {
							    unset( $taxonomies[$key] );
							}
							
							if ( ($key = array_search('post_format', $taxonomies) ) !== false ) {
							    unset( $taxonomies[$key] );
							}

							$terms = wp_get_post_terms( $post['post_id'], $taxonomies);
							$terms_name = [];   
							$terms_slug = [];   



							foreach ($terms as $key => $term) {
							    $terms_name[] = $term->name;
							    $terms_slug[] = '"' . $term->slug . '"';
							}

							$terms_name_str = implode(', ', $terms_name);
							$terms_slug_str = implode(', ', $terms_slug);
						?>
						<tr data-id="<?php echo $post['post_id'] ?>" data-post-category="<?php echo $post['post_type']; ?>" data-categories='[<?php echo $terms_slug_str ?>]'<?php echo $class ?>>
								<?php 
									$post_type = $post['post_type'];

									if ( $post_type == "post" ) {
										$post_name = 'Запись';
									}elseif ( $post_type == "page" ) {
										$post_name = 'Страница';

									}elseif( $post_type == "category" ) {
										$post_name = 'Категория';

									}elseif ( $post_type == "post_tag" ) {
										$post_name = 'Метка';
									}

									$title = get_the_title($post['post_id']);
									$url = get_permalink($post['post_id']);
								?>
								<td width="3%"></td> 
								<td width="3%" style="text-align: center;">
									<?php echo $i ?>	
								</td>
								
								<td width="16%">
									<a target="_blank" href="<?php echo $url ?>">
										<?php echo wp_trim_words( $title, 7, ' ...' ); ?>	
										<img src="<?php echo plugins_url('alice/img/new_window.png') ?>">
									</a>
								</td>
								
								<td width="11%" style="text-align: center;">
									<?php echo $post_name ?>	
								</td>

								<td width="11%" style="text-align: center;">
									<?php echo $terms_name_str ?>	
								</td>
					
								<td style="text-align: center;"><?php echo $post['count_external'] ?></td>
								<td style="text-align: center;"><?php echo $post['count_out_link'] ?></td>
								<td style="text-align: center;"><?php echo $post['count_in_link'] ?></td>
								<td style="text-align: center;"><?php echo $post['size_post'] ?></td>
								<td style="text-align: center;"><?php echo round($post['size_post']/$min_distance_link) ?></td>
								<?php $mydate = strtotime(get_the_date( 'Y/m/d', $post['post_id'])); ?>
								<td data-sort="<?php echo $mydate ?>" style="text-align: center;"><?php echo get_the_date( 'd/m/Y', $post['post_id']) ?></td>
								<?php $i++; ?>
						</tr>
					<?php endif; ?>	
				<?php endforeach; ?>
			</tbody>
		</table>
		<!-- <h2 style="display: none;">Настройки</h2> -->
		<form style="width: 100%;" class="send_posts_to_server<?php echo $form_class ?>" method="POST">
			<label class="btn btn-warning ok" for="send_posts_to_server_btn">
				<?php _e('Одобрить', 'alice') ?>
			</label>
				<input style="display: none;" id="send_posts_to_server_btn" type="submit" name="" value="Одобрить">
		</form>
	</div>

	
</div>
<?php include_once 'footer.php' ?>