<div class="page-wrap">
	<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e( 'Статистика кликов', 'alice' ); ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span>
		</h1>
	</div>

	<div class="table-responsive max_height">
		<div class="table-responsive-overlay"></div>
		<span class="spin-loader"></span>
		
		<div class="setting_message">
			<span class="close_setting_message">X</span>
			<div class="inner_setting_message">
				<?php _e('Добавте настройки в <a target="_blank" href="https://alice.2seo.pro/">сервисе</a>', 'alice') ?>
			</div>
		</div>

		<div class="progress-bar-parent-wrap">
			<div style="text-align: center; padding-bottom: 12px;">
				<img src="<?php echo plugins_url('alice/img/coffee.png') ?>">
			</div>
			<div class="progress-bar-wrap">
				<div class="custom-progress-bar"></div>
			<span><?php _e( 'Отправка записей в Alice сервис', 'alice') ?></span>
			</div>
		</div>

		<table width="100%" class="alice-stat-table alice-stat-table-click-stat">
			<thead>
				<th class="success" width="20%"><?php _e('Номер', 'alice') ?></th>
				<th class="success title" width="20%"><?php _e('Название Записи(где)', 'alice') ?></th>
				<th class="success" width="20%"><?php _e('Ключевое слово', 'alice') ?></th>
				<th class="success" width="20%"><?php _e('Запись на которую ссылается', 'alice') ?></th>
				<th class="success" width="20%"><?php _e('Количество кликов', 'alice') ?></th>
			</thead>
			<tbody>

				<?php foreach ( $clicks as $key => $click_object ) : ?>
					<?php
						$post_out['url'] = get_permalink( $click_object['post_id'] );
						$post_out['title'] =  get_the_title( $click_object['post_id'] );
						
						$id_post_in = url_to_postid( $click_object['url'] );
						
						$post_in['title'] = get_the_title( $id_post_in );
						$post_in['url'] = get_permalink( $id_post_in );
						$keyword = $click_object['keyword'];
						$count_clicks = $click_object['count_clicks'];
					?>
					<tr>
						<td width="5%" style="text-align: center;"><?php echo $key+1 ?></td> 
						<td width="27%">
							<a href="<?php echo $post_out['url'] ?>"><?php echo $post_out['title'] ?> <img src="http://ishtory.ru/wp-content/plugins/alice/img/new_window.png"></a>
						</td>
						<td width="20%" style="text-align: center;">
							<?php echo $keyword ?>
						</td>
						<td width="27%" style="text-align: center;">
							<a href="<?php echo $post_in['url'] ?>"><?php echo $post_in['title'] ?> <img src="http://ishtory.ru/wp-content/plugins/alice/img/new_window.png"></a>
						</td>
			
						<td width="20%" style="text-align: center;"><?php echo $count_clicks ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php include_once 'footer.php' ?>
<!-- wp-alice-clicks-stat -->