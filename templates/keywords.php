<?php
	global $wpdb;
	$table = $wpdb->prefix . "alice_page_stats";
	$table_settings = $wpdb->prefix . "alice_filter_settings";
	
	$settings = $wpdb->get_results( "SELECT `is_post`,`is_page`,`is_category`,`is_tag` FROM $table_settings", ARRAY_A )[0];

	$post_types = '';
	foreach ($settings as $key => $value) {
		if ($value == 1) {
			if ( $key == 'is_post' ) {
				$post_types .= "'post',";
			}
			if ( $key == 'is_page' ) {
				$post_types .= "'page',";
			}
			if ( $key == 'is_category' ) {
				$post_types .= "'category',";
			}
			if ( $key == 'is_tag' ) {
				$post_types .= "'post_tag',";
			}
		}
	}

	$post_types = trim($post_types, ",");
	$post_types = "(" . $post_types . ")";

	$all_posts = $wpdb->get_results( "SELECT `post_id`, `size_post`, `count_out_link`, `post_type`, `keys_from_server`,`custom_user_keywords`,`keys_from_admin`,`keys_from_step`, `checkbox`,`is_save`,`state` FROM $table WHERE `checkbox` = 1 AND `post_type` IN {$post_types}", ARRAY_A );
	$table_kpi = $wpdb->prefix . "kpi_seo_plugin";
	$table_exists = $wpdb->get_results("SHOW TABLES LIKE '".$table_kpi."'", ARRAY_A);

	if ( !empty( $table_exists ) ) {
		$kpi_seo = true;
	}else{
		$kpi_seo = false;
	}

	$checked_posts = $wpdb->get_results( "SELECT `post_id` FROM $table WHERE `is_save` = 1 AND `post_type` IN $post_types", ARRAY_A );
	
	if (count($all_posts) == count($checked_posts)) {
		$check_state = ' checked';
		$class_check_all = ' class="selected_custom_check"';
	}else{
		$check_state = '';
		$class_check_all = '';
	}

	// echo '<pre>'; var_dump($all_posts); echo '</pre>';
?>

<div class="page-wrap">
	<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e('Утверждение ключевых слов, которые будут использоваться в перелинковке', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span>
		</h1>
		<p><?php _e('Выберите статьи, которые будут участвовать в перелинковке, а также их ключевые слова, по которым будет идти поиск этих статей.', 'alice') ?></p>
	</div>

	<?php if ( !$kpi_seo ) : ?>
		<p class="kpiseo_info"><?php _e('<a target="_blank" href="http://kpiseo.beewise.ru/">Установите плагин KPI Seo</a> для улучшения качества работы со статьями', 'alice') ?></p>
	<?php endif; ?>

	<div class="table-responsive max_height keywords_table">
		<div class="table-responsive-overlay"></div>
		<span class="spin-loader"></span>
		
		<div class="progress-bar-parent-wrap">
			<div style="text-align: center; padding-bottom: 12px;">
				<img src="<?php echo plugins_url('alice/img/coffee.png') ?>">
			</div>
			<div class="progress-bar-wrap">
				<div class="custom-progress-bar"></div>
				<span class="progress-count"></span>
				<span><?php _e( 'Поиск связей и отправка в Alice сервис', 'alice') ?></span>
				<div style="text-align: center;">
					<?php _e( 'Осталось записей', 'alice') ?>		
					<i class="untreated_posts">0</i>
				</div>
			</div>
		</div>

		<table width="100%" class="alice-stat-table alice-stat-table-key">
			<!-- alice-stat-table-key -->
			<thead>
				<th class="success" width="5%" style="position: relative;">
					<!-- <label for="select_all_keywords"></label><input id="select_all_keywords" type="checkbox" name=""> -->
					<label for="select_all_keywords"<?php echo $class_check_all ?>>
						<input id="select_all_keywords" type="checkbox" name=""<?php echo $check_state ?>>
					</label>
				</th>
				<th class="success title" width="45%">
					<span>Название статьи</span>
					<label title="Раскрыть все пункты" for="open_all" class="open_all"><img src="<?php echo plugins_url('alice/img/open_all.png') ?>"></label>
					<input style="display: none;" type="checkbox" id="open_all" name="">
				</th>
				<th class="success" width="10%">Кол-во исходящих ссылок</th>
				<th class="success" width="10%">Рекомендуемое кол-во</th>
				<th class="success" width="10%">Кол-во ключей</th>
				<th class="success" width="10%">Статус</th>

				<?php if ( $kpi_seo ) : ?>
					<th class="success" width="10%">KPI</th>
				<?php endif; ?>

			</thead>
			<thead class="fixed_head">
				<th class="success" width="5%" style="position: relative;">
					<!-- <label for="select_all_keywords"></label><input id="select_all_keywords" type="checkbox" name=""> -->
					<label for="select_all_keywords"<?php echo $class_check_all ?>>
						<input id="select_all_keywords" type="checkbox" name=""<?php echo $check_state ?>>
					</label>
				</th>
				<th class="success title" width="45%">
					<span>Название статьи</span>
					<label title="Раскрыть все пункты" for="open_all" class="open_all"><img src="<?php echo plugins_url('alice/img/open_all.png') ?>"></label>
					<input style="display: none;" type="checkbox" id="open_all" name="">
				</th>
				<th class="success" width="10%">Кол-во исходящих ссылок</th>
				<th class="success" width="10%">Рекомендуемое кол-во</th>
				<th class="success" width="10%">Кол-во ключей</th>
				<th class="success" width="10%">Статус</th>

				<?php if ( $kpi_seo ) : ?>
					<th class="success" width="10%">KPI</th>
				<?php endif; ?>

			</thead>
			<tbody>
				<?php $i = 1; ?>
				<?php $all_keywords = []; ?>
				<?php $temp_arr = []; ?>
				<?php $duplicates = []; ?>
				<?php $duplicates_posts = []; ?>

				<?php 

					foreach ( $all_posts as $key => $post ) :
						$post_id = $post['post_id'];
						$post_type = $post['post_type'];
						
						// // $keywords = $wpdb->get_results( "SELECT `keys_from_server`,`custom_user_keywords`,`keys_from_admin`,`keys_from_step` FROM $table WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'", ARRAY_A )[0];

						$keys_from_server = !empty( $post['keys_from_server'] ) ? unserialize($post['keys_from_server']) : array();
						$keys_from_admin = !empty( $post['keys_from_admin'] ) ? unserialize($post['keys_from_admin']) : array();
						$keys_from_step = !empty( $post['keys_from_step'] ) ? unserialize($post['keys_from_step']) : array();
						$custom_user_keywords = !empty( $post['custom_user_keywords'] ) ? unserialize($post['custom_user_keywords']) : array();

						// // $keywords = !empty( $keywords ) ? unserialize( $keywords ) : array();
						$keywords = array_merge( $keys_from_server, $keys_from_admin, $keys_from_step, $custom_user_keywords );

						$new_keywords = [];

						foreach ( $keywords as $k => $keyword ) {
							$all_keywords[] = $keyword;
						}


					endforeach;

					$all_keywords_values = array_count_values($all_keywords);
					foreach ($all_keywords_values as $keyword => $value) {
						if ( $value > 1 )
							$duplicates[] = $keyword;
					}

				?>

				<?php $index = 0; ?>
				<?php $arr_temp = []; ?>
				<?php $keywords = []; ?>

				<?php foreach ( $all_posts as $key => $post ) : ?>
					<?php
						$open = '';
						if ( $key == 0 )
							$open = ' open';

						$post_id = $post['post_id'];
						$post_type = $post['post_type'];

						if ( $post_type == 'post' || $post_type == 'page' ) {

							$title = get_the_title( $post_id );
							$url = get_permalink( $post_id );

						}

						$keywords['keys_from_server'] = $keys_from_server = !empty( $post['keys_from_server']) ? unserialize($post['keys_from_server']) : array();
						$keywords['keys_from_admin'] = $keys_from_admin = !empty( $post['keys_from_admin']) ? unserialize($post['keys_from_admin']) : array();
						$keywords['keys_from_step'] = $keys_from_step = !empty( $post['keys_from_step']) ? unserialize($post['keys_from_step']) : array();
						$keywords['custom_user_keywords'] = $custom_user_keywords = !empty( $post['custom_user_keywords']) ? unserialize($post['custom_user_keywords']) : array();

						$all_keywords = array_merge( $keywords['keys_from_server'], $keywords['keys_from_admin'], $keywords['keys_from_step'], $keywords['custom_user_keywords'] );

						$size_post = $post['size_post'];
						$status = (int)$post['is_save'];
						
						if ( $status === 0 ) {
							// $message = __( 'Не отправлен', 'alice' );
							$message = '<img src="'.plugins_url( '../img/not_sent.png', __FILE__ ).'">';

						}elseif ( $status === 1 ) {
							$message = '<img src="'.plugins_url( '../img/sent.png', __FILE__ ).'">';
							// $message = __( 'Отправлен', 'alice' );
						}else{
							$message = __( 'Ошибка', 'alice' );
						}

						$new_keywords = [];
						$not_duplicates = [];

						foreach ($keywords as $key => $keywords_arr) {
							$not_duplicates[$key] = array_diff($keywords_arr, $duplicates);
							$this_duplicates[$key] = array_diff($keywords_arr, $not_duplicates[$key]);
						}

						foreach ($not_duplicates as $cat => $keywords_arr) {
							foreach ($keywords_arr as $key => $value) {

								$tag = '<span class="'.$cat.' keyword_in_modal_close"><span class="edit_elem" contenteditable="true">' . $value . '</span><i class="close_icon"><img src="' . plugins_url( '../img/remove_keyword.png', __FILE__ ) . '"></i></span>';
								$new_keywords[] = $tag;

							}
						}

						foreach ($this_duplicates as $cat => $keywords_arr) {
							foreach ($keywords_arr as $key => $value) {
								$class = !in_array($value, $arr_temp) ? ' first' : '';	
								
								$arr_temp[] = $value;
								$tag = '<span class="'.$cat.' keyword_in_modal_close red'.$class.'"><span class="edit_elem" contenteditable="true">' . $value . '</span><i class="close_icon"><img src="' . plugins_url( '../img/remove_repeat.png', __FILE__ ) . '"></i></span>';
								$new_keywords[] = $tag;
							}
						}

					?>
					
					<?php ( (int)$post['is_save'] === 1 ) ? $class = ' class="custom-selected"' :  $class = '' ; ?>
					
					<tr data-post-category="<?php echo $post_type; ?>" data-id="<?php echo $post['post_id'] ?>"<?php echo $class ?>>
						<td></td>
						<td class="show-keywords<?php echo $open ?>">
							<span class="keywords-title"><?php echo wp_trim_words( $title, 7, ' ...' ); ?></span><a class="keyword_title_link" target="_blank" href="<?php echo $url ?>" title="открытие ссылки в новом окне"><img src="<?php echo plugins_url('alice/img/new_window.png') ?>"></a>

							<div class="alice-modal-wrap<?php //echo ( $index === 0 ) ? ' open-first' : '' ?>">
								<?php $index++; ?>
								<div class="alice-modal-main"><?php echo implode('', $new_keywords); ?><span class="add_keyword"><span></span></span></div>
								<!-- <span class="alice-modal-btn close">закрыть</span> -->
								<button class="alice-modal-btn submit-btn"><?php _e('Утвердить', 'alice') ?></button>
							</div>
						</td>
						<td style="text-align: center;">0</td>
						<td style="text-align: center;"><?php echo round($size_post/1500) ?></td>
						<td style="text-align: center;"><?php echo count($new_keywords) ?></td>

						<td style="text-align: center;"><?php echo $message ?></td>
						
						<?php if ( $kpi_seo ) : ?>
							<?php $kpi = $wpdb->get_results("SELECT kpi_seo FROM $table_kpi WHERE `post_id` = $post_id", ARRAY_A)[0]['kpi_seo'] ?>
							<?php if (!isset($kpi)) $kpi = 0 ?>
							<td style="text-align: center;"><?php echo $kpi ?></td>
						<?php endif; ?>
					</tr>
				<?php $i++; ?>
					

				<?php endforeach; ?>
			</tbody>
		</table>
		<form class="keyword-form">
			<label for="keyword-form-submit" class="btn btn-warning alice"><?php _e('Найти связи и отправить на сервер', 'alice') ?></label>
			<input style="display: none;" type="submit" id="keyword-form-submit" name="keyword-form-submit" value="">
		</form>

		<form class="keyword-form-continue">
			<label for="keyword-form-submit-continue" class="btn btn-warning alice"><?php _e('Продолжить прерванную обработку', 'alice') ?></label>
			<input style="display: none;" type="submit" id="keyword-form-submit-continue" name="keyword-form-submit-continue" value="">
		</form>
	</div>
</div>

<?php include_once 'footer.php' ?>