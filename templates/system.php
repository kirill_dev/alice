<div class="alert-ms">
	<span class="close-alert-ms">&#10006;</span>
	<?php _e( 'Ваша текущая версия PHP ' . phpversion() . '!!', 'alice') ?><br>
	<?php _e('Для корректной работы плагина требуется PHP не ниже 5.6!!', 'alice') ?><br>
	<?php _e('ОБНОВИТЕ версию PHP!', 'alice') ?>
</div>