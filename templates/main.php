<?php 
	global $wpdb;
	$table_menu = $wpdb->prefix . "alice_plugin_menu";
	$pages = $wpdb->get_results( "SELECT * FROM $table_menu", ARRAY_A );
	global $wp_version;
?>

<div class="alice-body">

	<div class="alice-main">
		<?php
			if (!defined('PHP_VERSION_ID')) {
			    $version = explode('.', PHP_VERSION);
			    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
			}

			if ( PHP_VERSION_ID < 50600 ) { ?>
				<?php include_once('system.php'); ?>
				
		<?php	} ?>

		<div class="alice-content-wrap page-wrap">

			<?php if ( isset($_GET['page']) && $_GET['page'] === 'wp-alice' ) : ?>
				<div>
					<div class="tab_title">
						<h1>Информация о перелинковке</h1>
						<p>Для полноценной работы плагина необходима версия wordpress 4.5 + и php версии не ниже 5.6. <span class="green_btn">У вас wordpress <?php echo $wp_version .', php '. PHP_VERSION ?></span> </p>
						<?php 
							if ( ( file_exists( WP_PLUGIN_DIR . "/clearfy/clearfy.php") || file_exists( WP_PLUGIN_DIR . "/clearfy-pro/admin/clearfy-admin.php") ) && (!empty(get_option('wbcr_clearfy_disable_json_rest_api')) || (get_option('clearfy_option')['disable_json_rest_api'] !== NULL && get_option('clearfy_option')['disable_json_rest_api'] == 'on') ) ) { ?>
								<div style="max-width: 1050px;">
									<p class="red_btn">
										Отключен JSON REST API!
										Для соеддинения с сервисом включите JSON REST API в плагине Clearfy!
									</p>
								</div>
							<?php
							}
						?>
					</div>
					<div class="tab_content">
						<?php $url = admin_url( 'admin.php?page=wp-alice-token' ); ?>
						<div class="plugin_desc">

							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_1.png') ?>">Плагин разработан так, что вся логика рабочего процесса происходит на стороне клиента. При этом нам удалось достичь плотной связки плагина и сервиса, таким образом, что они не могут работать друг без друга. За счет этого, нам удалось оставить открытым исходный код и максимально увеличить производительность процесса.
							</div>
							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_2.png') ?>">Плагин работает по принципу аналитики title и h1 страниц и их дифференциации на n-граммы, с последующим поиском n-грамм в рамках текста всего сайта с возможностью гибких настроек параметров. Также существует возможность загрузки ключевых слов для каждой отдельной статьи, чтоб совершать более эффективный поиск связей.</div> 
							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_3.png') ?>">При тестировании максимальная нагрузка на сервер была 20CP. Это никак не повлияло на остальную работу сайта и его процессов. Следуя нашей логике, если на сайте большое количество статей и высокая посещаемость, то и сервер у такого сайта должен быть достаточно мощный. <b>Если же вы не уверены в своем сервере, мы настоятельно рекомендуем производить процесс перелинковки в минимально нагруженное время.</b></div>
							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_4.png') ?>">Плагин не вносит никаких изменений в базу данных, до тех пор, пока в сервисе не будет выбран и запущен процесс будущей линковки. Как и перед запуском любого плагина, меняющего контент сайта, <b>мы настоятельно рекомендуем сделать бэкап перед началом работы.</b></div>
							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_5.png') ?>">Дополнительно наш плагин умеет делать автоматическую перелинковку сайта после контента, формата «Читайте также». Мы создали удобный и гибкий функционал, который позволяет быстро настроить вид будущей перелинковки.</div>
							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_6.png') ?>">Плагин умеет вести статистику кликов по проставленным ссылкам, для того, чтоб вы могли отслеживать эффективность установленных связей и заменять нерабочие мертвые ссылки на эффективные и кликабельные, тем самым увеличивая поведенческие факторы вашего сайта и улучшая его ранжирование.</div>
							<div class="desc_item"><img src="<?php echo plugins_url('alice/img/icon_7.png') ?>">В данный момент, плагин позволяет линковать статьи в рамках отдельной рубрики и в рамках всего сайта.
							</div>
					</div>
					<div class="btns_wrap">
						<a class="btn btn-warning play" href="<?php echo $url ?>">Начать работу</a>
						<a class="btn btn-primary doc" href="#">Документация</a>
					</div>
					
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php include_once 'footer.php' ?>