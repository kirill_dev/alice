<!-- Перелинковка после статьи, нужны такие настройки:
1. Кол-во статей (2-10)
2. Связывать статьи в одной рубрике или по всему сайту
3. Картинка первая что в статье или миниатюра
4. Размер картинки
5. Расположение блоков: слева картинка, справа текст; картинка сверху, текст снизу;
6. Описание статьи: брать только title или description тоже? Брать h1 и делать анонс из текста?
7. Максимальная длина анонса
8. Название перелинковки, по умолчанию "Похожие статьи:" -->
<!--
	1.count_posts
	2.relation_area
	3.post_thumb
	4.thumb_size
	5.block_layout
	6.post_description
	7.excerpt_size
	8.blocks_title
	9.state
-->
<!-- wp_alice_recent_posts_settings -->


<div class="wrapper-recent-post-form page-wrap">
	<div class="tab_title" style="margin-bottom: 0">
		<h1 class="alice-main-title">
			<span><?php _e('Настройки Похожих Постов', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span></h1>
		<p>
			<?php _e('Для того, чтобы настройки вступили в силу, перезагрузите главную страницу сайта со сбросом кеша', 'alice') ?>
			
		</p>
	</div>
	<form class="recent-post-form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
		
		<?php $settings['enable_recent_posts'] == 'on' ? $state = ' checked' : $state = '' ?>
		<div class="custom_checkbox_wrap<?php echo $state ?>">
			<span class="checkbox-slider-left"><b><?php _e('Выключить', 'alice') ?></b></span>
			<label for="enable_recent_post_checkbox">
				<input id="enable_recent_post_checkbox" type="checkbox" name="enable_recent_posts" <?php echo $state ?>>
				<div class="recent_post_trigger"></div>
			</label>
			
			<span class="checkbox-slider-right"><b><?php _e('Включить', 'alice') ?></b></span>
		</div>
		
		<p>
			<b><?php _e('Заголовок блока', 'alice') ?></b>
			<input style="height: 28px;" type="text" name="blocks_title" value="<?php _e( $settings['blocks_title'], 'alice' ) ?>">
		</p>	
		
		<p>
			<b><?php _e('Количество записей', 'alice') ?></b>
			<input type="number" value="<?php _e( $settings['count_posts'], 'alice' ) ?>" name="count_posts" min="2" max="10">
		</p>	

		<p>
			<b><?php _e('Описание статьи', 'alice') ?></b>
			<select name="post_description">
				<?php if ( $settings['post_description'] == 'only_title' ) : ?>
					<option selected="selected" value="only_title"><?php _e('использовать только заголовок', 'alice') ?></option>				
					<option value="title_desc"><?php _e('использовать заголовок и анонс', 'alice') ?></option>		
				<?php elseif ( $settings['post_description'] == 'title_desc' ) : ?>
					<option value="only_title"><?php _e('использовать только заголовок', 'alice') ?></option>				
					<option  selected="selected" value="title_desc"><?php _e('использовать заголовок и анонс', 'alice') ?></option>	
				<?php endif; ?>
			</select>
		</p>	

		
		<p>
			<b><?php _e('Максимальная длина анонса', 'alice') ?></b>
			<input type="number" name="excerpt_size" value="<?php _e( $settings['excerpt_size'], 'alice' ) ?>" min="">
		</p>

		<p>
			<b><?php _e('Расположение блоков', 'alice') ?></b>
			<select name="block_layout">
				<?php if ( $settings['block_layout'] == 'left_img' ) : ?>
					<option selected="selected" value="left_img"><?php _e('слева картинка, справа текст', 'alice') ?></option>				
					<option value="top_img"><?php _e('картинка сверху, текст снизу', 'alice') ?></option>	
				<?php elseif ( $settings['block_layout'] == 'top_img' ) : ?>
					<option value="left_img"><?php _e('слева картинка, справа текст', 'alice') ?></option>				
					<option selected="selected"  value="top_img"><?php _e('картинка сверху, текст снизу', 'alice') ?></option>				
				<?php endif; ?>
			</select>
		</p>

		<p>
			<b><?php _e('Размер картинки (ширина в px)', 'alice') ?></b>
			<input type="number" name="thumb_size" value="<?php _e( $settings['thumb_size'], 'alice' ) ?>" min="50" max="1000">
		</p>
		
		<p>
			<b><?php _e('Какую картинку использовать в статье', 'alice') ?></b>
			<select name="post_thumb">
				<?php if ( $settings['post_thumb'] == 'thumbnail' ) : ?>
					<option selected="selected" value="thumbnail"><?php _e('Миниатюра', 'alice') ?></option>				
					<option value="first_image"><?php _e('Первая картинка', 'alice') ?></option>
				<?php elseif ( $settings['post_thumb'] == 'first_image' ) : ?>
					<option value="thumbnail"><?php _e('Миниатюра', 'alice') ?></option>				
					<option selected="selected" value="first_image"><?php _e('Первая картинка', 'alice') ?></option>			
				<?php endif; ?>
			</select>
		</p>


		<p>
			<b><?php _e('Размер картинки (высота в px)', 'alice') ?></b>
			<input type="number" name="thumb_size_height" value="<?php _e( $settings['thumb_size_height'], 'alice' ) ?>" min="50" max="1000">
		</p>

		
		<p>	
			<b><?php _e('Область связи статьи', 'alice') ?></b>
			<select name="relation_area">
				<?php if ( $settings['relation_area'] == 'all_site' ) : ?>
					<option selected="selected" value="all_site"><?php _e('По сайту', 'alice') ?></option>				
					<option value="all_category"><?php _e('По категории', 'alice') ?></option>	
				<?php elseif ( $settings['relation_area'] == 'all_category' ) : ?>
					<option value="all_site"><?php _e('По сайту', 'alice') ?></option>				
					<option selected="selected" value="all_category"><?php _e('По категории', 'alice') ?></option>			
				<?php endif; ?>
			</select>
		</p>

		<p>
			<b><?php _e('Размер шрифта (в px)', 'alice') ?></b>
			<input type="number" name="title_font_size" value="<?php _e( $settings['title_font_size'], 'alice' ) ?>">
		</p>

		<div class="submit_wrap">
				<label for="asvc" class="btn btn-success save"><?php _e('Сохранить', 'alice') ?></label>
				<input style="display: none;" id="asvc" type="submit" value="<?php _e('Сохранить', 'alice') ?>">
				<?php wp_nonce_field('wp_nonce_alice-style-css-id', 'wp_nonce_alice-style-css'); ?>
			</div>	

		<input type="hidden" name="action" value="wp_alice_recent_posts">
	</form>
	<?php wp_nonce_field('wp_nonce_alice-recent-post-id', 'wp_nonce_alice-recent-post'); ?>
</div>
