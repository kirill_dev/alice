<?php $alice_link_style_color = get_option('color1'); ?>
<?php $alice_link_style_size = get_option('link_size'); ?>
<?php $custom_css_class = get_option('custom_css_class'); ?>
<?php $enable_preview = get_option('enable_preview'); ?>
<?php $preview_border_color = get_option('preview_border_color'); ?>
<?php $preview_font_color = get_option('preview_font_color'); ?>
<?php $preview_font_size = get_option('preview_font_size'); ?>
<?php $preview_bg_color = get_option('preview_bg_color'); ?>
<?php 
	$preview_img_size = get_option('preview_img_size'); 
	
	if ( $preview_img_size == 'thumbnail' ) {
		$preview_img_size = 55;
	}
?>
<div class=" page-wrap">
	<div class="tab_title">
		<h1 class="alice-main-title">
			<span><?php _e('Настройки CSS', 'alice') ?></span>
			<span class="question-tooltip"><img src="<?php echo plugins_url('alice/img/question.png') ?>"></span></h1>
		<p>
			<?php _e('Статус простановки ссылок вы можете просмотреть как в плагине, так и в <a href="https://alice.2seo.pro">сервисе <img src="http://ishtory.ru/wp-content/plugins/alice/img/new_window.png"></a>', 'alice') ?>
			
		</p>
	</div>
	<form class="css-settings" action="<?php echo esc_url( admin_url('admin.php?page=wp-alice-css') ); ?>" method="POST">
		<div>
			<h2>
				<b><?php _e('Основные', 'alice') ?></b>
			</h2>
			<p>
				<label>
					<b><?php _e('Цвет ссылок', 'alice') ?></b>
				</label><br>
				<input name="color1" class="jscolor" id="colorSelector" type="text" value="<?php echo $alice_link_style_color ?>" />
			</p>
			<p>
				<label><b><?php _e('Размер шрифта ссылок', 'alice') ?></b></label><br>
				<input name="link_size" type="text" value="<?php echo $alice_link_style_size ?>" placeholder="16px" />
			</p>
			<p>
				<label><b><?php _e('Пользовательский класс для ссылок:<br>(тут вы можете добавить один класс)', 'alice') ?></b></label><br>
				<input type="text" name="custom_css_class" value="<?php echo $custom_css_class ?>">
			</p>
		</div>

		<div class="preview_block_wrap">
			<h2>
				<b><?php _e('Включить превью', 'alice') ?></b>
				<label>
					<input type="checkbox" name="enable_preview"<?php echo ($enable_preview == 'on') ? " checked" : '' ?>>
					<div class="cat_filter_checkbox"></div>
				</label>
			</h2>
			<?php if ($enable_preview == "on") {
				$style = '';
				}else {
				$style = ' style="display: none;"';
				} ?>
			<div class="preview_block"<?php echo $style ?>>
				<p>
					<label>
						<b><?php _e('Цвет рамки', 'alice') ?></b>
					</label><br>
					<input name="preview_border_color" class="jscolor" id="colorSelector" type="text" value="<?php echo $preview_border_color ?>" />
				</p>
				
				<p>
					<label>
						<b><?php _e('Цвет фона', 'alice') ?></b>
					</label><br>
					<input name="preview_bg_color" class="jscolor" id="colorSelector" type="text" value="<?php echo $preview_bg_color ?>" />
				</p>

				<p>
					<label>
						<b><?php _e('Цвет шрифта', 'alice') ?></b>
					</label><br>
					<input name="preview_font_color" class="jscolor" id="colorSelector" type="text" value="<?php echo $preview_font_color ?>" />
				</p>

				<p>
					<label><b><?php _e('Размер шрифта', 'alice') ?></b></label><br>
					<input name="preview_font_size" type="text" value="<?php echo $preview_font_size ?>" placeholder="16px" />
				</p>

				<p>
					<b><?php _e('Размер картинки (ширина в px)', 'alice') ?></b><br>
					<input type="number" name="preview_img_size" value="<?php echo $preview_img_size ?>" min="50" max="1000">
				</p>
			</div>
		</div>
		<div class="submit_wrap">
			<label for="asvc" class="btn btn-success save"><?php _e('Сохранить', 'alice') ?></label>
			<input style="display: none;" id="asvc" type="submit" value="<?php _e('Сохранить', 'alice') ?>">
			<?php wp_nonce_field('wp_nonce_alice-style-css-id', 'wp_nonce_alice-style-css'); ?>
		</div>	
	</form>
</div>
<?php include_once 'footer.php' ?>