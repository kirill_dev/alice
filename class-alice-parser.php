<?php
if ( !class_exists('ALICE_Content_Parser') ) {
	class ALICE_Content_Parser
	{

		public function str_replace_nth( $search, $replace, $subject, $nth, $post, $custom_class )
		{
			global $wpdb;
			$cron_links = $wpdb->prefix . "alice_connections_from_server";
			$table_settings = $wpdb->prefix . "alice_filter_settings";
			$format = array( '%d', '%d', '%d', '%d', '%d', '%d', '%s', '%d', '%d', '%s' );

			$min_distance_link = (int)$wpdb->get_results( "SELECT `min_distance_link` FROM $table_settings",ARRAY_A)[0]['min_distance_link'];
			$references_to_the_first_article = (int)$wpdb->get_results( "SELECT `references_to_the_first_article` FROM $table_settings",ARRAY_A)[0]['references_to_the_first_article'];

		    $found = preg_match_all( '~\b' . preg_quote($search) . '\b|<(?:(a).*?</\1>|[^<>]*>)(*SKIP)(*F)~u', $subject, $matches, PREG_OFFSET_CAPTURE );
			
			// $area = 250;
			$area = $min_distance_link;

			$start_link_tag = '<a class="alice_link'.$custom_class.'" target="_blank" href="' . get_permalink($post['input_article_id']) . '">';

			while ( ($lastPos = strpos($subject, $start_link_tag, $lastPos) ) !== false ) {
			    $positions[] = $lastPos;
			    $lastPos = $lastPos + strlen($start_link_tag);
			}

			$count_links_to_same_page = true;
			if ( count($positions) > $references_to_the_first_article ) {
				$count_links_to_same_page = false;
			}

			$start = $matches[0][$nth][1] - $area;	
			$finish = $are + strlen($search) + $area;
			$length_mb = $are + strlen($search) + $area;

			$last_char = strlen($subject);

			if ( $start < 0 ) $start = 0;
			// if ( $finish > $last_char ) $finish = $last_char;

			$allowed_area = substr( $subject, $start, $finish );

			$is_clear = strpos($allowed_area, '<a class="alice_link');

			if ( $post['connection_links_plans_id'] === NULL && $post['connections_id'] === NULL &&  $post['connection_links_id'] === NULL ) {
                
				$post['connection_links_plans_id'] = 0;
				$post['connections_id'] = 0;
				$post['connection_links_id'] = 0;
			}

			$data_insert = array(
				'connection_links_plans_id' => $post['connection_links_plans_id'],
				'connections_id' => $post['connections_id'],
				'connection_links_id' => $post['connection_links_id'],
				'project_id' => $post['project_id'],
				'input_article_id' => $post['input_article_id'],
				'output_article_id' => $post['output_article_id'],
				'text' => $search,
				'processing' => 0
            );

			$erros = [];
			if ( $is_clear ) {
				$errors['increased_minimum_distance'] = true;
			}

			if ( !$found ) {
				$errors['alredy_exists'] = true;
			}

			if ( !$count_links_to_same_page ) {
				$errors['increased_max_links_to_same_page'] = true;
			}
			
			$data_insert['status_errors'] = !empty($errors) ? serialize($erros) : ''; 

			// file_put_contents(__DIR__."/log.txt", 'data_insert - ' . var_export($data_insert, true) . "\n" ,FILE_APPEND);

			if ( $is_clear !== false || !$found || !$count_links_to_same_page ) {
				$data_insert['status'] = 0;
                $insert = $wpdb->insert( $cron_links, $data_insert, $format );                
			}

		    if ( false !== $found && $found > $nth && !$is_clear && $count_links_to_same_page ) {
				$data_insert['status'] = 2;
                $insert = $wpdb->insert( $cron_links, $data_insert, $format );
              
		        return substr_replace( $subject, $replace, $matches[0][$nth][1], strlen($search) );
		    }

		    return $subject;
		}

		public function str_replace_first( $search, $replace, $content, $post, $custom_class )
		{
			global $wpdb;
			$cron_links = $wpdb->prefix . "alice_connections_from_server";

			if ( is_array( $search ) && is_array( $replace ) ) {

				$string = $content;
				$temp = [];

				foreach ( $search as $key => $value ) {

					$temp[] = $value;
					
					$count_values = array_count_values( $temp );
					$output = $this->str_replace_nth( $value, $replace[$key], $string, $count_values[$value] - 1, $post[$key], $custom_class );
                   
					$string = $output;
				}
				return $string;

			} else {
				
				if ( $post[0]['connection_links_plans_id'] === NULL && $post[0]['connections_id'] === NULL &&  $post[0]['connection_links_id'] === NULL ) {
					$post[0]['connection_links_plans_id'] = 0;
					$post[0]['connections_id'] = 0;
					$post[0]['connection_links_id'] = 0;
				}

				$data_insert = array(
					'connection_links_plans_id' => $post[0]['connection_links_plans_id'],
					'connections_id' => $post[0]['connections_id'],
					'connection_links_id' => $post[0]['connection_links_id'],
					'project_id' => $post[0]['project_id'],
					'input_article_id' => $post[0]['input_article_id'],
					'output_article_id' => $post[0]['output_article_id'],
					'text' => $search,
					'processing' => 0,
					'status' => 2,
					'status_errors' => ''
	            );

                $format = array( '%d', '%d', '%d', '%d', '%d', '%d', '%s', '%d', '%d', '%s' );
                $insert = $wpdb->insert( $cron_links, $data_insert, $format );

			    $search = '/' . preg_quote( $search, '/' ) . '|<(?:(a).*?</\1>|[^<>]*>)(*SKIP)(*F)/u';
			    // $search = '/' . preg_quote( $search, '/' ) . '|<(?:(h[1-6]).*?</\1>|[^<>]*>)(*SKIP)(*F)/';

			    return preg_replace( $search, $replace, $content, 1 );

			}
		}

		public function relinking( $posts )
		{
			
			header('Content-type: text/plain; charset=utf-8');            
			global $wpdb;
			
			$custom_class = get_option('custom_css_class') ? ' ' . get_option('custom_css_class') : '';
			$table_links = $wpdb->prefix . "alice_cross_links";
			$cron_links = $wpdb->prefix . "alice_connections_from_server";
			
			$new_posts = [];

			if (  empty( $posts ) )
			    return json_encode(['status' => 'empty']);

			foreach ( $posts as $key => $post ) {

			    $current_post_id = $post['output_article_id'];

			    if ( $key === 0 || $current_post_id == $post['output_article_id'] ) {
			        $new_posts[$post['output_article_id']][] = $post;
			    }
			    
			}

			foreach ($new_posts as $id_post_out => $sorted_posts) {
			    
			    $my_postid = $id_post_out;

			    $content_post = get_post($my_postid);
			    $content = $content_post->post_content;
			    $content = apply_filters('the_content', $content);
			    $content = str_replace(']]>', ']]&gt;', $content);

			    $search = [];
			    $replace = [];
			    
			    foreach ($sorted_posts as $k => $post) {

			        $search[] = $post['text'];
			        $replace[] = '<a class="alice_link'.$custom_class.'" target="_blank" data-id="' . $id_post_out . '" href="' . get_permalink($post['input_article_id']) . '">' . $post['text'] . '</a>';    

			    }
			    
			    $content = $this->str_replace_first( $search, $replace, $content, $sorted_posts );
			    $content1 = $content;
			    // Создаем массив данных
			    $my_post = array();
			    $my_post['ID'] = $id_post_out;
			    $my_post['post_content'] = $content;


			    // Обновляем данные в БД
			    $wp_update = wp_update_post( wp_slash($my_post) );
			    // file_put_contents( __DIR__ . "/relink_pages.txt", '- wp_update - ' . var_export( $wp_update, true ) . "\n", FILE_APPEND);

			    $content_post = get_post($wp_update);
			    $content = $content_post->post_content;

			    // file_put_contents( __DIR__ . "/test.txt", '- after - ' . var_export( $content, true ) . "\n", FILE_APPEND);

			    if ( $wp_update !== 0 ) {
			        // $update = $wpdb->get_results( "UPDATE $table_links SET `status` = 1 WHERE id_post_out = $id_post_out", ARRAY_A );
			    }

			    // file_put_contents( __DIR__ . "/relink_pages.txt", '- wp_update - ' . var_export( $wp_update, true ) . "\n", FILE_APPEND);
			}

			$this->send_response_to_server();
			return json_encode(['status' => 'OK', 'code' => 200]);
		}

		public function relinking_single_post( $posts )
		{ 
			header('charset=utf-8');            
			global $wpdb, $allowedposttags;
			$allowedposttags['a']['data-alice_link_span'] = true;

			$custom_class = get_option('custom_css_class') ? ' ' . get_option('custom_css_class') : '';
			
			$table_links = $wpdb->prefix . "alice_cross_links";
			$cron_links = $wpdb->prefix . "alice_connections_from_server";
			
			$new_posts = [];

			if (  empty( $posts ) )
			    return json_encode(['status' => 'empty']);

			foreach ( $posts as $key => $post ) {

			    $current_post_id = $post['output_article_id'];

			    if ( $key === 0 || $current_post_id == $post['output_article_id'] ) {
			        $new_posts[$post['output_article_id']][] = $post;
			    }
			    
			}

			foreach ($new_posts as $id_post_out => $sorted_posts) {
			    
			    $my_postid = $id_post_out;

			    $content_post = get_post($my_postid);
			    $content = $content_post->post_content;
			    $content = apply_filters('the_content', $content);
			    $content = str_replace(']]>', ']]&gt;', $content);

			    $search = [];
			    $replace = [];
			    
			    foreach ($sorted_posts as $k => $post) {

			        $search[] = $post['text'];
			        $replace[] = '<a class="alice_link'.$custom_class.'" target="_blank" data-id="' . $id_post_out . '" href="' . get_permalink($post['input_article_id']) . '">' . $post['text'] . '</a>';    

			    }

			    $content = $this->str_replace_first( $search, $replace, $content, $sorted_posts, $custom_class );
			    
			    // file_put_contents( __DIR__ . "/content.txt", '- content - ' . var_export( $content, true ) . "\n", FILE_APPEND);
			    
			    $input_article_id = (int)$sorted_posts[0]['input_article_id'];
			    $text = $sorted_posts[0]['text'];
			    
			    $response = $wpdb->get_results( "SELECT `output_article_id`, `input_article_id`, `status`, `text`, `status_errors` FROM {$cron_links} WHERE `output_article_id` = {$id_post_out} AND `input_article_id` = {$input_article_id} AND `text` = '$text'", ARRAY_A);

			    // Создаем массив данных
			    $my_post = array();
			    $my_post['ID'] = $id_post_out;
			    $my_post['post_content'] = $content;

			    // Обновляем данные в БД
			    $wp_update = wp_update_post( wp_slash($my_post) );

			    $content_post = get_post($wp_update);
			    $content = $content_post->post_content;

			    if ( $wp_update !== 0 ) {
			        // $update = $wpdb->get_results( "UPDATE $table_links SET `status` = 1 WHERE id_post_out = $id_post_out", ARRAY_A );
			    }

			    $table = $wpdb->prefix . "alice_connections_from_server";

			    $data_update = array( 'processing' => 1 );
			    $where = array( 'processing' => 0 );
			    $update = $wpdb->update( $table, $data_update, $where );
			}

			return $response;
		}

		public function send_response_to_server()
		{
		    global $wpdb;
		    $table = $wpdb->prefix . "alice_connections_from_server";

		    $posts = $wpdb->get_results("SELECT `connection_links_plans_id`, `connections_id`, `connection_links_id`, `project_id`, `input_article_id`, `output_article_id`, `status`, `status_errors` FROM $table WHERE `processing` = 0 AND connection_links_plans_id != 0", ARRAY_A);
		
		    // $url = 'http://alis.2dev.pro/public/api/executedConnections';
		    $url = 'https://alice.2seo.pro/api/executedConnections';
		    $args = array(
		        'timeout'     => 45,
		        'redirection' => 5,
		        'httpversion' => '1.0',
		        'blocking'    => true,
		        'headers' => array(),
		        'body'    => $posts,
		        'cookies' => array()
		    );
		    
		    $response = wp_remote_post( $url, $args );

		    $data_update = array( 'processing' => 1 );
		    $where = array( 'processing' => 0 );
		    $update = $wpdb->update( $table, $data_update, $where );
		    
		    // file_put_contents( __DIR__ . "/relink_pages.txt", '- update - ' . var_export( $update, true ) . "\n", FILE_APPEND);
		}
	}
}