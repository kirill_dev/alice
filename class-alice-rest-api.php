<?php
if ( class_exists('WP_REST_Controller') && !class_exists('ALICE_Rest_Api') ) {
    class ALICE_Rest_Api extends WP_REST_Controller
    {
        public $namespace;
        public $rest_base;
        private $wpdb;
        public static $check_if_gutenberg_is_on;

        public function __construct() 
        {
            global $wpdb;
            require_once 'class-alice-parser.php'; 
            require_once 'class-search-connections.php'; 

            $this->namespace = 'alice/v2';
            $this->rest_base = 'alice-plugin';
            $this->wpdb = $wpdb;
            self::$check_if_gutenberg_is_on = WP_Alice::check_if_gutenberg_is_on();
        }

        public function register_routes() 
        {
            register_rest_route( $this->namespace, '/' . $this->rest_base . '/get-post-data-from-server', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'get_post_data_from_server' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/get-custom-keywords-from-server', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'get_custom_keywords_from_server' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/get-settings-from-server', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'get_settings_from_server' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/delete-connection', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'delete_connection' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/delete-connection-cron', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'delete_connection_cron' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/relink-pages', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'relink_pages' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/relink-single-page', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'relink_single_page' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            )); 

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/check-alice-status', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'check_alice_status' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));

            register_rest_route( $this->namespace, '/' . $this->rest_base . '/remove-all-connects', array(
                'methods'         => WP_REST_Server::CREATABLE,
                'callback'        => array( $this, 'remove_all_connects' ),
                'args'            => array(
                    'context'          => array(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            ));
        }

        public function remove_all_connects( WP_REST_Request $request )
        {
            $data = $request->get_json_params();
            $token = get_option('alice_access_token');
   
            if ( $token == $data ) {

                $table_name = $this->wpdb->prefix . "alice_cross_links";
                $this->wpdb->query("TRUNCATE TABLE $table_name");
            }
            
            return json_encode(['status' => 'OK', 'code' => 200]);
        }

        public function check_alice_status( WP_REST_Request $request )
        {
            return json_encode(['status' => 'OK', 'code' => 200]);
        }

        public function esc_sql_name( $name ) {
            return str_replace( "`", "``", $name );
        }

        public function get_settings_from_server( WP_REST_Request $request )
        {   
            $data = $request->get_json_params();
            // file_put_contents(__DIR__."/log.txt", 'cvasavsvas ' . var_export( $data, true) . "\n", FILE_APPEND);
            if ( isset( $data ) && !empty( $data ) ) {

                $settings_from_server = $data;

                $table_settings = $this->wpdb->prefix . "alice_filter_settings";
                $table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_settings . "'", ARRAY_A);

                if ( !empty( $table_exists ) ) {

                    $configs = json_decode( $settings_from_server['config'], true );  
                    $stop_keys = json_decode( $settings_from_server['stop_keys'], true ); 
                    $stop_pref = json_decode( $settings_from_server['stop_pref'], true );

                    // file_put_contents(__DIR__ . "/log.txt", 'configs - ' . var_export($configs, true) . "\n", FILE_APPEND);

                    $this->wpdb->query( "UPDATE $table_settings SET `stop_keys` = '$stop_keys' WHERE `id_row` = 111" );
                    $this->wpdb->query( "UPDATE $table_settings SET `stop_pref` = '$stop_pref' WHERE `id_row` = 111" );

                    if ( !isset($configs['allow_subtitles']) )
                        $configs['allow_subtitles'] = 'off';

                    if ( !isset($configs['keyword_from_h1']) )
                        $configs['keyword_from_h1'] = 'off';

                    if ( !isset($configs['keyword_from_title']) )
                        $configs['keyword_from_title'] = 'off';

                    if ( !isset($configs['2word']) )
                        $configs['2word'] = 'off';
                    if ( !isset($configs['3word']) )
                        $configs['3word'] = 'off';
                    if ( !isset($configs['4word']) )
                        $configs['4word'] = 'off';
                    if ( !isset($configs['cut_off_the_pretexts']) )
                        $configs['cut_off_the_pretexts'] = 'off';
                    if ( !isset($configs['direct_entry']) )
                        $configs['direct_entry'] = 'off';
                    if ( !isset($configs['end_selection']) )
                        $configs['end_selection'] = 'off';
                    if ( !isset($configs['small_entry']) )
                        $configs['small_entry'] = 'off';
                     // file_put_contents(__DIR__."/log.txt", 'configs ' . var_export( $configs, true), FILE_APPEND);
                    if ( !empty($configs) ) {
                        foreach ($configs as $config => $value) {
                            if ( strpos($config, '-') !== false  )
                                $config = str_replace('-', '_', $config);

                            $col_name = $this->esc_sql_name($config);
                            $col_name = $this->esc_sql_name($config);

                            $checkcolumn = $this->wpdb->get_results("SELECT column_name FROM information_schema.columns WHERE table_name = '$table_settings' AND column_name = '$col_name'", ARRAY_A);

                            if ( empty($checkcolumn) && $col_name == 'keyword_from_h1' || $col_name == 'keyword_from_title' || $col_name == 'allow_subtitles' ) {
                                $this->wpdb->query("ALTER TABLE $table_settings ADD `$col_name` TEXT NOT NULL");
                                // file_put_contents(__DIR__ . "/log.txt", 'value - ' . var_export($value, true) . "\n", FILE_APPEND);
                            }

                            $sql = $this->wpdb->prepare( "UPDATE $table_settings SET `$col_name` = '%s' WHERE `id_row` = 111", 
                                esc_sql($value)
                            );
                            $r = $this->wpdb->query($sql);

                        }
                    }
                }
            }
        }

        public function alice_content_parser ()
        {   
            $parser = new ALICE_Content_Parser();
            return $parser;
        }

        public function delete_connection_cron ( WP_REST_Request $request )
        {
            header('Content-type: text/plain; charset=utf-8');            
            global $wpdb;

            $table_links = $wpdb->prefix . "alice_cross_links";
            $cron_links = $wpdb->prefix . "alice_connections_from_server";
            $custom_class = get_option('custom_css_class') ? ' ' . get_option('custom_css_class') : '';

            $posts = $request->get_json_params();

            $new_posts = [];

            if (  empty( $posts ) )
                return json_encode( ['status' => 'empty'] );

            foreach ( $posts as $key => $post ) {

                if ( isset( $post['status'] ) && $post['status'] === 0 ) {
                    $current_post_id = $post['output_article_id'];

                    if ( $key === 0 || $current_post_id == $post['output_article_id'] ) {
                       $new_posts[$post['output_article_id']][] = $post;
                    }
                }

            }

            foreach ( $new_posts as $id_post_out => $sorted_posts ) {

                $my_postid = $id_post_out;

                $content_post = get_post($my_postid);
                $content = $content_post->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]&gt;', $content);

                $search = [];
                $replace = [];

                foreach ($sorted_posts as $k => $post) {

                   $search[] = '<a class="alice_link' . $custom_class . '" target="_blank" data-id="' . $id_post_out . '" href="' . get_permalink( $post['input_article_id'] ) . '">' . $post['text'] . '</a>';
                   $replace[] = $post['text'];    

                }

                $id_post_in = $post['input_article_id'];
                
                $keyword = $post['text'];

                // $content = str_replace( $search, $replace, $content );
                
                // Создаем массив данных
                $my_post = array();
                $my_post['ID'] = $id_post_out;
                $my_post['post_content'] = $content;

                // Обновляем данные в БД
                $wp_update = wp_update_post( wp_slash($my_post) );
                
                if ( $wp_update !== 0 ) {
                    $update = $wpdb->query( "DELETE FROM $cron_links WHERE `input_article_id` = $id_post_in AND `output_article_id` = $id_post_out AND `text` = '$keyword'");
                }
            }   

            return json_encode(['status' => 'OK', 'code' => 200]);
        }

        public function relink_single_page ( WP_REST_Request $request )
        {
            $posts = $request->get_json_params();
            $response = $this->alice_content_parser()->relinking_single_post( $posts );

            return $response;
        }
        
        public function relink_pages ( WP_REST_Request $request )
        {   
            $posts = $request->get_json_params();
            $this->alice_content_parser()->relinking( $posts );
        }

        public function delete_connection ( WP_REST_Request $request )
        {

            header('Content-type: text/plain; charset=utf-8');            
            global $wpdb;

            $table_links = $wpdb->prefix . "alice_cross_links";
            $cron_links = $wpdb->prefix . "alice_connections_from_server";
            $custom_class = get_option('custom_css_class') ? ' ' . get_option('custom_css_class') : '';
            $posts = $request->get_json_params();
            
            $new_posts = [];

            if (  empty( $posts ) )
                return json_encode(['status' => 'empty']);

            foreach ( $posts as $key => $post ) {

                $current_post_id = $post['output_article_id'];

                if ( $key === 0 || $current_post_id == $post['output_article_id'] ) {
                   $new_posts[$post['output_article_id']][] = $post;
                }
            }

            // file_put_contents(__DIR__."/log.txt", 'efvcdsvsdvsdvsdv - ' . var_export( self::$check_if_gutenberg_is_on, true) . "\n", FILE_APPEND);
            $noopenner = '';
            if ( self::$check_if_gutenberg_is_on )
                $noopenner = ' rel="noopener noreferrer"';

            foreach ( $new_posts as $id_post_out => $sorted_posts ) {

                $my_postid = $id_post_out;

                $content_post = get_post($my_postid);
                $content = $content_post->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]&gt;', $content);

                $search = [];
                $replace = [];

                $pattern = '~\b' . $keyword . '\b|<(?:(h[1-6]).*?</\1>|[^<>]*>)(*SKIP)(*F)~ui';
                // preg_match_all('%<a(.*?)href="http([s])*:\/\/(.*?)([>]{1})(.*?)<\/a>%i', $content_post, $matches, PREG_OFFSET_CAPTURE);
                // preg_replace( '%<a(.*?)href="http([s])*:\/\/(.*?)([>]{1})(.*?)<\/span>%i', $post['text'], $content);

                // foreach ( $sorted_posts as $k => $post ) {

                //    $search[] = '<a class="alice_link'.$custom_class.'" target="_blank" href="' . get_permalink($post['input_article_id']) . '"' . $noopenner . '>' . $post['text'] . '</a><span class="alice_link_span">' . $id_post_out . '</span>';
                //    $replace[] = $post['text'];    

                // }

                $id_post_in = $post['input_article_id'];
                
                $keyword = $post['text'];

                // $content = str_replace($search, $replace, $content);

                $dom = new DOMDocument();
                $dom->formatOutput = true;
                $dom->loadHTML('<?xml encoding="utf-8" ?>'.$content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $alice_links = $dom->getElementsByTagName('a');
                foreach ($alice_links as $alice_link) {
                    $classes = explode(' ', $alice_link->getAttribute('class'));

                    if ( $alice_link->nodeValue == $post['text'] && in_array('alice_link', $classes) ) {

                        $text = $dom->createTextNode($post['text']);
                        $nodetext = $dom->appendChild($text);

                        $span = $alice_link->nextSibling;
                        if ( $span != NULL )
                            $alice_link->parentNode->removeChild( $span );
                        
                        $res = $alice_link->parentNode->replaceChild($text, $alice_link);
                    }
                }

                // die();
                foreach ($dom->childNodes as $item) {
                    if ($item->nodeType == XML_PI_NODE){
                        $dom->removeChild($item);
                    }
                }
                $dom->encoding = 'UTF-8';

                $new_content = $dom->saveXML();
                $new_content = str_replace( array('<?xml encoding="utf-8" ?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"??>'), array('','',''), $new_content );
                $content = $new_content;

                // $content = $this->alice_content_parser()->delete_single_connection( $search, $replace, $content, $sorted_posts );
                
                // Создаем массив данных
                $my_post = array();
                $my_post['ID'] = $id_post_out;
                $my_post['post_content'] = $content;

                // Обновляем данные в БД
                $wp_update = wp_update_post( wp_slash($my_post) );
                
                // file_put_contents( __DIR__ . "/delete_connection.txt", '- wp_update - ' . var_export( $wp_update, true ) . "\n", FILE_APPEND);
                
                if ( $wp_update !== 0 ) {
                    $update = $wpdb->query( "DELETE FROM $cron_links WHERE `input_article_id` = $id_post_in AND `output_article_id` = $id_post_out AND `text` = '$keyword'");
                }
            }   

            return json_encode(['status' => 'OK', 'code' => 200]);
        }

        public function get_custom_keywords_from_server (  WP_REST_Request $request )
        {
            $alice_class = new Search_Connections();

            $table = $this->wpdb->prefix . "alice_page_stats";
            $table_settings = $this->wpdb->prefix . "alice_filter_settings";

            $arr_keys = $request->get_json_params();
            
            $finish_arr = [];
            $ids = [];
            if ( !empty( $arr_keys ) && is_array( $arr_keys ) ) {

                foreach ($arr_keys as $key => $value) {
                    $post_url = $key;
                    $post_id = url_to_postid( $post_url );
                    $post_type = get_post_type( $post_id );

                    if ( !$post_id ) {
                        $url_arr = explode('/', $post_url);
                        
                        if ( is_array($url_arr) ) 
                            $slug = $url_arr[count($url_arr) - 1];

                        $obj = get_term_by('slug', $slug, 'post_tag');
                        $post_type = 'post_tag';

                        if ( !$obj ) {
                            $obj = get_term_by('slug', $slug, 'category');
                            $post_type = 'category';
                        }
                        
                        $post_id = isset($obj->term_id) ? $obj->term_id : 0;
                    }
                                        
                    // $existing_keywords = $this->wpdb->get_results( "SELECT `custom_user_keywords` FROM $table WHERE `post_id` = $post_id AND `post_type` = '$post_type'", ARRAY_A )[0]['custom_user_keywords'];   
                    //   if ( !empty($existing_keywords) ){
                    //     $existing_keywords = unserialize($existing_keywords);
                    // }else{
                    //     $existing_keywords = [];
                    // }

                    $existing_keywords_all = $this->wpdb->get_results( "SELECT `keys_from_server`, `keys_from_admin`, `keys_from_step`, `custom_user_keywords` FROM $table WHERE `post_id` = $post_id AND `post_type` = '$post_type'", ARRAY_A )[0];   
                    
                    $keys_from_server = $existing_keywords_all['keys_from_server'];
                    $keys_from_admin = $existing_keywords_all['keys_from_admin'];
                    $keys_from_step = $existing_keywords_all['keys_from_step'];
                    $custom_user_keywords = $existing_keywords_all['custom_user_keywords'];
                    
                    if ( !empty($keys_from_server) ){
                        $keys_from_server = unserialize($keys_from_server);
                    }else{
                        $keys_from_server = [];
                    }

                     if ( !empty($keys_from_admin) ){
                        $keys_from_admin = unserialize($keys_from_admin);
                    }else{
                        $keys_from_admin = [];
                    }

                    if ( !empty($keys_from_step) ){
                        $keys_from_step = unserialize($keys_from_step);
                    }else{
                        $keys_from_step = [];
                    }

                    if ( !empty($custom_user_keywords) ){
                        $custom_user_keywords = unserialize($custom_user_keywords);
                    }else{
                        $custom_user_keywords = [];
                    }

                    $existing_keywords_all_new = array_unique( array_merge( $keys_from_server, $keys_from_admin, $keys_from_step, $custom_user_keywords ) );
                    

                    // file_put_contents(__DIR__ . "/log.txt", '- $post_id - ' . var_export( $post_id, true ) . "\n", FILE_APPEND);
                    // file_put_contents(__DIR__ . "/log.txt", '- existing_keywords_all_new - ' . var_export( $existing_keywords_all_new, true ) . "\n", FILE_APPEND); 
                    // die();
                    
                    $arr_keys_yandex = $value;

                    $new_custom_user_keywords = array_diff( $arr_keys_yandex, $existing_keywords_all_new  );

                    // file_put_contents(__DIR__ . "/log.txt", '- arr - ' . var_export( $arr, true ) . "\n", FILE_APPEND);
                    // file_put_contents(__DIR__ . "/log.txt", '- custom_user_keywords - ' . var_export( $custom_user_keywords, true ) . "\n", FILE_APPEND);
                    // file_put_contents(__DIR__ . "/log.txt", '- existing_keywords - ' . var_export( $existing_keywords, true ) . "\n\n\n", FILE_APPEND);
                    // foreach ( $arr_keys_yandex as $key_word ) {
                        // $key_arr = $alice_class->setColl( $key_word );
                        // $key_arr = $key_word;
                        // file_put_contents(__DIR__ . "/log.txt", '- key_arr - ' . var_export( $key_arr, true ) . "\n", FILE_APPEND);
                        // file_put_contents(__DIR__ . "/log.txt", '- existing_keywords - ' . var_export( $existing_keywords, true ) . "\n", FILE_APPEND);
                        $temp = array_unique( array_merge( $custom_user_keywords, $new_custom_user_keywords ) );
                        // file_put_contents(__DIR__ . "/log.txt", '- temp - ' . var_export( array_merge( $key_arr, $existing_keywords ), true ) . "\n", FILE_APPEND);
                        // file_put_contents(__DIR__ . "/log.txt", '- temp1 - ' . var_export( array_merge( [], [] ), true ) . "\n", FILE_APPEND);
                        $existing_keywords = $temp;        
                    // }
                    // file_put_contents(__DIR__ . "/log.txt", '- existing_keywords - ' . var_export( $existing_keywords, true ) . "\n", FILE_APPEND);
            
                    // file_put_contents(__DIR__ . "/log.txt", '- 11existing_keywords - ' . var_export( $existing_keywords, true ) . "\n", FILE_APPEND);

                    // file_put_contents(__DIR__ . "/log.txt", '- existing_keywords END- ' . var_export( $existing_keywords, true ) . "\n", FILE_APPEND);
                    // die();
                    if ( !empty($existing_keywords) ){
                        $existing_keywords = serialize($existing_keywords);
                    }else{
                        $existing_keywords = '';
                    }
                    // file_put_contents(__DIR__ . "/log2.txt", $post_id . ' : ' .  var_export( $existing_keywords, true ) . "\n", FILE_APPEND);
                    
                    //$insert = $this->wpdb->query( "UPDATE $table SET `keys_from_server` = '$existing_keywords' WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'" ); 
                    $insert = $this->wpdb->query( "UPDATE $table SET `custom_user_keywords` = '$existing_keywords' WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'" ); 
                }
            }

            return json_encode(['status' => 'done!']);
        }
    }
}