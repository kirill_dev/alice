<?php
/*
Plugin Name: Alice
Description: Плагин осуществляет перелинковку сайта.
Author: Сергей Полховский, Анастасия Леднева, Кирилл Пацуков, Дмитрий Деркач
Plugin URI:  
Author URI:  
Version: 1.0.6
Text Domain: alice
Domain Path: /languages
License URI: https://www.gnu.org/licenses/gpl-2.0.html
License: GPL2
	Alice is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	Alice is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Alice. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
if ( !class_exists('WP_Alice') ) :
	global $wpdb;

	require 'plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/kirill_dev/alice',
		__FILE__, //Full path to the main plugin file or functions.php.
		'alice'
	);
	$myUpdateChecker->setAuthentication('7e4WdQn_NHJkckt12S5_');
	$myUpdateChecker->setBranch('master');

	class WP_Alice
	{
		const ALICE_PLUGIN_VERSION = '1.0.6';

		static $instance = false;
		private $wpdb;
		private $table_menu;
		protected $stremmer;
		public $import;
		private $alice_access_token;
		
		public static function alice_get_instance() 
		{
			if ( !self::$instance )
				self::$instance = new self;
			return self::$instance;
		}

		private function __construct()
		{
			require 'class-alice-stremmer-ru.php'; 
			require 'class-search-connections.php'; 
			require 'class-alice-parser.php'; 
			require 'class-alice-rest-api.php'; 
			require 'class-import-links-to-alice.php'; 

			global $wpdb;
			$this->wpdb = $wpdb;
			$this->table_menu = $this->wpdb->prefix . "alice_plugin_menu";
			$this->stremmer = new Stemmer_RU();

			$this->alice_access_token = get_option( 'alice_access_token' );

			add_action( 'rest_api_init', array( $this,'create_initial_rest_routes'), 0 );
			if ( isset( $_GET['page'] ) && !empty( $_GET['page'] ) && strpos($_GET['page'], 'wp-alice') !== false ) {
				add_action( 'admin_print_styles', array( $this, 'alice_enqueue_styles' ) );
				add_action( 'admin_print_scripts', array( $this, 'alice_enqueue_scripts' ) );
				add_filter('admin_footer_text', array( $this, 'change_footer_admin'), 9999);
				add_filter( 'update_footer', array( $this, 'change_footer_version'), 9999);			
			}	

			add_action( 'wp_head', array( $this, 'preview_global_var' ), 1 );
			add_action( 'wp_head', array( $this, 'custom_alice_class' ), 1 );

			add_action( 'admin_init', array( $this, 'wpse_60168_custom_menu' ) );
				
			add_action( 'wp_enqueue_scripts', array( $this, 'alice_enqueue_front_styles' ), 100 );
			add_action( 'wp_enqueue_scripts', array( $this, 'alice_enqueue_front_scripts' ) );	

			add_action( 'admin_menu', array( $this, 'alice_add_admin_page' ) );

			add_action( 'wp_ajax_send_posts_to_server', array( $this, 'send_posts_to_server' ) );	
			
			//qcsasasvasvasv
			add_action( 'wp_ajax_change_class_link', array( $this, 'change_class_link' ) );
			add_action( 'wp_ajax_add_class_link', array( $this, 'add_class_link' ) );
			add_action( 'wp_ajax_add_class_link_global', array( $this, 'add_class_link_global' ) );
			//qcsasasvasvasv

			add_action( 'wp_ajax_check_processing', array( $this, 'check_processing' ) );
			add_action( 'wp_ajax_check_processing_on_the_server', array( $this, 'check_processing_on_the_server' ) );
			
			add_action( 'wp_ajax_alice_count_clicks', array( $this, 'alice_count_clicks' ) );
			add_action( 'wp_ajax_nopriv_alice_count_clicks', array( $this, 'alice_count_clicks' ) );

			add_action( 'wp_ajax_alice_preview_for_link', array( $this, 'alice_preview_for_link' ) );
			add_action( 'wp_ajax_nopriv_alice_preview_for_link', array( $this, 'alice_preview_for_link' ) );

			add_action( 'admin_post_nopriv_send_posts_to_server', array( $this, 'send_posts_to_server' ) );
			add_action( 'admin_post_send_posts_to_server', array( $this, 'send_posts_to_server' ) );

			add_action( 'wp_ajax_save_keywords_for_post', array( $this, 'save_keywords_for_post' ) );
			add_action( 'wp_ajax_send_connexion_to_server', array( $this, 'send_connexion_to_server' ) );
			add_action( 'wp_ajax_settings_form_by_cat', array( $this, 'settings_form_by_cat' ) );
			add_action( 'wp_ajax_settings_form_by_cat_all', array( $this, 'settings_form_by_cat_all' ) );
			add_action( 'wp_ajax_settings_form_by_cat_deselect_all', array( $this, 'settings_form_by_cat_deselect_all' ) );

			// RE-linking
			add_action( 'admin_post_nopriv_relinking', array( $this, 'relinking' ) );
			add_action( 'admin_post_relinking', array( $this, 'relinking' ) );

			// delete RE-linking
			add_action( 'admin_post_nopriv_delete_relinking', array( $this, 'delete_relinking' ) );
			add_action( 'admin_post_delete_relinking', array( $this, 'delete_relinking' ) );

			// Recent Post
			add_action( 'admin_post_nopriv_wp_alice_recent_posts', array( $this, 'wp_alice_recent_posts' ) );
			add_action( 'admin_post_wp_alice_recent_posts', array( $this, 'wp_alice_recent_posts' ) );

			add_action('save_post', array( $this, 'check_post_in_stat_table' ), 10, 3  );
			// add_action('before_delete_post', array( $this, 'delete_post_in_stat_table' ) );

			add_action( 'admin_init', array( $this, 'codex_init') );
			
			add_filter('the_content',  array( $this, 'add_recent_posts') , 999, 1 ) ;

			// add_action( 'save_post', array( $this, 'save_meta_box_alice_keywords' ));

			add_action('transition_post_status', array( $this, 'create_meta_before_publish'), 10, 3 );

			add_action ('admin_init', array( $this, 'check_if_gutenberg_is_on') );

			// Update Plugin
			add_action( 'plugins_loaded', array( $this, 'my_upgrade_function') );

			register_activation_hook( __FILE__, array( $this, 'create_menu_table' ) );
			// register_deactivation_hook( __FILE__, array( $this, 'myplugin_deactivate' ) );

		}

		public function change_class_link()
		{
			ImportLinksToAlice::change_class_link();
		}

		public function add_class_link()
		{
			ImportLinksToAlice::add_class_link();
		}

		public function add_class_link_global()
		{
			ImportLinksToAlice::add_class_link_global();
		}
		
		public function my_upgrade_function()
		{	
			global $wpdb;
			if ( self::ALICE_PLUGIN_VERSION !== get_option('alice_plugin_version') ) {
				update_option('alice_plugin_version', self::ALICE_PLUGIN_VERSION);
				
				$table_cross_links = $wpdb->prefix . 'alice_cross_links';
				
	    		$column_date_post = "date_post";
	    		$column_token = "token";
	    		$checkcolumn_date_post = $wpdb->get_results("SELECT column_name FROM information_schema.columns WHERE table_name = '$table_cross_links' AND column_name = '$date_post'", ARRAY_A);
	    		$checkcolumn_token = $wpdb->get_results("SELECT column_name FROM information_schema.columns WHERE table_name = '$table_cross_links' AND column_name = '$token'", ARRAY_A);

	    		if( empty($checkcolumn_date_post) ) 
	    		    $wpdb->query("ALTER TABLE $table_cross_links ADD date_post text NOT NULL");

	    		if( empty($checkcolumn_token) ) 
	    		    $wpdb->query("ALTER TABLE $table_cross_links ADD token text NOT NULL");	

    		}
		}

		public static function check_if_gutenberg_is_on()
		{
			$gutenberg    = false;
			$block_editor = false;

			if ( has_filter( 'replace_editor', 'gutenberg_init' ) ) {
				// Gutenberg is installed and activated.
				$gutenberg = true;
			}

			if ( version_compare( $GLOBALS['wp_version'], '5.0-beta', '>' ) ) {
				// Block editor.
				$block_editor = true;
			}

			if ( ! $gutenberg && ! $block_editor ) {
				return false;
			}

			include_once ABSPATH . 'wp-admin/includes/plugin.php';

			if ( ! is_plugin_active( 'classic-editor/classic-editor.php' ) ) {
				return true;
			}

			$use_block_editor = ( get_option( 'classic-editor-replace' ) === 'no-replace' );

			return $use_block_editor;
		}

		//Hide admin footer from admin
		public function change_footer_admin () { return ' ';}
		public function change_footer_version() { return ' ';}

		public function codex_init() {
		    add_action( 'delete_post', array( $this, 'delete_post_in_stat_table'), 10 );
		}

		public function create_meta_before_publish($new_status, $old_status, $post)
		{
			if ( $old_status == 'new' && 'auto-draft' === $new_status ) {
				add_action('add_meta_boxes', array( $this, 'add_meta_box_alice_keywords'), 1 );
		   }
		}

		function add_meta_box_alice_keywords(){
			$screens = array( 'post', 'page' );
			add_meta_box( 'alice_use_and_keyword', 'Использовать в перелинковке', array( $this, 'keywords_metabox'), $screens, 'side', 'high' );
		}

		function keywords_metabox( $post, $meta )
		{
			$post_id = $post->ID;
			$table = $this->wpdb->prefix . 'alice_page_stats';
			$keywords = $this->wpdb->get_results("SELECT `keys_from_server` FROM $table WHERE `post_id` = $post_id", ARRAY_A)[0]['keys_from_server'];
			$screens = $meta['args'];

			wp_nonce_field( plugin_basename( __FILE__ ), 'alice_noncename' );
			
			if ( $value == 'on' ) {
				$checked = ' checked';
			}elseif ( $value == 'off' ){
				$checked = '';
			}

			echo '<label for="alice_use_post_in_link">Использовать&nbsp;<input type="checkbox" id="alice_use_post" name="alice_use_post_in_link"'.$checked.'/></label>';
			echo '<p>Вставте ключевые слова</p>';
			echo '<textarea style="width: 100%; min-height: 300px;" id="post_admin_keywords" name="post_admin_keywords"></textarea>';
		}

		function save_meta_box_alice_keywords( $post_id ) {

			$post_type = get_post_type($post_id);

			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return;

			if ( ! wp_verify_nonce( $_POST['alice_noncename'], plugin_basename(__FILE__) ) )
				return;

			if( ! current_user_can( 'edit_post', $post_id ) )
				return;

			if ( isset( $_POST['alice_use_post_in_link'] ) && $_POST['alice_use_post_in_link'] == 'on' ) {
				$alice_use_post_in_link = 'on';				
			}else{
				$alice_use_post_in_link = 'off';
			}

			if ( $alice_use_post_in_link = 'on' && $post_type != 'revision' ) {
				
				$title = get_the_title( $post_id );
				$table = $this->wpdb->prefix . 'alice_page_stats';
				$alice_class = new Search_Connections();
				$post_type = get_post_type($post_id);
				$content_post = get_post( $post_id );
				$content_post = $content_post->post_content;

	    		if ( $content_post !== null || !empty( $content_post ) ) {

	    			$content = apply_filters('the_content', $content_post);
	    			$post_content = str_replace(']]>', ']]&gt;', $content);

					$post_content = preg_replace("/('|\"|\r?\n)/", '', $post_content);
			        $post_content = preg_replace("/\n/", '', $post_content);
			        $post_content = str_replace( ' ', '', $post_content);
			        $post_content = str_replace( '  ', '', $post_content);
			        $count_chars = mb_strlen($post_content,'UTF-8');
	    
	    		} else {

	    			$count_chars = 0;

	    		}

	    		if ( isset( $_POST['post_admin_keywords'] ) && !empty( $_POST['post_admin_keywords'] ) ) {

					$keywords = $_POST['post_admin_keywords'];
					$keywords = explode( array("\r", "\n"), $keywords);
					$keywords = serialize($keywords);
	    			
	    		}else{
	    			$keywords = '';
	    		}
			
				$data_insert = array(
				    'post_id' => $post_id,
				    'post_type' => $post_type,
				    'size_post' => $count_chars,
				    'count_in_link' => $key,
				    'count_out_link' => $key,
				    'count_external' => $key,
				    'keys_from_server' => '',
				    'keys_from_admin' => $keywords,
				    'keys_from_step' => '',
				    'custom_user_keywords' => '',
				    'checkbox' => 1,
				    'is_save' => 0,
				    'state' => $state
				);

				$format = array( '%d', '%s', '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%d', '%d', '%s' );
				$insert = $this->wpdb->insert( $table_menu, $data_insert, $format );	
				
				$this->get_count_of_links();

				$kpi = 0;
				$table_kpi = $this->wpdb->prefix . "kpi_seo_plugin";
				$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '".$table_kpi."'", ARRAY_A);
				$url = get_permalink( $post_id );

				if ( !empty( $table_exists ) )
					$kpi = $this->wpdb->get_results("SELECT kpi_seo FROM $table_kpi WHERE `post_id` = $post_id", ARRAY_A)[0]['kpi_seo'];

				$keywords_arr = array_unique( $alice_class->setColl($title) );
				if ( !empty( $keywords_arr ) ){
					$keywords_arr = serialize( $keywords_arr );
				}else{
					$keywords_arr = '';
				}

				$insert = $this->wpdb->query( "UPDATE $table SET `keys_from_server` = '$keywords_arr' WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type' AND keys_from_server = ''" );
				
				$counts = $this->wpdb->get_results("SELECT `count_in_link`, `count_out_link`, `size_post` FROM $table WHERE `post_id` = $post_id AND `post_type` = '$post_type'", ARRAY_A)[0];

				$posts[] = array(
					'title' => $title,
					'token' => $this->alice_access_token,
					'article_id' => $post_id,
					'url' => $url,
					'publish_date' => get_the_date('Y-m-d H:i:s', $post_id),
					'count_input_link' => (int)$counts['count_in_link'],
					'count_output_link' => (int)$counts['count_out_link'],
					'count_symbols' => (int)$counts['size_post'],
					'kpi' => (int)$kpi,
					'status' => 'new'
				);

				$url = 'https://alice.2seo.pro/api/setStats';
				$data_string = json_encode($posts);			                                                                                                                     
				$ch = curl_init($url);                                                                      
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				    'Content-Type: application/json',                                                                                
				    'Content-Length: ' . strlen($data_string))                                                                       
				);                                                                                                                   
				                                                                                                                     
				$result = curl_exec($ch);
			}
		}

		public function preview_global_var()
		{ 
			$enable_preview = get_option('enable_preview');
			if ( $enable_preview == 'on' ) :
			?>
				<script type="text/javascript">
				    var enable_preview = true;      
				 </script>
			<?php
			
			else : ?>
				<script type="text/javascript">
				    var enable_preview = false;      
				 </script>
			<?php
			endif;
		}

		public function myplugin_deactivate()
		{
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_recent_posts_settings" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_clicks_count_sort" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_click_stat" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_connections_from_server" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_cross_links" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_filter_settings" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_page_stats" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}alice_plugin_menu" );
			$this->wpdb->query( "DROP TABLE IF EXISTS {$this->wpdb->prefix}recent_posts_settings" );

			delete_option('alice_access_token');
			delete_option('alice_plugin_version');	
		}

		public function alice_preview_for_link()
		{	

			$preview_border_color = get_option('preview_border_color');
			$preview_font_color = get_option('preview_font_color');
			$preview_font_size = get_option('preview_font_size');
			$preview_img_size = get_option('preview_img_size');
			$preview_bg_color = get_option('preview_bg_color');

			$href = $_POST['href'];
			$id = url_to_postid($href);
			
			// if ( $preview_img_size != 'thumbnail' ) {
				$size = array($preview_img_size,$preview_img_size);
			// }else{
			// 	$size = 'thumbnail';
			// }

			$thumbnail = get_the_post_thumbnail_url( $id, $size );
			$title = get_the_title( $id );
			$arr['image'] = $thumbnail;
			$arr['title'] = $title;

			$arr['preview_border_color'] = $preview_border_color;
			$arr['preview_font_color'] = $preview_font_color;
			$arr['preview_img_size'] = $preview_img_size;
			$arr['preview_bg_color'] = $preview_bg_color;
			$arr['preview_font_size'] = $preview_font_size;
			echo json_encode( $arr );
			// echo '<pre>'; var_dump($arr); echo '</pre>';

			wp_die();
		}

		public function wp_alice_recent_posts()
		{
			header('Content-type: text/plain; charset=utf-8'); 
			
			$blocks_title = $_POST['blocks_title'];
			$is_enabled = ( isset($_POST['enable_recent_posts']) ) ? $_POST['enable_recent_posts'] : 'off';
			// $is_enabled = $_POST['enable_recent_posts'] ?? 'off';
			$count_posts = (int)$_POST['count_posts'];
			$excerpt_size = (int)$_POST['excerpt_size'];
			$thumb_size = (int)$_POST['thumb_size'];
			$thumb_size_height = (int)$_POST['thumb_size_height'];
			$title_font_size = (int)$_POST['title_font_size'];
			$block_layout = $_POST['block_layout'];
			$post_thumb = $_POST['post_thumb'];
			$relation_area = $_POST['relation_area'];
			$post_description = $_POST['post_description'];

			$table = $this->wpdb->prefix . "alice_recent_posts_settings";

			$data = array(
				'count_posts' => $count_posts,
				'relation_area' => $relation_area,
				'post_thumb' => $post_thumb,
				'thumb_size' => $thumb_size,
				'thumb_size_height' => $thumb_size_height,
				'title_font_size' => $title_font_size,
				'block_layout' => $block_layout,
				'post_description' => $post_description,
				'excerpt_size' => $excerpt_size,
				'blocks_title' => $blocks_title,
				'enable_recent_posts' => $is_enabled
			);

			$where = array( 'id' => 1 );

			$this->wpdb->update( $table, $data, $where );

			wp_redirect( admin_url( 'admin.php?page=wp-alice-recent-posts' ), 301 ); 
			exit;
		}

		public function add_recent_posts( $content )
		{
			global $post;
			$table = $this->wpdb->prefix . "alice_recent_posts_settings";	

			$recent_settings = $this->wpdb->get_results("SELECT * FROM {$table}", ARRAY_A)[0];

			$enable_recent_posts = $recent_settings['enable_recent_posts'];


			if ( $enable_recent_posts === 'off' ) 
				return $content;

			$categories = get_the_category($post->ID); 
			if ($categories) : 
				$category_ids = array(); 

				foreach ( $categories as $individual_category) $category_ids[] = $individual_category->term_id; 

				if ( $recent_settings['relation_area'] == 'all_site' ) {
					$args = array( 
						'post__not_in' => array( $post->ID ), 
						'showposts' => $recent_settings['count_posts'], 
						'ignore_sticky_posts' => 1, 
						'orderby' => 'rand'
					);
				} elseif ( $recent_settings['relation_area'] == 'all_category' ) {
					$args = array( 
						'category__in' => $category_ids, 
						'post__not_in' => array( $post->ID ), 
						'showposts' => $recent_settings['count_posts'], 
						'ignore_sticky_posts' => 1, 
						'orderby' => 'rand'
					);
				}

				$my_query = new WP_Query( $args ); 
				// var_dump($my_query);
				if ( $my_query->have_posts() ) :
					
					$blocks_title = $this->wpdb->get_results( "SELECT `blocks_title` FROM {$table}", ARRAY_A)[0]['blocks_title'];

					if ( !empty( $blocks_title ) ) {
						$blocks_title_tag = '<h3>' . __($blocks_title, 'alice') . '</h3>';
					}else{
						$blocks_title_tag = '';
					}

					$extra_stuff .= '<div class="alice-related-posts"><div class="postauthor-top">' . $blocks_title_tag . '</div><ul>';
					$layout = ' '.$recent_settings['block_layout'];
					$counter = 0; 
					while( $my_query->have_posts() ) :
						$my_query->the_post();
						// $extra_stuff .= get_the_content();
						++$counter; 

						$postclass = 'alice-recent-post'; 


						if ( has_post_thumbnail() && $recent_settings['post_thumb'] == 'thumbnail' ) :

							// $img = get_the_post_thumbnail($my_query->ID, array( (int)$recent_settings['thumb_size'], auto ) );
							$img = get_the_post_thumbnail_url($my_query->ID, array( (int)$recent_settings['thumb_size'], (int)$recent_settings['thumb_size_height'] ) );

						elseif ( $recent_settings['post_thumb'] == 'first_image' ) :

						 	$src_img = $this->catch_that_image( get_the_content() );
						 	
							$img = '<img src="' . $src_img . '" alt="'. get_the_title() .'" height="'.(int)$recent_settings['thumb_size_height'].'" width="'.(int)$recent_settings['thumb_size'].'" class="wp-post-image" />';
						else :
							$src_img = plugins_url('alice/img/default.jpg');
							$img = '<img src="' . $src_img . '" alt="'. get_the_title() .'" height="'.(int)$recent_settings['thumb_size_height'].'" width="'.(int)$recent_settings['thumb_size'].'" class="wp-post-image" />';	
						endif;
						
						$r = $li+$counter;

						$get_the_title = mb_substr( get_the_title(), 0, $recent_settings['excerpt_size'] );

						$extra_stuff .= '<li class="' . $postclass . '">
						<a rel="nofollow" class="alice-recent-link" href="' . get_permalink() . '" title="'. get_the_title() .'">
							<span class="alice-recent-thumb-wrap' . $layout . '" style="background-image:url('.$img.'); height: '.(int)$recent_settings['thumb_size_height'].'px; width: '.(int)$recent_settings['thumb_size'].'px"></span>
							<span class="alice-recent-title' . $layout . '" style="font-size:' . $recent_settings['title_font_size'] . 'px">' . $get_the_title . '</span>
						</a>
						</li>';
					endwhile;

					$extra_stuff .= '</ul></div>';
				endif;	
			endif;
			wp_reset_query();

			return $content . $extra_stuff;			
		}

		public function catch_that_image( $content )
		{
			global $post, $posts;
			$first_img = '';
			ob_start();
			ob_end_clean();
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
			$first_img = $matches[1][0];

			if(empty($first_img)) { 
				$first_img = plugins_url('alice/img/default.jpg');
			}
			return $first_img;
		}

		public function create_menu_table()
		{
			// Menu Table
			$table_menu = $this->wpdb->prefix . "alice_plugin_menu";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_menu . "'", ARRAY_A);

			if ( empty( $table_exists ) ) {
			    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			    $sql_request = "CREATE TABLE " . $table_menu . " (
			        id int(11) NOT NULL AUTO_INCREMENT,
			  	 	menu_item_name text NOT NULL,
			  	 	menu_item_slug text NOT NULL,
			  	 	state bit NOT NULL,
			        PRIMARY KEY(id))
			        ENGINE = INNODB
			        DEFAULT CHARACTER SET = utf8
			        COLLATE = utf8_general_ci";
			    $dbDelta = dbDelta($sql_request);

			    $pages = ['token' => 'Первый Этап', 'stat' => 'Второй Этап', 'keywords' => 'Третий Этап', 're-linking' => 'Четвертый Этап'];

			    foreach ( $pages as $key => $page) {
			    	
			    	$state = 0;
			    	if ( $key == 'token' ) {
			    		$state = 1;
			    	}	

				    $data_insert = array(
				        'menu_item_name' => $page,
				        'menu_item_slug' => $key,
				        'state' => $state,
				    );

				    $format = array( '%s', '%s', '%d');
				    $insert = $this->wpdb->insert( $table_menu, $data_insert, $format );
			    }
			}

			//CLICKS
			$table_clicks = $this->wpdb->prefix . "alice_click_stat";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_clicks . "'", ARRAY_A);
			if ( empty( $table_exists ) ) {
			    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			    $sql_request = "CREATE TABLE " . $table_clicks . " (
			        id int(11) NOT NULL AUTO_INCREMENT,
			  	 	post_id int(254) NOT NULL,
			  	 	keyword text NOT NULL,
			  	 	url text NOT NULL,
			  	 	count_clicks int(254) NOT NULL,
			        PRIMARY KEY(id))
			        ENGINE = INNODB
			        DEFAULT CHARACTER SET = utf8
			        COLLATE = utf8_general_ci";
			    $dbDelta = dbDelta($sql_request);
			}

			$table_recent_posts = $this->wpdb->prefix . "alice_recent_posts_settings";
			$table_recent_posts_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_recent_posts . "'", ARRAY_A);

			if ( empty( $table_recent_posts_exists ) ) {
				require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
				$sql_request = "CREATE TABLE " . $table_recent_posts . " (
				    id int(11) NOT NULL AUTO_INCREMENT,
				    count_posts int(254) NOT NULL,
				    relation_area text NOT NULL,
				    post_thumb text NOT NULL,
				    thumb_size int(254) NOT NULL,
				    thumb_size_height int(254) NOT NULL,
				    title_font_size int(254) NOT NULL,
				    block_layout text NOT NULL,
    				post_description text NOT NULL,
    				excerpt_size int(254) NOT NULL,
    				blocks_title text NOT NULL,
    				enable_recent_posts text NOT NULL,
				    PRIMARY KEY(id))
				    ENGINE = INNODB
				    DEFAULT CHARACTER SET = utf8
				    COLLATE = utf8_general_ci";
				$dbDelta = dbDelta($sql_request);

				$data_insert_recent = array(
				    'count_posts' => 2,
				    'relation_area' => 'all_site',
				    'post_thumb' => 'thumbnail',
				    'thumb_size' => 180,
				    'thumb_size_height' => 180,
				    'title_font_size' => 14,
				    'block_layout' => 'left_img',
				    'post_description' => 'only_title',
				    'excerpt_size' => 70,
				    'blocks_title' => __('Похожие записи', 'alice'),
				    'enable_recent_posts' => 'off'
				);

				$format_recent = array( '%d', '%s', '%s', '%d', '%d', '%d', '%s', '%s', '%d', '%s', '%s' );

				$insert = $this->wpdb->insert( $table_recent_posts, $data_insert_recent, $format_recent );
			}

			$table_clicks_count_sort = $this->wpdb->prefix . "alice_clicks_count_sort";
			$table_clicks_count_sort_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_clicks_count_sort . "'", ARRAY_A);

			if ( empty( $table_clicks_count_sort_exists ) ) {
				require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
				$sql_request = "CREATE TABLE " . $table_clicks_count_sort . " (
				    id int(11) NOT NULL AUTO_INCREMENT,
				    sort text NOT NULL,
				    PRIMARY KEY(id))
				    ENGINE = INNODB
				    DEFAULT CHARACTER SET = utf8
				    COLLATE = utf8_general_ci";
				$dbDelta = dbDelta($sql_request);

				$data_insert_recent = array(
				    'sort' => 'DESC'
				);

				$format_recent = array( '%s' );

				$insert = $this->wpdb->insert( $table_clicks_count_sort, $data_insert_recent, $format_recent );
			}

			update_option('link_size', '16px');
			update_option('color1', '1E73BE');
			update_option('enable_preview', 'off');
			update_option('preview_border_color', 'DEDEDE');
			update_option('preview_bg_color', 'DEDEDE');
			update_option('preview_font_color', '000000');
			update_option('preview_font_size', '14px');
			update_option('preview_img_size', 55);
		}

		public function alice_count_clicks()
		{
			$table = $this->wpdb->prefix . "alice_click_stat";
			$post_id = (int)$_POST['page_id'];
			$keyword = esc_sql( $_POST['keyword'] );
			$href = esc_sql( $_POST['url'] );

			$count_clicks = (int)$this->wpdb->get_results( "SELECT `count_clicks` FROM $table WHERE `post_id` = $post_id AND `keyword` = '$keyword' AND `url` = '$href'", ARRAY_A )[0]['count_clicks'];

			if ( $count_clicks !== 0 ) {
				
				++$count_clicks;

				$data = array(
				    'count_clicks' => $count_clicks
				);
				
				$where = array( 'post_id' => $post_id, 'keyword' => $keyword, 'url' => $href );

				$response = $this->wpdb->update( $table, $data, $where );


			}else{

				$data = array(
				    'post_id' => $post_id,
				    'keyword' => $keyword,
				    'count_clicks' => 1,
				    'url' => $href
				);

				$format = array( '%d', '%s', '%d', '%s' );
				
				$response = $this->wpdb->insert( $table, $data, $format );
			}

			echo json_encode(['status' => $response]);

			wp_die();
		}

		public function check_post_in_stat_table( $post_id, $post, $update )
		{
			$post_type = get_post_type($post_id);

			if ( isset( $_POST['alice_use_post_in_link'] ) && $_POST['alice_use_post_in_link'] == 'on' ) {
				$alice_use_post_in_link = 'on';				
			}else{
				$alice_use_post_in_link = 'off';
			}
			
			if ( $alice_use_post_in_link == 'off' || $post_type == 'revision'  )
				return;

			$table = $this->wpdb->prefix . "alice_page_stats";

			$result_post_id = $this->wpdb->get_results( "SELECT `post_id` FROM $table WHERE `post_id` = $post_id", ARRAY_A)[0];

			$title = get_the_title( $post_id );
			$table = $this->wpdb->prefix . 'alice_page_stats';
			$alice_class = new Search_Connections();
			$content_post = get_post( $post_id );
			$content_post = $content_post->post_content;

			if ( $content_post !== null || !empty( $content_post ) ) {

				$content = apply_filters('the_content', $content_post);
				$post_content = str_replace(']]>', ']]&gt;', $content);

				$post_content = preg_replace("/('|\"|\r?\n)/", '', $post_content);
		        $post_content = preg_replace("/\n/", '', $post_content);
		        $post_content = str_replace( ' ', '', $post_content);
		        $post_content = str_replace( '  ', '', $post_content);
		        $count_chars = mb_strlen($post_content,'UTF-8');

			} else {

				$count_chars = 0;

			}

			if ( isset( $_POST['post_admin_keywords'] ) && !empty( $_POST['post_admin_keywords'] ) ) {
				$keywords = isset($_POST['post_admin_keywords']) ? $_POST['post_admin_keywords'] : array();
				$keywords = explode( "\n", $keywords);
				$keywords = serialize($keywords);
			}else{
				$keywords = '';
			}

			if ( !isset($result_post_id) ) {
				
				$data_insert = array(
				    'post_id' => $post_id,
				    'post_type' => $post_type,
				    'size_post' => $count_chars,
				    'count_in_link' => 0,
				    'count_out_link' => 0,
				    'count_external' => 0,
				    'keys_from_server' => '',
				    'keys_from_admin' => $keywords,
				    'keys_from_step' => '',
				    'custom_user_keywords' => '',
				    'checkbox' => 1,
				    'is_save' => 0,
				    'state' => '',
				);

				$format = array( '%d', '%s', '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%d', '%d', '%s' );
				$insert = $this->wpdb->insert( $table, $data_insert, $format );

			}elseif ( isset($result_post_id['post_id']) && !empty($result_post_id['post_id']) ) {
				
				$data = array(
				    'post_type' => $post_type,
				    'size_post' => (int)$count_chars
				);
				

				$where = array( 'post_id' => $post_id );
				$update = $this->wpdb->update( $table, $data, $where );
			}

			$this->get_count_of_links();

			$kpi = 0;
			$table_kpi = $this->wpdb->prefix . "kpi_seo_plugin";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '".$table_kpi."'", ARRAY_A);
			$url = get_permalink( $post_id );

			if ( !empty( $table_exists ) )
				$kpi = $this->wpdb->get_results("SELECT kpi_seo FROM $table_kpi WHERE `post_id` = $post_id", ARRAY_A)[0]['kpi_seo'];

			$keywords_arr = array_unique( $alice_class->setColl($title) );
			if ( !empty( $keywords_arr ) ){
				$keywords_arr = serialize( $keywords_arr );
			}else{
				$keywords_arr = '';
			}

			$insert = $this->wpdb->query( "UPDATE $table SET `keys_from_server` = '$keywords_arr' WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type' AND keys_from_server = ''" );
			
			$counts = $this->wpdb->get_results("SELECT `count_in_link`, `count_out_link`, `size_post` FROM $table WHERE `post_id` = $post_id AND `post_type` = '$post_type'", ARRAY_A)[0];

			$posts[] = array(
				'title' => $title,
				'token' => $this->alice_access_token,
				'article_id' => $post_id,
				'url' => $url,
				'publish_date' => get_the_date('Y-m-d H:i:s', $post_id),
				'count_input_link' => (int)$counts['count_in_link'],
				'count_output_link' => (int)$counts['count_out_link'],
				'count_symbols' => (int)$counts['size_post'],
				'kpi' => (int)$kpi,
				'status' => 'new'
			);

			$url = 'https://alice.2seo.pro/api/setStats';
			$data_string = json_encode($posts);			                                                                                                                     
			$ch = curl_init($url);                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   
			                                                                                                                     
			$result = curl_exec($ch);
		}

		public function delete_post_in_stat_table ( $postid ) {
			
			$table = $this->wpdb->prefix . "alice_page_stats";
			$table_cross = $this->wpdb->prefix . "alice_cross_links";

			$this->wpdb->delete( $table, array( 'post_id' => $postid ) );
			$this->wpdb->delete( $table, array( 'id_post_out' => $postid ) );
			$this->wpdb->delete( $table, array( 'id_post_in' => $postid ) );
		}

		public function create_initial_rest_routes() 
		{
		    $controller = new ALICE_Rest_Api();
		    $controller->register_routes();
		}

		public function alice_create_tables()
		{
			$table = $this->wpdb->prefix . "alice_page_stats";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table . "'", ARRAY_A);

			if ( empty( $table_exists ) ) {
			    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			    $sql_request = "CREATE TABLE " . $table . " (
			        id int(11) NOT NULL AUTO_INCREMENT,
			        post_id int(254) NOT NULL,
			        post_type text NOT NULL,
			        size_post int(254) NOT NULL,
			        count_in_link int(254) NOT NULL,
			        count_out_link int(254) NOT NULL,
			        count_external int(254) NOT NULL,
			        keys_from_server text NOT NULL,
			        keys_from_admin text NOT NULL,
			        keys_from_step text NOT NULL,
			        custom_user_keywords text NOT NULL,
			        checkbox int(254) NOT NULL,
			        is_save bit NOT NULL,
			        state text NOT NULL,
			        PRIMARY KEY(id))
			        ENGINE = INNODB
			        DEFAULT CHARACTER SET = utf8
			        COLLATE = utf8_general_ci";
			    $dbDelta = dbDelta($sql_request);

			    $site_types = get_post_types();

			    unset( $site_types['attachment'] );
			    unset( $site_types['revision'] );
			    unset( $site_types['nav_menu_item'] );
			    unset( $site_types['custom_css'] );
			    unset( $site_types['customize_changeset'] );
			    unset( $site_types['oembed_cache'] );

			    $all_posts_ids = [];

			    foreach ( $site_types as $key => $post_type ) {
			        $args = array(
			            'post_type' => $post_type,
			            'post_status' => 'publish',
			            'posts_per_page' => -1,
			            'fields' => 'ids'
			        );

			        $site_posts_group = get_posts( $args );
			        $all_posts_ids[$post_type] = $site_posts_group;
			    }
			    
			    // $args = array(
			    //     'taxonomy' => 'category',
			    //     'hide_empty' => false,
			    //     'fields' => 'ids'
			    // );

			    // $terms = get_terms( $args );

			    // $all_posts_ids['category'] = $terms;

			    // $args = array(
			    //     'hide_empty' => false,
			    //     'fields' => 'ids'
			    // );

			    // $tags = get_tags( $args );
			    
			    // $all_posts_ids['post_tag'] = $tags;

			    foreach ( $all_posts_ids as $key_post_type => $post_ids ) {

			    	foreach ( $post_ids as $key => $post_id ) {
			    		
			    		if ( $key_post_type === 'post_tag' || $key_post_type === 'category' ) {
			    			
			    			$term = get_term( $post_id );
			    			
			    			if ( $term !== NULL ) {

			    			    if ( isset($term->taxonomy) ) {
			    			        $post_type = $term->taxonomy;
			    			        $post_id = $term->term_id;

			    			        if ( $post_type == 'post_tag' ) {
			    			            $count_days = '';
			    			        }
			    			    }

			    			    $content_post = $term->description;
			    			}

			    		} else {
			    			
			    			$content_post = get_post( $post_id );
			    			$content_post = $content_post->post_content;
			    			$post_type = get_post_type( $post_id );
			    			
			    		}

			    		if ( $content_post !== null || !empty( $content_post ) ) {

			    			$content = apply_filters('the_content', $content_post);
			    			$post_content = str_replace(']]>', ']]&gt;', $content);
			    			// $count_chars = strlen( $post_content );

	    					$post_content = preg_replace("/('|\"|\r?\n)/", '', $post_content);
	    			        $post_content = preg_replace("/\n/", '', $post_content);
	    			        $post_content = str_replace( ' ', '', $post_content);
	    			        $post_content = str_replace( '  ', '', $post_content);
	    			        $count_chars = mb_strlen($post_content,'UTF-8');
			    
			    		} else {

			    			$count_chars = 0;

			    		}

			    		$data_insert = array(
			    		    'post_id' => $post_id,
			    		    'post_type' => $post_type,
			    		    'size_post' => (int)$count_chars,
			    		    'count_in_link' => 0,
			    		    'count_out_link' => 0,
			    		    'count_external' => 0,
			    		    'keys_from_server' => '',
			    		    'keys_from_admin' => '',
			    		    'keys_from_step' => '',
			    		    'custom_user_keywords' => '',
			    		    'checkbox' => 0,
			    		    'is_save' => 0,
			    		    'state' => ''
			    		);

			    		$format = array( '%d', '%s', '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%d', '%d', '%s' );
			    		$insert = $this->wpdb->insert( $table, $data_insert, $format );
			    	}//END $post_ids
			    }//END $all_post_ids
			}

			// Settings Table
			$table_settings = $this->wpdb->prefix . "alice_filter_settings";
			$table_r_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_settings . "'", ARRAY_A);

			if ( empty( $table_r_exists ) ) {
			    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			    $sql_request = "CREATE TABLE " . $table_settings . " (
			        id int(11) NOT NULL AUTO_INCREMENT,
			        id_row int(254) NOT NULL,
			        is_post bit NOT NULL,
			        is_page bit NOT NULL,
			        is_category bit NOT NULL,
			        is_tag bit NOT NULL,
			        categories_selected text NOT NULL,
			        count_out_more int(254) NOT NULL,
			        count_out_less int(254) NOT NULL,
			        recom_link int(254) NOT NULL,
			        min_symb int(254) NOT NULL,
			        stop_keys text NOT NULL,
			        stop_pref text NOT NULL,
			        2word text NOT NULL,
			        3word text NOT NULL,
			        4word text NOT NULL,
			        cut_off_the_pretexts text NOT NULL,
			        direct_entry text NOT NULL,
			        end_selection text NOT NULL,
			        small_entry text NOT NULL,
			        references_to_the_first_article text NOT NULL,
			        min_distance_link int(254) NOT NULL,
			        one_rubric text NOT NULL,
			        count_link int(254) NOT NULL,
			        metrica_ya text NOT NULL,
			        google_analitics text NOT NULL,
			        live_internet text NOT NULL,
			        keyword_from_h1 text NOT NULL,
			        PRIMARY KEY(id))
			        ENGINE = INNODB
			        DEFAULT CHARACTER SET = utf8
			        COLLATE = utf8_general_ci";
			    $dbDelta = dbDelta($sql_request);
			   
				$data_insert = array(
				    'id_row' => 111,
				    'is_post' => 1,
				    'is_page' => 1,
				    'is_category' => 0,
				    'is_tag' => 0,
				    'categories_selected' => '',
				    'count_out_more' => 0,
				    'count_out_less' => 0,
				    'recom_link' => 0,
				    'min_symb' => 0,
				    'stop_keys' => '',
				    'stop_pref' => '',
				    '2word' => 'on',
				    '3word' => 'on',
				    '4word' => 'off',
				    'cut_off_the_pretexts' => 'off',
				    'direct_entry' => 'on',
				    'end_selection' => 'on',
				    'small_entry' => 'off',
				    'references_to_the_first_article' => '1',
				    'min_distance_link' => 500,
				    'one_rubric' => 'off',
				    'count_link' => 500,
				    'metrica_ya' => '',
				    'google_analitics' => '',
				    'live_internet' => '',
				    'keyword_from_h1' => 'on'
				);

				$format = array( '%d', '%d', '%d', '%d', '%d', '%s', '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s', '%d', '%s', '%s', '%s', '%s');
				$insert = $this->wpdb->insert( $table_settings, $data_insert, $format );
			}

			// Cross links Table
			$table_cross_links = $this->wpdb->prefix . "alice_cross_links";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $table_cross_links . "'", ARRAY_A);

			if ( empty( $table_exists ) ) {
			    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			    $sql_request = "CREATE TABLE " . $table_cross_links . " (
			        id int(11) NOT NULL AUTO_INCREMENT,
			        id_post_in int(254) NOT NULL,
			        id_post_out int(254) NOT NULL,
			  	 	keyword text NOT NULL,
			  	 	start int(254) NOT NULL,
			  	 	finish int(254) NOT NULL,
			  	 	context text NOT NULL,
			  	 	status bit NOT NULL,
			  	 	state bit NOT NULL,
			  	 	date_post text NOT NULL,
			  	 	token text NOT NULL,
			        PRIMARY KEY(id))
			        ENGINE = INNODB
			        DEFAULT CHARACTER SET = utf8
			        COLLATE = utf8_general_ci";
			    $dbDelta = dbDelta($sql_request);
			}

			//Cron connections
			$cron_links = $this->wpdb->prefix . "alice_connections_from_server";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '" . $cron_links . "'", ARRAY_A);
			
			if ( empty( $table_exists ) ) {
			    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			    $sql_request = "CREATE TABLE " . $cron_links . " (
			        id int(11) NOT NULL AUTO_INCREMENT,
			        connection_links_plans_id int(254) NOT NULL,
			        connections_id int(254) NOT NULL,
			        connection_links_id int(254) NOT NULL,
			        project_id int(254) NOT NULL,
			        input_article_id int(254) NOT NULL,
			        output_article_id int(254) NOT NULL,
			  	 	text text NOT NULL,
			  	 	status int(254) NOT NULL,
			  	 	processing int(254) NOT NULL,
			  	 	status_errors text NOT NULL,
			        PRIMARY KEY(id))
			        ENGINE = INNODB
			        DEFAULT CHARACTER SET = utf8
			        COLLATE = utf8_general_ci";
			    $dbDelta = dbDelta($sql_request);
			}
		}

		public function alice_enqueue_styles()
		{
			wp_enqueue_style( 'KpiSeoBootStrapStylesheet', plugins_url('bootstrap/css/bootstrap.css', __FILE__), array(), filemtime(__DIR__.'/bootstrap/css/bootstrap.css') );

			wp_enqueue_style( 'datatables-css', plugins_url('css/datatables.min.css', __FILE__), array(), filemtime(__DIR__.'/css/datatables.min.css') );
			wp_enqueue_style( 'datatables-css' );

			wp_register_style( 'alice-css', plugin_dir_url(__FILE__) . 'css/alice.css', array(), filemtime(__DIR__.'/css/alice.css') );
			wp_enqueue_style( 'alice-css' );
		}

		public function alice_enqueue_front_styles()
		{

			if ( file_exists( ABSPATH . 'wp-content/plugins/alice/css/alice_front.css' ) ) {
				wp_register_style( 'alice_front-css', plugin_dir_url(__FILE__) . 'css/alice_front.css', array(), filemtime(__DIR__.'/css/alice_front.css') );
				wp_enqueue_style( 'alice_front-css' );
			}

			wp_register_style( 'alice_related_posts_css', plugin_dir_url(__FILE__) . 'css/alice_recent_posts.css', array(), filemtime(__DIR__.'/css/alice_recent_posts.css') );
			wp_enqueue_style( 'alice_related_posts_css' );
		}

		public function alice_enqueue_front_scripts()
		{

			wp_register_script( 'alice-front-js', plugin_dir_url(__FILE__) . 'js/alice-front.js', array('jquery'), filemtime(__DIR__.'/js/alice-front.js'), true );
			wp_enqueue_script( 'alice-front-js' );
		}

		public function alice_enqueue_scripts()
		{	
			wp_register_style( 'mCustomScrollbar', plugin_dir_url(__FILE__) . 'css/jquery.mCustomScrollbar.css', array(), filemtime(__DIR__.'/css/jquery.mCustomScrollbar.css') );
			wp_enqueue_style( 'mCustomScrollbar' );

			// "jquery.mCustomScrollbar.concat.min.js"
			wp_register_script( 'mCustomScrollbar', plugin_dir_url(__FILE__) . 'js/jquery.mCustomScrollbar.concat.min.js', array('jquery'), filemtime(__DIR__.'/js/jquery.mCustomScrollbar.concat.min.js'), true );
			wp_enqueue_script( 'mCustomScrollbar' );

			wp_register_style( 'select2css', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css', false, '1.0', 'all' );
			wp_register_script( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_style( 'select2css' );
			wp_enqueue_script( 'select2' );

			wp_register_script( 'color-js', plugin_dir_url(__FILE__) . 'js/jscolor.js', array(), false, true );
			wp_enqueue_script( 'color-js' );

			// подключаем IRIS
			wp_register_script( 'datatables-js', plugin_dir_url(__FILE__) . 'js/datatables.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'datatables-js' );

			wp_register_script( 'alice-js', plugin_dir_url(__FILE__) . 'js/alice.js', array('jquery'), filemtime(__DIR__.'/js/alice.js'), true );
			wp_enqueue_script( 'alice-js' );
		}

		public function alice_add_admin_page()
		{

			$menu_data = $this->wpdb->get_results("SELECT `menu_item_slug`, `state` FROM $this->table_menu", ARRAY_A);

			add_menu_page( 'Alice', 'Alice', 'edit_pages', 'wp-alice', array( $this ,'alice_admin_page_action' ), plugins_url( 'alice/img/icon.png' ), 30 );

			add_submenu_page('wp-alice', 'Описание', 'Описание', 'edit_pages', 'wp-alice', array( $this ,'alice_admin_page_action' ) );
			add_submenu_page('wp-alice', 'Первый этап', 'Соединение с сервисом', 'edit_pages', 'wp-alice-token', array( $this ,'alice_page_token_action' ) );
			
			if (  $menu_data[1]['state'] != 0 ) {
				add_submenu_page('wp-alice', 'Утверждение статей', 'Утверждение статей', 'edit_pages', 'wp-alice-stat', array( $this ,'alice_page_stat_action' ) );
			}

			if (  $menu_data[2]['state'] != 0 ) {
				add_submenu_page('wp-alice', 'Утверждение ключей', 'Утверждение ключей', 'edit_pages', 'wp-alice-keywords', array( $this ,'alice_page_keywords_action' ) );
				add_submenu_page('wp-alice', 'Статус простановки', 'Статус простановки', 'edit_pages', 'wp-alice-re-linking', array( $this ,'alice_page_relinking_action' ) );
				add_submenu_page('wp-alice', 'Управление классами ссылок', 'Управление классами ссылок', 'edit_pages', 'wp-alice-import', array( $this ,'alice_page_import_action' ) );
			}

			if (  $menu_data[1]['state'] != 0 ) {
				add_submenu_page('wp-alice', 'Настройки', 'Настройки', 'edit_pages', 'wp-alice-css', array( $this ,'alice_admin_page_action_css' ) );
				add_submenu_page('wp-alice', 'Статистика кликов', 'Статистика кликов', 'edit_pages', 'wp-alice-clicks-stat', array( $this ,'alice_clicks_stat_action' ) );			
			}	
			
			add_submenu_page('wp-alice', 'Похожие записи', 'Похожие записи', 'edit_pages', 'wp-alice-recent-posts', array( $this ,'alice_recent_post_action' ) );
		}

		public function wpse_60168_custom_menu() 
		{
			header('Content-type: text/html; charset=utf-8');
		    global $menu;

		    $menu_data = $this->wpdb->get_results("SELECT `menu_item_slug`, `state` FROM $this->table_menu", ARRAY_A);

		    foreach( $menu as $key => $value )
		    {

		        if( 'wp-alice' == $value[2] && $menu_data[1]['state'] == 1 && $menu_data[2]['state'] == 1 && $menu_data[3]['state'] == 0 ){

		        	// if ( $menu_data[1]['state'] == 0 && $menu_data[2]['state'] == 0 && $menu_data[3]['state'] == 0 ) {
		         //    	$menu[$key][4] .= " disable_second_stage disable_third_stage disable_fourth_stage";
		        		
		        	// }elseif ( $menu_data[1]['state'] == 1 && $menu_data[2]['state'] == 0 && $menu_data[3]['state'] == 0 ){

		         //    	$menu[$key][4] .= " disable_third_stage disable_fourth_stage";

		        	// }elseif ( $menu_data[1]['state'] == 1 && $menu_data[2]['state'] == 1 && $menu_data[3]['state'] == 0 ) {
		            	$menu[$key][4] .= " disable_fourth_stage";
		        	// }
		        }
		    }
		}

		public function alice_page_token_action()
		{
			
			if ( !empty($_POST) && wp_verify_nonce($_POST['wp_nonce_alice-token'], 'wp_nonce_alice-token-id') && isset( $_POST['alice-access-token'] ) ) {

				$all_posts_ids = $this->get_site_post_types();
				$table_config = $this->wpdb->prefix . "alice_filter_settings";

				$url = 'https://alice.2seo.pro/api/checkConnect';
				// $url = 'http://alis.2dev.pro/public/api/checkConnect';

				if ( (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') || (! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
				    $server_request_scheme = 'https';
				} else {
				    $server_request_scheme = 'http';
				}

				$response = wp_remote_get( $url . '?token=' . $_POST['alice-access-token'] . '&url=' . $server_request_scheme . '://' . $_SERVER['SERVER_NAME'] );
				$body = json_decode( $response['body'] );
				// var_dump( $body );
				update_option('alice_access_token', $_POST['alice-access-token']);
				
				if ( isset($body->status) && $body->status == 200 ) {
						
					$this->alice_create_tables();

					$this->wpdb->query("UPDATE $this->table_menu SET `state` = 1 WHERE `menu_item_slug` = 'stat'");

					$this->get_count_of_links();
					// var_dump($body->status);

					// echo '<div class="modal_message success">' . $body->message . '</div>';
					wp_redirect( admin_url('admin.php?page=wp-alice-stat'), '301' );

				}elseif ( isset($body->status) && $body->status == 422 ) {
					if (isset($_GET['noheader']))
						require_once(ABSPATH . 'wp-admin/admin-header.php');

					if ( isset( $body->token[0] ) && !empty( $body->token[0] ) ) {

						echo  '<div class="modal_message error">' . $body->token[0] . '</div>';

					}elseif ( isset( $body->url[0] ) && !empty( $body->url[0] ) ) {
						
						echo  '<div class="modal_message error">' . $body->url[0] . '</div>';

					}

				}else{
					if (isset($_GET['noheader']))
						require_once(ABSPATH . 'wp-admin/admin-header.php');

					echo  '<div class="modal_message error">Что-то пошло не так</div>';

				}
			}
			
			include_once('templates/settings.php');
		}

		public function alice_page_stat_action()
		{
			include_once('templates/stat.php');
		}

		public function alice_page_keywords_action()
		{

			if ( isset($_POST['update_keywords']) ) {
				$alice_class = new Search_Connections();

				$table = $this->wpdb->prefix . "alice_page_stats";
				$table_settings = $this->wpdb->prefix . "alice_filter_settings";

				$posts = $this->wpdb->get_results( "SELECT * FROM $table WHERE `checkbox` = 1", ARRAY_A );
				foreach ($posts as $key => $post) {
					
					$post_id = $post['post_id'];
					$post_type = $post['post_type'];

					$title = get_the_title( $post_id );
					$url = get_permalink( $post_id );

					$keywords_arr = array_unique( $alice_class->setColl($title) );
					
					if ( !empty( $keywords_arr ) ){
						$keywords_arr = serialize( $keywords_arr );
					}else{
						$keywords_arr = '';
					}
					

					$insert = $this->wpdb->query( "UPDATE $table SET `keys_from_server` = '$keywords_arr' WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'" );
				}

				wp_redirect( admin_url('admin.php?page=wp-alice-keywords'), '301' );
			} 

			include_once('templates/keywords.php');
		}

		public function alice_page_import_action()
		{
			$this->import = new ImportLinksToAlice();
		}

		public function alice_page_relinking_action()
		{
			include_once('templates/re-linking.php');
		}

		public function get_clicks( $limit )
		{
			$table = $this->wpdb->prefix . "alice_click_stat";

			if ( isset( $_GET['clicks-stat-page'] ) && !empty( $_GET['clicks-stat-page'] ) ) {
				$page = $_GET['clicks-stat-page']; 
			}else{
				$page = 1; 
			}

			if ( $page == 1 ) {
				$offset = 0;
			}else{
				$offset = ($page-1)*$limit;
			}

			$table_sort = $this->wpdb->prefix . "alice_clicks_count_sort";

			$sort = $this->wpdb->get_results( "SELECT `sort` FROM {$table_sort}", ARRAY_A)[0]['sort'];

			$clicks = $this->wpdb->get_results( "SELECT * FROM {$table} ORDER BY `count_clicks` {$sort} LIMIT {$limit} OFFSET {$offset}", ARRAY_A);

			return $clicks;
		}

		public function alice_clicks_stat_action ()
		{

			$table = $this->wpdb->prefix . "alice_click_stat";
			$limit = 50;

			if ( isset($_POST['count_clicks_sort']) ) {
				$table_sort = $this->wpdb->prefix . "alice_clicks_count_sort";
				$sort = $this->wpdb->get_results( "SELECT `sort` FROM {$table_sort}", ARRAY_A)[0]['sort'];
				
				if ( $sort == 'DESC' ) {
					$data = array('sort' => 'ASC');
					$data = 'ASC';
				}elseif ( $sort == 'ASC' ) {
					$data = array('sort' => 'DESC');
					$data = 'DESC';
				}

				$this->wpdb->query("UPDATE {$table_sort} SET `sort` = '$data'");
			}

			$clicks = $this->get_clicks($limit);

			$count_rows = (int)$this->wpdb->get_results( "SELECT COUNT(*) FROM {$table}", ARRAY_A)[0]["COUNT(*)"];

			include_once('templates/clicks-stat.php');
		}

		public function alice_recent_post_action ()
		{
			$table = $this->wpdb->prefix . "alice_recent_posts_settings";

			$settings = $this->wpdb->get_results( "SELECT * FROM {$table}", ARRAY_A)[0];

			include_once('templates/recent-posts.php');
		}

		public function alice_admin_page_action_css ()
		{	
			$table_added_connections = $this->wpdb->prefix . "alice_connections_from_server";
			
			// var_dump($_POST);

			if ( !empty($_POST) && wp_verify_nonce($_POST['wp_nonce_alice-style-css'], 'wp_nonce_alice-style-css-id') ) {

				if ( isset($_POST['enable_preview']) && !empty($_POST['enable_preview'])) {
					$enable_preview = update_option('enable_preview', $_POST['enable_preview']);

					if ( isset($_POST['preview_border_color']) && !empty($_POST['preview_border_color']))
						$preview_border_color = update_option( 'preview_border_color', $_POST['preview_border_color'] );

					if ( isset($_POST['preview_font_color']) && !empty($_POST['preview_font_color']))
						$preview_font_color = update_option( 'preview_font_color', $_POST['preview_font_color'] );

					if ( isset($_POST['preview_font_size']) && !empty($_POST['preview_font_size']))
						$preview_font_size = update_option( 'preview_font_size', $_POST['preview_font_size'] );

					if ( isset($_POST['preview_bg_color']) && !empty($_POST['preview_bg_color']))
						$preview_bg_color = update_option( 'preview_bg_color', $_POST['preview_bg_color'] );

					if ( isset($_POST['preview_img_size']) && !empty($_POST['preview_img_size'])){
						$preview_img_size = update_option( 'preview_img_size', $_POST['preview_img_size'] );
					}else{
						update_option( 'preview_img_size', 55 );
					}
				}else{
					$enable_preview = update_option('enable_preview', 'off');
				}
				

				if ( isset($_POST['link_size']) && !empty($_POST['link_size']) && isset($_POST['color1']) && !empty($_POST['color1']) && (strpos($_POST['link_size'], 'px') !== false) ) {

					$link_size = update_option('link_size', $_POST['link_size']);
					$color1 = update_option('color1', $_POST['color1']);


					// var_dump("expression", $_POST['enable_preview']);

					if ( isset($_POST['custom_css_class']) ) {

						// $old_custom_css_class =  get_option('custom_css_class') ? ' ' . get_option('custom_css_class') : '';
						$custom_css_class = update_option( 'custom_css_class', $_POST['custom_css_class'] );
						// $custom_class = get_option('custom_css_class') ? ' ' . get_option('custom_css_class') : '';

						//$posts_id = $this->wpdb->get_results( "SELECT * FROM {$table_added_connections}", ARRAY_A );
						$this->custom_alice_class( $custom_css_class );	
						// if ( $old_custom_css_class !== $custom_class ) {
							// foreach ( $posts_id as $key => $post) {
								
							// 	$id_post_out = $post['output_article_id'];

							// 	$content_post = get_post( $id_post_out );
							// 	$content = $content_post->post_content;
							// 	$content = apply_filters('the_content', $content);
							// 	$content = str_replace(']]>', ']]&gt;', $content);

							// 	$search = '<a class="alice_link'.$old_custom_css_class.'" target="_blank" href="' . get_permalink( $post['input_article_id'] ) . '">' . $post['text'] . '</a><span class="alice_link_span">' . $id_post_out . '</span>';

							// 	$replace = '<a class="alice_link'.$custom_class.'" target="_blank" href="' . get_permalink( $post['input_article_id'] ) . '">' . $post['text'] . '</a><span class="alice_link_span">' . $id_post_out . '</span>'; 

							// 	$res = str_replace($search, $replace, $content);


							// 	// Создаем массив данных
							// 	$my_post = array();
							// 	$my_post['ID'] = $id_post_out;
							// 	$my_post['post_content'] = $res;

							// 	// Обновляем данные в БД
							// 	$wp_update = wp_update_post( wp_slash($my_post) );
							// 	file_put_contents(__DIR__."/log.txt", 'wp_update ' . var_export( $wp_update, true ) . "\n", FILE_APPEND);
							// }
						// }
					}

					$data = ".alice_link{position:relative;color: #" . $_POST['color1'] . "!important;font-size:" . $_POST['link_size'] . "!important;}.alice_link_span{display:none!important;}";
					file_put_contents( ABSPATH . 'wp-content/plugins/alice/css/alice_front.css', $data . "\n");
					if ( file_exists( ABSPATH . 'wp-content/plugins/alice/css/alice_front.css' ) ) {
						wp_register_style( 'alice_front-css', plugin_dir_url(__FILE__) . 'css/alice_front.css', array(), filemtime(__DIR__.'/css/alice_front.css') );
						wp_enqueue_style( 'alice_front-css' );
					}
					echo '<div class="alert-ms" style="color: green">' . __('Настройки сохранены', 'alice') . '</div>';
				}else{
					echo '<div class="alert-ms">ERROR<div>';
				}

			}
			

			include_once('templates/css-settings.php');
		}

		public function custom_alice_class()
		{
			$custom_css_class = get_option('custom_css_class');
			if ( isset($custom_css_class) && !empty($custom_css_class) ) :
			?>
				<script type="text/javascript">
				    var custom_css_class = '<?php echo $custom_css_class ?>';      
				</script>

			<?php
			endif;
		}

		public function esc_sql_name( $name ) {
		    return str_replace( "`", "``", $name );
		}

		public function get_count_of_links ()
		{	
			$arr_links = array();
			$all_posts_ids = $this->get_site_post_types();
			$table_stats = $this->wpdb->prefix . "alice_page_stats";
			if ( !empty( $all_posts_ids ) ) {

				foreach ( $all_posts_ids as $type => $posts_arr ) {
					foreach ($posts_arr as $post_id) {

						if ( $type === 'post_tag' || $type === 'category' ) {
							
							$term = get_term( $post_id );
							
							if ( $term !== NULL ) {

							    if ( isset($term->taxonomy) ) {
							        $post_type = $term->taxonomy;
							        $post_id = $term->term_id;

							        if ( $post_type == 'post_tag' ) {
							            $count_days = '';
							        }
							    }

							    $content_post = $term->description;
							}

						} elseif ( $type == 'post' || $type == 'page' ) {
							
							$content_post = get_post( $post_id );
							$content_post = $content_post->post_content;
							$post_type = get_post_type( $post_id );
							
						}

						preg_match_all('%<a(.*?)href="http([s])*:\/\/(.*?)([>]{1})(.*?)<\/a>%i', $content_post, $matches, PREG_OFFSET_CAPTURE);
						$count_out_link = 0;
						$count_external = 0;

						if ( isset( $matches[0] ) && !empty( $matches[0] ) ) {
							foreach ($matches[0] as $key => $value) {
								$url = $value[0];

								// file_put_contents(__DIR__."/log.txt", '$url - ' . var_export( strpos($url, '/wp-content/uploads/'), true ) . "\n\n", FILE_APPEND);
								if ( (strpos($url, '<img') === false) && ( preg_match('#href="(.*?)' . $_SERVER['SERVER_NAME'] . '([^><])#', $url) ) && strpos($url, '/wp-content/uploads/') === false ) {
									$count_out_link++;
									
									$arr_links[$post_id][] = $url;

								}elseif ( (strpos($url, '<img') === false)  && ( !preg_match('#href="(.*?)' . $_SERVER['SERVER_NAME'] . '([^><])#', $url) ) && strpos($url, '/wp-content/uploads/') === false ) {
									// file_put_contents(__DIR__."/preg.txt", 'url2 - ' . var_export( $url, true ) . "\n", FILE_APPEND);
									$count_external++;
								}
							}
							$data = array( 'count_out_link' => $count_out_link, 'count_external' => $count_external );
							$where = array( 'post_id' => $post_id );
							$this->wpdb->update($table_stats, $data, $where);
						}
					}
				}	
			} 

			// file_put_contents(__DIR__."/preg.txt", 'arr_links - ' . var_export( $arr_links, true ) . "\n", FILE_APPEND);
			foreach ($arr_links as $post_id_out => $arr_post_url) {
					// file_put_contents(__DIR__."/log.txt", '$post_id_out - ' . var_export( $post_id_out, true ) . "\n", FILE_APPEND);
				foreach ($arr_post_url as $post_url) {
					preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $post_url, $matches);

					if ( isset($matches[2][0]) && !empty($matches[2][0]) ) {
						$post_id_in = url_to_postid( $matches[2][0] );

						// file_put_contents(__DIR__."/log.txt", '$post_id_in - ' . var_export( $post_id_in, true ) . "\n", FILE_APPEND);
						// file_put_contents(__DIR__."/log.txt", '$matches[2][0] - ' . var_export( $matches[2][0], true ) . "\n\n\n", FILE_APPEND);

						if ( $post_id_in !== 0 ) {

							if ( !isset($counts[$post_id_in]) )
								$counts[$post_id_in] = 0;

							$counts[$post_id_in] = $counts[$post_id_in] + 1 ;
						}
					}
				}
			}
			if ( !empty( $arr_links ) ) {
				arsort($counts);

				foreach ( $counts as $post_id => $count_in_link ) {
					$data = array( 'count_in_link' => $count_in_link );
					$where = array( 'post_id' => $post_id );
					$this->wpdb->update($table_stats, $data, $where);
				}
			}
		}

		public function find_links_on_the_site()
		{
			
		}

		public function alice_admin_page_action()
		{	
			include_once('templates/main.php');
		}

		public function check_system()
		{
			include_once('templates/system.php');
		}

		public function send_posts_to_server()
		{
	
			$max_execution_time = ini_set('max_execution_time', 0);	
			$set_time_limit = set_time_limit(0);

			$alice_class = new Search_Connections();

			$table = $this->wpdb->prefix . "alice_page_stats";
			$table_settings = $this->wpdb->prefix . "alice_filter_settings";

			//$anagrams = $this->wpdb->get_results( "SELECT `2word`, `3word`, `4word` FROM $table_settings", ARRAY_A )[0];
			$keyword_from = $this->wpdb->get_results( "SELECT `keyword_from_title`, `keyword_from_h1` FROM $table_settings", ARRAY_A )[0];

			$posts_from_plugin = $_POST['ids'];
			$kpi_exists = false;
			$table_kpi = $this->wpdb->prefix . "kpi_seo_plugin";
			$table_exists = $this->wpdb->get_results("SHOW TABLES LIKE '".$table_kpi."'", ARRAY_A);

			if ( !empty( $table_exists ) )
				$kpi_exists = true;
			
			$posts_from_plugin_chunk = array_chunk($posts_from_plugin, 100);
			$posts_arr_size = count( $posts_from_plugin_chunk );

			$posts['delete'] = false;

			if ( $_POST['flag'] === null ) {
				$this->wpdb->query( "UPDATE $table SET `checkbox` = 0" );
				$number_request = 0;
				$posts['delete'] = true;
			}else{
				$number_request = $_POST['flag'];
			}

			// echo '<pre>'; var_dump($number_request); echo '</pre>';
			// file_put_contents(__DIR__."/log.txt", '$posts_from_plugin[$number_request] ' . var_export( $posts_from_plugin[$number_request], true) . "\n", FILE_APPEND);
			foreach ( $posts_from_plugin_chunk[$number_request] as $key => $post_data ) {
				$kpi = 0;
				$post_id = $post_data['id'];
				// $post_type = $post_data['type'];
				$post_type = get_post_type( $post_id );
				$ids[] = (int)$post_id;

				$counts = $this->wpdb->get_results("SELECT `count_in_link`, `count_out_link`, `size_post` FROM $table WHERE `post_id` = $post_id AND `post_type` = '$post_type'", ARRAY_A)[0];
				
				if ( $kpi_exists )
					$kpi = $this->wpdb->get_results("SELECT kpi_seo FROM $table_kpi WHERE `post_id` = $post_id", ARRAY_A)[0]['kpi_seo'];

				if ( $post_type == 'post' || $post_type == 'page' ) {

					$title = get_the_title( $post_id );
					$url = get_permalink( $post_id );

				}elseif ( $post_type == 'post_tag' || $post_type == 'category' ) {

					$term = get_term( $post_id );
					
					if ( isset( $term->taxonomy ) ) {
					    $post_type = $term->taxonomy;
					    $post_id = $term->term_id;
					    $url = get_term_link( $post_id );

					    if ( $post_type == 'post_tag' ) {
					        $count_days = '';
					        $url = get_tag_link( $post_id );
					    }
					}

					$title = $term->name;
				}

				if ( $keyword_from['keyword_from_h1'] == 'on' ) {

					$keywords_arr_h1 = array_unique( $alice_class->setColl($title) );
				}

				if ( $keyword_from['keyword_from_title'] == 'on' ) {
					$get_the_wp_title = get_post_meta( $post_id, '_yoast_wpseo_title', true );
					$keywords_arr_title = array_unique( $alice_class->setColl($get_the_wp_title) );
				}

				$keywords_arr = array_unique( array_merge( $keywords_arr_h1, $keywords_arr_title ) );
				
				if ( !empty( $keywords_arr ) ){
					$keywords_arr = serialize( $keywords_arr );
				}else{
					$keywords_arr = '';
				}

				$insert = $this->wpdb->query( "UPDATE $table SET `keys_from_server` = '$keywords_arr' WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'" );
			 		// file_put_contents(__DIR__."/log.txt", 'keywords_arr_h1 ' . var_export( $keywords_arr_h1, true) . "\n", FILE_APPEND);

				$this->wpdb->query( "UPDATE $table SET `checkbox` = 1 WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'" );

				$posts['posts'][] = array(
					'title' => $title,
					'token' => $this->alice_access_token,
					'article_id' => $post_id,
					'url' => $url,
					'publish_date' => get_the_date('Y-m-d H:i:s', $post_id),
					'count_input_link' => (int)$counts['count_in_link'],
					'count_output_link' => (int)$counts['count_out_link'],
					'count_symbols' => (int)$counts['size_post'],
					'kpi' => (int)$kpi
				);
			}
           
			$url = 'https://alice.2seo.pro/api/setStats';
			$data_string = json_encode($posts);			                                                                                                                     
			$ch = curl_init($url);                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   
			                                                                                                                     
			$result = curl_exec($ch);

			echo json_encode( ['all_count' => count($posts_from_plugin), 'number_request' => (int)$number_request, 'posts_arr_size' => $posts_arr_size, 'status' => __('Статьи отправлены!', 'alice')] );
			wp_die();
		}

		public function save_keywords_for_post()
		{
			$table = $this->wpdb->prefix . "alice_page_stats";

			if ( isset($_POST['new_keywords']) && !empty($_POST['new_keywords']) && !empty($_POST['post_id']) ) {
				$keywords = $_POST['new_keywords'];

				$keys_from_server = !empty($keywords['keys_from_server']) ? serialize($keywords['keys_from_server']) : '';
				$keys_from_admin = !empty($keywords['keys_from_admin']) ? serialize($keywords['keys_from_admin']) : '';
				$keys_from_step = !empty($keywords['keys_from_step']) ? serialize($keywords['keys_from_step']) : '';
				$custom_user_keywords = !empty($keywords['custom_user_keywords']) ? serialize($keywords['custom_user_keywords']) : '';

				$post_id = $_POST['post_id'];

				$this->wpdb->query( "UPDATE $table SET `keys_from_server` = '$keys_from_server' WHERE `post_id` = $post_id" );
				$this->wpdb->query( "UPDATE $table SET `keys_from_admin` = '$keys_from_admin' WHERE `post_id` = $post_id" );
				$this->wpdb->query( "UPDATE $table SET `keys_from_step` = '$keys_from_step' WHERE `post_id` = $post_id" );
				$this->wpdb->query( "UPDATE $table SET `custom_user_keywords` = '$custom_user_keywords' WHERE `post_id` = $post_id" );
				$this->wpdb->query( "UPDATE $table SET `is_save` = 1 WHERE `post_id` = $post_id" );

			}
			echo json_encode(['status' => 1]);
			wp_die();
		}

		public function settings_form_by_cat_deselect_all()
		{
			$table = $this->wpdb->prefix . "alice_filter_settings";
			$table_stats = $this->wpdb->prefix . "alice_page_stats";
			
			$this->wpdb->query( "UPDATE $table_stats SET `checkbox` = 0" );
			//$this->wpdb->query( "UPDATE $table_stats SET `checkbox` = 0 WHERE `post_id` NOT IN $ids_for_sql" );
			$this->wpdb->query( "UPDATE $table SET `categories_selected` = ''" );
		
			wp_die();
		}

		public function settings_form_by_cat_all()
		{
			$table = $this->wpdb->prefix . "alice_filter_settings";
			$table_stats = $this->wpdb->prefix . "alice_page_stats";
			
			$categories = ( isset($_POST['data']) ) ? $_POST['data'] : [];
			// $categories = $_POST['data'] ?? [];
			$categories = serialize( $categories );
			
			$this->wpdb->query( "UPDATE $table_stats SET `checkbox` = 1" );
			//$this->wpdb->query( "UPDATE $table_stats SET `checkbox` = 0 WHERE `post_id` NOT IN $ids_for_sql" );
			$this->wpdb->query( "UPDATE $table SET `categories_selected` = '$categories'" );
	
			wp_die();
		}

		public function settings_form_by_cat()
		{
			$table = $this->wpdb->prefix . "alice_filter_settings";
			$table_stats = $this->wpdb->prefix . "alice_page_stats";
			
			$categories = ( isset($_POST['data']) ) ? $_POST['data'] : [];
			// $categories = $_POST['data'] ?? [];
			$categories = serialize( $categories );
			
			$ids = ( isset($_POST['ids']) ) ? $_POST['ids'] : [];
			// $ids = $_POST['ids'] ?? [];
			$ids_for_sql = "('" . implode("','", $ids) . "')";

			$this->wpdb->query( "UPDATE $table_stats SET `checkbox` = 1 WHERE `post_id` IN $ids_for_sql" );
			$this->wpdb->query( "UPDATE $table_stats SET `checkbox` = 0 WHERE `post_id` NOT IN $ids_for_sql" );
			$this->wpdb->query( "UPDATE $table SET `categories_selected` = '$categories'" );
	
			wp_die();
		}

		public function check_processing()
		{
			$slug = $_POST['slug'];
            $state = $this->wpdb->get_results("SELECT `state` FROM $this->table_menu WHERE `menu_item_slug` = '$slug'", ARRAY_A)[0]['state'];

            echo json_encode(['state' => $state, 'status' => __('Связи отправлены! <a target="_blank" style="color: green; text-decoration: underline;" href="https://alice.2seo.pro/">Перейдите в Сервис</a>','alice')]);
            wp_die();
		}

		public function check_processing_on_the_server()
		{

			$url = 'https://alice.2seo.pro/api/checkStatsStatus';
			$args = array(
				'method' => 'GET',
				'timeout'     => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking'    => true,
				'headers' => array(),
				'body'    => array( 'token' => $this->alice_access_token ),
				'cookies' => array()
			);

			$response = wp_remote_post( $url, $args );
			$res = $response;

			$this->wpdb->query("UPDATE $this->table_menu SET `state` = 1 WHERE `menu_item_slug` = 'keywords'");
			
			echo json_encode(['response' => $res, 'status' => __('Статьи отправлены!', 'alice')]);
			
			wp_die();
		}

		public function check_count_references_to_article( $id, $post_id, $setting_count )
		{
			$table_cross_links = $this->wpdb->prefix . "alice_cross_links";

			$arr_connection_count = (int)$this->wpdb->get_results( "SELECT COUNT(*) FROM $table_cross_links WHERE `id_post_in` = $id AND `id_post_out` = $post_id", ARRAY_A )[0]['COUNT(*)'];
			// file_put_contents( __DIR__ . "/log.txt", '$arr_connection_count - ' . var_export( $arr_connection_count, true ) . "\n", FILE_APPEND );
			// file_put_contents( __DIR__ . "/log.txt", '$setting_count - ' . var_export( $setting_count, true ) . "\n", FILE_APPEND );
			if ( (int)$arr_connection_count < (int)$setting_count ){
				return true;
			}else {
				return false;
			}
		}

		public function send_connexion_to_server()
		{
			$max_execution_time = ini_set('max_execution_time', 0);	
			$set_time_limit = set_time_limit(0);
			$table = $this->wpdb->prefix . "alice_page_stats";
			$table_cross_links = $this->wpdb->prefix . "alice_cross_links";
			$table_config = $this->wpdb->prefix . "alice_filter_settings";
			// $get_site_post_types = $this->get_site_post_types();
			$config = $this->wpdb->get_results( "SELECT * FROM $table_config", ARRAY_A )[0];
			
			$continue_proc = $_POST['continue_proc'];
			
			//IF FIRST AJAX REQUEST AND NOT NEW PROCESS - CLEAR ALL DATA
			if ( !isset($_POST['time']) && $continue_proc == 'false' ) {

				$posts_for_perelink_origin = $_POST['ids'];
				$delete = $this->wpdb->query("TRUNCATE TABLE $table_cross_links");
				$this->wpdb->query( "UPDATE $table SET `is_save` = 0" );

				file_put_contents( __DIR__."/ids.txt", json_encode($posts_for_perelink_origin) );
				file_put_contents( __DIR__."/posts_with_keywords.txt", json_encode($posts_for_perelink_origin) );
				file_put_contents( __DIR__ . "/posts_for_perelink.txt", json_encode( $posts_for_perelink_origin ) );
			}
			
			$posts_for_perelink_origin = json_decode( file_get_contents(  __DIR__."/ids.txt" ), true );

			$check_data = json_decode( file_get_contents(  __DIR__."/posts_with_keywords.txt" ), true );

			if ( $continue_proc == 'true' && $check_data === null && !isset($_POST['time']) ) {
				echo json_encode(array('status' => __('Невозможно продолжить, так как предыдущий процесс не был прерван!', 'alice'), 'code' => 404));		
				wp_die();
			}
			
			//GET ALL POSTS
			$posts_for_perelink_current = json_decode( file_get_contents(  __DIR__."/posts_for_perelink.txt" ), true );

			if ( (!isset($_POST['same_post']) || $_POST['same_post'] != 'true') || ( $continue_proc == 'true' && (!isset($_POST['same_post']) || $_POST['same_post'] != 'true') && isset($_POST['time']) ) ) {
				file_put_contents( __DIR__ . "/posts_for_perelink.txt", json_encode( $posts_for_perelink_origin ) );
			}

			//GET ALL POSTS
			$posts_for_perelink_current = json_decode( file_get_contents(  __DIR__."/posts_for_perelink.txt" ), true );
			//GET PART OF POSTS FOR LOOP AND CUT THEM FROM ARRAY
			$posts_for_perelink = array_splice( $posts_for_perelink_current, 0, 300 );
			
			//POSTS FOR TO GET THEIR CONTENT
			$data = json_decode( file_get_contents(  __DIR__."/posts_with_keywords.txt" ), true );
			//GET FIRST POST FROM posts_with_keywords.txt WICH CONTENT WILL BE USED TO FIND KEYS
			$curent_post = $data[0];

			$post_id = (int)$curent_post['id'];
			$post_type = $curent_post['type'];
			$this->wpdb->query( "UPDATE $table SET `is_save` = 1 WHERE `post_id` = $post_id AND `post_type` LIKE '$post_type'" );

			$content_post = get_post( $post_id );
			$content_post = $content_post->post_content;
			$content_post = apply_filters('the_content', $content_post);
			$content_post = str_replace(']]>', ']]&gt;', $content_post);	

			foreach  ( $posts_for_perelink as $key => $post_data_clone) {
				$id = (int)$post_data_clone['id'];
				$post_type_clone = $post_data_clone['type'];
				if ( $id != $post_id ) {

					$all_keys = $this->wpdb->get_results( "SELECT `keys_from_server`, `custom_user_keywords`, `keys_from_admin`,`keys_from_step` FROM $table WHERE `post_id` = $id AND `post_type` LIKE '$post_type_clone'", ARRAY_A )[0];
					
					$keys_from_server = $all_keys['keys_from_server'];
					$custom_user_keywords = $all_keys['custom_user_keywords'];
					$keys_from_admin = $all_keys['keys_from_admin'];
					$keys_from_step = $all_keys['keys_from_step'];

					$keys_from_server = !empty($keys_from_server) ? unserialize($keys_from_server) : array();
					$custom_user_keywords = !empty($custom_user_keywords) ? unserialize($custom_user_keywords) : array();
					$keys_from_admin = !empty($keys_from_admin) ? unserialize($keys_from_admin) : array();
					$keys_from_step = !empty($keys_from_step) ? unserialize($keys_from_step) : array();

					$keys_from_server_new = array_merge($keys_from_server, $custom_user_keywords, $keys_from_admin, $keys_from_step );
					// file_put_contents( __DIR__ . "/log.txt", '$post_id - ' . var_export( $post_id, true ) . "\n", FILE_APPEND );

					foreach ( $keys_from_server_new as $key => $keyword ) {
						$pos = explode(' ', $keyword);
						if( $config['direct_entry'] === 'on' ) {
							$pattern = '~(\A|\s)\b' . $keyword . '\b|<(?:(h[1-6]).*?</\1>|[^<>]*>)(*SKIP)(*F)~ui';

							if ($config['allow_subtitles'] === 'on') {
								$pattern = '~\b' . $keyword . '\b~ui';
							}

							if ( preg_match_all( $pattern, $content_post, $outs, PREG_OFFSET_CAPTURE ) ) {
								foreach( $outs[0] as $k => $out ) {
									$pos_start = $out[1];
									$length = iconv_strlen( $out[0], 'UTF-8' ) + 80;
									$found_keyword = trim($out[0]);

									$count_sibling_keywords = (int)$this->wpdb->get_results("SELECT COUNT(*) FROM `$table_cross_links` WHERE `keyword` = '{$found_keyword}' AND `id_post_out` = {$post_id}", ARRAY_A)[0]['COUNT(*)'];

									$current_index_number = $count_sibling_keywords;
									$result = $this->create_context( $content_post, $pattern, $k, $current_index_number, $found_keyword );
									
									// file_put_contents( __DIR__ . "/log.txt", '$key - ' . var_export( $k, true ) . "\n", FILE_APPEND );
									// file_put_contents( __DIR__ . "/log.txt", '$keyword - ' . var_export( $out[0], true ) . "\n\n", FILE_APPEND );
									// file_put_contents( __DIR__ . "/log.txt", '$current_index_number - ' . var_export( $current_index_number, true ) . "\n\n", FILE_APPEND );
									// if ( $this->check_count_references_to_article( $id, $post_id, $out[0] ) ) {
										$arr_connection[] = array(
											'input_article_id' => $id,
											'token' => $this->alice_access_token,
											'output_article_id' => $post_id,//from 
											'start' => $out[1],
											'finish' => $out[1] + strlen( $out[0] ),
											'key' => trim($out[0]), 
											'context' => $result,
											'date_post' => get_the_time('Y-m-d H:i:s', $id)
										);

										// $keyword_t = trim($out[0]);
										$start = $out[1];
										$finish = $out[1] + strlen( $out[0] );
										$date_post = get_the_time('Y-m-d H:i:s', $id);
										$token = $this->alice_access_token;			
										// $insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) VALUES ($id, $post_id, '$keyword_t', $start, $finish, '$result', 0, 0, '$date_post', '$token')");
										$setting_count = $config['references_to_the_first_article'];
										if ( isset($setting_count) && !empty($setting_count) && $setting_count != 'off' ) {
											
											if ( $this->check_count_references_to_article( $id, $post_id, $setting_count ) ) {
												$insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");
											}

										}else{

											$insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");

										}

									// }
								}		
							}
						}

						//если выбран "с подбором окончания"
						if( $config['end_selection'] == 'on' && isset($pos) && !empty($pos) ) {
							$pattern = '~(\A|\s)';

							foreach ($pos as $key => $value) {

								$space = '\b[^.!?].{0,2}';
								if ( $key+1 == count($pos) )
									$space = '';

								$pattern .= '\b' . $this->stremmer->getWordBase($value) .'[а-яА-Яa-zA-Z0-9]{0,20}'.$space;
							}

							if ($config['allow_subtitles'] === 'on') {
							
								$pattern .= '\b~u';
							
							}else{

								$pattern .= '\b|<(?:(h[1-6]).*?</\1>|[^<>]*>)(*SKIP)(*F)~u';
							}

							// $pattern .= '\b~u';
							// file_put_contents( __DIR__ . "/log.txt", '$pattern - ' . var_export( $pattern, true ) . "\n", FILE_APPEND );
							preg_match_all($pattern, $content_post, $outs, PREG_OFFSET_CAPTURE);

							if ( !empty( $outs ) ) {
								
								foreach($outs[0] as $k => $out){	
									$pos_start = $out[1];
									$length = iconv_strlen( $out[0], 'UTF-8' ) + 80;
									
									$found_keyword = trim($out[0]);

									$count_sibling_keywords = (int)$this->wpdb->get_results("SELECT COUNT(*) FROM `$table_cross_links` WHERE `keyword` = '{$found_keyword}' AND `id_post_out` = {$post_id}", ARRAY_A)[0]['COUNT(*)'];

									$current_index_number = $count_sibling_keywords;
									$result = $this->create_context( $content_post, $pattern, $k, $current_index_number, $found_keyword );
									// $content = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result));

									// file_put_contents( __DIR__ . "/log.txt", '$content - ' . var_export( $content, true ) . "\n", FILE_APPEND );
									// if ( $this->check_count_references_to_article( $id, $post_id, $out[0] ) ) {
										$arr_connection[] = array(
											'input_article_id' => $id,
											'token' => $this->alice_access_token,
											'output_article_id' => $post_id,//from 
											'start' => $out[1],
											'finish' => $out[1] + strlen( $out[0] ),
											'key' => trim($out[0]), 
											'context' => $result,
											'date_post' => get_the_time('Y-m-d H:i:s', $id)
										);
										// $keyword_t = trim($out[0]);
										$start = $out[1];
										$finish = $out[1] + strlen( $out[0] );
										$date_post = get_the_time('Y-m-d H:i:s', $id);
										$token = $this->alice_access_token;			
										// $insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) VALUES ($id, $post_id, '$keyword_t', $start, $finish, '$result', 0, 0, '$date_post', '$token')");
										$setting_count = $config['references_to_the_first_article'];
										if ( isset($setting_count) && !empty($setting_count) && $setting_count != 'off' ) {
											
											if ( $this->check_count_references_to_article( $id, $post_id, $setting_count ) ) {
												$insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");
											}

										}else{

											$insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");

										}

									// }
								}
							}
						}

						// //если выбрано "разбавленные вхождения" - найдет и добавит в общий массив
						if($config['small_entry'] == 'on') {

							if ($config['allow_subtitles'] === 'on') {
								$end = '\b~u';
							}else{
								$end = '\b|<(?:(h[1-6]).*?</\1>|[^<>]*>)(*SKIP)(*F)~u';
							}

							$pattern = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[1] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[2] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[3] . $end;

							$pattern1 = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b' . $pos[1].'\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[2] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[3] . $end;
							
							$pattern2 = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[1] . '\b\s{1}\b' . $pos[2] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[3] . $end;		
							
							$pattern3 = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[1] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[2] . '\b\s{1}\b' . $pos[3] . $end;				
							
							$pattern4 = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b' . $pos[1] . '\b\s{1}\b' . $pos[2] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[3] . $end;					
							$pattern5 = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[1] . '\b\s{1}\b' . $pos[2] . '\b\s{1}\b' . $pos[3] . $end;					
							$pattern6 = '~(\A|\s)\b' . mb_strtolower($pos[0]) . '\b\s{1}\b' . $pos[1] . '\b\s{1}\b[а-я]{1,}\b\s{1}\b' . $pos[2] . '\b\s{1}\b' . $pos[3] . $end; 				
							
							if (preg_match_all($pattern, $content_post, $outs1, PREG_OFFSET_CAPTURE) 
								|| preg_match_all($pattern1, $content_post, $outs2, PREG_OFFSET_CAPTURE)  
								|| preg_match_all($pattern2, $content_post, $outs3, PREG_OFFSET_CAPTURE) 
								|| preg_match_all($pattern3, $content_post, $outs4, PREG_OFFSET_CAPTURE) 
								|| preg_match_all($pattern4, $content_post, $outs5, PREG_OFFSET_CAPTURE) 
								|| preg_match_all($pattern5, $content_post, $outs6, PREG_OFFSET_CAPTURE) 
								|| preg_match_all($pattern6, $content_post, $outs7, PREG_OFFSET_CAPTURE)){

									if ( !empty($outs1) == 1 ){
										$outs = $outs1;
										$pattern = $pattern;
									}elseif (!empty($outs2) == 2) {
										$outs = $outs2;
										$pattern = $pattern1;
									}elseif (!empty($outs3) == 3) {
										$outs = $outs3;
										$pattern = $pattern2;
									}elseif (!empty($outs4) == 4) {
										$outs = $outs4;
										$pattern = $pattern3;
									}elseif (!empty($outs4) == 5) {
										$outs = $outs5;
										$pattern = $pattern4;
									}elseif (!empty($outs4) == 6) {
										$outs = $outs6;
										$pattern = $pattern5;
									}elseif (!empty($outs4) == 7) {
										$outs = $outs7;
										$pattern = $pattern6;
									}

									foreach($outs[0] as $k => $out) {	
										$pos_start = $out[1];
										$length = iconv_strlen( $out[0], 'UTF-8' ) + 80;

										$found_keyword = trim($out[0]);

										$count_sibling_keywords = (int)$this->wpdb->get_results("SELECT COUNT(*) FROM `$table_cross_links` WHERE `keyword` = '{$found_keyword}' AND `id_post_out` = {$post_id}", ARRAY_A)[0]['COUNT(*)'];

										$current_index_number = $count_sibling_keywords;
										$result = $this->create_context( $content_post, $pattern, $k, $current_index_number, $found_keyword );
										// $content = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result));

										// file_put_contents( __DIR__ . "/log.txt", '$content - ' . var_export( $content, true ) . "\n", FILE_APPEND );
										// if ( $this->check_count_references_to_article( $id, $post_id, $k, $out[0] ) ) {
										// file_put_contents( __DIR__ . "/preg.txt", '$out[0] - ' . var_export( $out[0], true ) . "\n", FILE_APPEND );
										
											$arr_connection[] = array(
												'input_article_id' => $id,
												'token' => $this->alice_access_token,
												'output_article_id' => $post_id,//from 
												'start' => $out[1],
												'finish' => $out[1] + strlen( $out[0] ),
												'key' => trim($out[0]), 
												'context' => $result,
												'date_post' => get_the_time('Y-m-d H:i:s', $id)
											);
											
											// $keyword_t = trim($out[0]);
											$start = $out[1];
											$finish = $out[1] + strlen( $out[0] );
											$date_post = get_the_time('Y-m-d H:i:s', $id);
											$token = $this->alice_access_token;		
											// $insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) VALUES ($id, $post_id, '$keyword_t', $start, $finish, '$result', 0, 0, '$date_post', '$token')");
											$setting_count = $config['references_to_the_first_article'];
											if ( isset($setting_count) && !empty($setting_count) && $setting_count != 'off' ) {
												
												if ( $this->check_count_references_to_article( $id, $post_id, $setting_count ) ) {
													$insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");
												}

											}else{

												$insert = $this->wpdb->query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");

											}
										// }
									}
							}
						}	
					}
				}
			}

			//GET POSTS TO CHECK ON FRONTED IF CONINUE
			if ( $continue_proc == 'true'  && !isset($_POST['time']) ) {
				$check_posts_in_table = $posts_for_perelink_origin;
			}

			//SAVE REMAINING POSTS WICH KEYS USED TO posts_for_perelink.txt
			file_put_contents( __DIR__ . "/posts_for_perelink.txt", json_encode( $posts_for_perelink_current ) );

			if ( !empty($posts_for_perelink_current) ) {
	
				$count = count( $data );
				
				echo json_encode( array('current_post' => $curent_post, 'remaining_count' => count($posts_for_perelink_current), 'code' => 1, 'untreated_posts' => $count, 'continue' => $continue_proc, 'all_count' => count($posts_for_perelink_origin ), 'to_check' => $check_posts_in_table) );
				wp_die();

			}else{

				array_splice($data, 0, 1);
				$count = count( $data );
				if ( $count != 0 ) {
				    $data = json_encode($data);
				    
				    file_put_contents( __DIR__."/posts_with_keywords.txt", $data . "\n" );
				    $data = json_decode( file_get_contents(  __DIR__."/posts_with_keywords.txt" ), true );

				    echo json_encode( array('status' => __('В процессе', 'alice'), 'code' => 401, 'untreated_posts' => $count, 'continue' => $continue_proc, 'all_count' => count($posts_for_perelink_origin), 'current_post' => $curent_post, 'to_check' => $check_posts_in_table) );
				    wp_die();

				}elseif ( $count == 0 ){
				    file_put_contents( __DIR__."/posts_with_keywords.txt", 'END' );
				    file_put_contents( __DIR__."/posts_for_perelink.txt", 'END' );
				}else{
					file_put_contents( __DIR__."/count.txt", 'ERROR- ' . var_export( $count, true ) . "\n", FILE_APPEND );
				}

				if ( !isset($_POST['ids']) || empty($_POST['ids']) ) {	
					echo json_encode(array('status' => __('Вы не выбрали ни одной статьи', 'alice'), 'code' => 201));
					wp_die();
				}

				$formatted_cross_links = $this->wpdb->get_results("SELECT `id_post_in` `input_article_id`, `token`, `id_post_out` `output_article_id`, `start`, `finish`, `keyword` `key`, `context`, `date_post` FROM `$table_cross_links`", ARRAY_A);

				if ( empty($formatted_cross_links) ) {
					echo json_encode(array('status' => __('Связи не записались в таблицу!', 'alice'), 'code' => 404));		
					wp_die();
				}

				// file_put_contents( __DIR__ . "/log.txt", '$formatted_cross_links - ' . var_export( $formatted_cross_links, true ) . "\n", FILE_APPEND );
				$this->send_connection_from_table( $formatted_cross_links );
			}	
		}

		public function send_connexion_to_server_test()
		{		
		}

		public function send_connection_from_table( $formatted_cross_links )
		{
			$url = 'https://alice.2seo.pro/api/setConnections';
			$id_request = rand(0, 100000);
			
			if ( isset($formatted_cross_links) && !empty($formatted_cross_links) ) {
				
				$formatted_cross_links = array_chunk($formatted_cross_links, 1000);
				// $ser_posts = json_encode( array_slice($formatted_cross_links, 1) );

				foreach ($formatted_cross_links as $key => $value) {
					$value = array( 'id_request' => $id_request, 'connections' => $value, 'all_count' => count($formatted_cross_links), 'current' => $key+1 );
					$ser_posts = json_encode($value);
					$data_string = $ser_posts;			                                                                                                                     
					$ch = curl_init($url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					curl_setopt($ch, CURLOPT_TIMEOUT, 70);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);

					// curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					    'Content-Type: application/json',                                                                                
					    'Content-Length: ' . strlen($data_string)
					));                                                                   
					$response = curl_exec($ch);
				}
	            
	            $this->wpdb->query("UPDATE $this->table_menu SET `state` = 1 WHERE `menu_item_slug` = 're-linking'");
	           
	            echo json_encode(array('status' => __('Связи будут доставлены через несколько минут!<br><br><a target="_blank" style="color: white; text-decoration: underline;" href="https://alice.2seo.pro/">Перейдите в Сервис</a>', 'alice'), 'code' => 200));
			}else{
				echo json_encode(array('status' => __('Связей не найдено!', 'alice'), 'code' => 404));	
			}

			wp_die();
		}

	 	public function create_context( $content_post, $pattern, $key, $current_index_number, $keyword_trim )
	 	{
	 		$content_post = strip_tags($content_post);

	 		preg_match_all( $pattern, $content_post, $outs, PREG_OFFSET_CAPTURE );

	 		$keyword = $outs[0][$current_index_number][0];
	 		$pos_start = $outs[0][$current_index_number][1];
	 		
	 		if ( !isset($keyword) && !isset($pos_start) ) {
	 			$keyword = $outs[0][count($outs[0])-1][0];
	 			$pos_start = $outs[0][count($outs[0])-1][1];
	 		}

	 		$content_post = str_replace( array("\r", "\n"), array( " ", " " ), $content_post );
	 		$result = trim(substr( $content_post, $pos_start - 100, strlen($keyword) + 200 ));
		    $result = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result));
 			$result = explode(' ', $result);

 			$keyword_split = explode(' ', $keyword_trim);

 			if ( strpos($result[count($result)-1], '?') !== false || !in_array($result[count($result)-1], $keyword_split)) {
	        	unset( $result[count($result)-1] );
 			}

	        if ( strpos($result[0], '?') !== false || !in_array($result[0], $keyword_split) ){
	        	unset( $result[0] );
	        }

		    $result = '... ' . implode(' ', $result) . ' ...';
		
	 		return $result;
	 	}

	 	public function split($str, $len = 1) {

		    $arr		= [];
		    $length 	= mb_strlen($str, 'UTF-8');

		    for ($i = 0; $i < $length; $i += $len) {

		        $arr[] = mb_substr($str, $i, $len, 'UTF-8');

		    }

		    return $arr;

		}

	 	public function array_change_key( $array ) 
		{ 
		    if ( !is_array($array) ) { 
		        return FALSE; 
		    } 

		    foreach ($array as $key => $value) { 
		        $values[] = $value;
		    } 
		    return $values; 
		}

		public function array_flatten( $array ) 
		{ 
		    if (!is_array($array)) { 
		        return FALSE; 
		    } 
		    $result = array(); 
		    foreach ($array as $key => $value) { 
		        if (is_array($value)) { 
		            $result = array_merge($result, $this->array_flatten($value)); 
		        } else { 
		            $result[$key] = $value; 
		        } 
		    } 
		    return $result; 
		}

		public function cutQuery( $query ) 
		{	
			if ( !intval( $query ) ) {
				$query = $this->stremmer->getWordBase( $query );
			}
			// file_put_contents(__DIR__."/cut.txt", 'query - ' . var_export($query, true) ."\n", FILE_APPEND);
			return $query;
		}

		public function get_site_post_types()
		{
			$site_types = get_post_types();

			unset( $site_types['attachment'] );
			unset( $site_types['revision'] );
			unset( $site_types['nav_menu_item'] );
			unset( $site_types['custom_css'] );
			unset( $site_types['customize_changeset'] );
			unset( $site_types['oembed_cache'] );

			$all_posts_ids = [];

			foreach ( $site_types as $key => $post_type ) {
			    $args = array(
			        'post_type' => $post_type,
			        'post_status' => 'publish',
			        'posts_per_page' => -1,
			        'fields' => 'ids'
			    );

			    $site_posts_group = get_posts( $args );
			    $all_posts_ids[$post_type] = $site_posts_group;
			}
			
			$args = array(
			    'taxonomy' => 'category',
			    'hide_empty' => false,
			    'fields' => 'ids'
			);

			$terms = get_terms( $args );
			$all_posts_ids['category'] = $terms;

			$args = array(
			    'hide_empty' => false,
			    'fields' => 'ids'
			);

			$tags = get_tags( $args );
			$all_posts_ids['post_tag'] = $tags;

			return $all_posts_ids;
		}
	}
	
	$WP_Alice = WP_Alice::alice_get_instance();
endif;
