(function($){
	$(document).on( 'ready', function (e) {

		if ( typeof custom_css_class !== 'undefined' && custom_css_class.length )
			$('.alice_link').addClass(custom_css_class);

		$('.alice_link').on('click', function(e){
			// e.preventDefault();
			var _this = $(this),
				page_id = parseInt( _this.data('id') ),
				url = _this.attr('href');	

				if ( typeof page_id == "undefined" ) {
					var page_id = parseInt( _this.next().text() );
				}
				
				var data = {
					'action' : 'alice_count_clicks',
					'page_id' : page_id,
					'keyword' : _this.text(),
					'url' : url
				};

				$.ajax({
					type: 'POST',
					url: '/wp-admin/admin-ajax.php',
					data: data,
					success: function(response) {
						var response = JSON.parse( response );
						
							console.log('response', response);
						if ( response.status == 1 ) {
							// window.location = url;
							// console.log('url', url);
						}
					}
				});
			// console.log('data', data);
		});

		var send_ajax = true;
		$('.alice_link:not(.has_preview)').on('mouseenter', function(e){
			var _this = $(this),
				this_ = this,
				href = this.href;

			var data = {
				'action' : 'alice_preview_for_link',
				'href' : href
			};

			_this.addClass('has_preview');
			var top = _this.offset().top,
				left = _this.offset().left;
				
			if ( send_ajax && enable_preview) {
				send_ajax = false;
				$.ajax({
					url : '/wp-admin/admin-ajax.php',
					type: 'POST',
					data: data,
					success: function(response) {
						send_ajax = true;
						var response = JSON.parse(response),
							image = response.image,
							title = response.title,
							preview_border_color = response.preview_border_color,
							preview_font_color = response.preview_font_color,
							preview_img_size = response.preview_img_size,
							preview_bg_color = response.preview_bg_color,
							preview_font_size = response.preview_font_size;
							// console.log(response);
						var style = 'style="border: 1px solid #'+preview_border_color+';"';
						var bg = 'style="background-color: #'+preview_bg_color+';"';
						var color = 'style="color: #'+preview_font_color+'; font-size: '+preview_font_size+';"';
						var preview = $('<div class="preview_wrap" '+style+'><div class="preview" '+bg+'><img style="max-width: '+preview_img_size+'px;" src="'+image+'"><span '+color+'>'+title+'</span></div></div>');
						
						preview.appendTo('body');
						var new_left = left - preview.width()/2 + this_.offsetWidth/2;
						// $('.preview_wrap').offset({top: top+this_.offsetHeight+5, left:left-this_.offsetWidth}).fadeIn(200);
						$('.preview_wrap').offset({top: top+this_.offsetHeight+5, left:new_left}).fadeIn(200);
					} 
				});	
			}
			
		});

		$(document).on('mouseleave', '.alice_link.has_preview', function(e){
			var _this = $(this);
			$('body').find( '.preview_wrap' ).remove();
			_this.removeClass('has_preview');
		});

	});
})(jQuery);