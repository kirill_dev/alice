(function($){
	$(document).on( 'ready', function () {
		// console.log('old version');
		var table_responsive_view = $('.table-responsive');

		$('input[name="custom_css_class"]').on('keypress', function(e) {
	        if (e.which == 32)
	            return false;
		 });
	
	    var table = $('.alice-stat-table-stat').DataTable({
	    	lengthMenu: [[ 10, 25, 50, -1], [ 10, 25, 50, "Все"]],
	        dom: 'lBSfrtip',
	        select: {
	        	style: 'multi',
	        	// info: false
	        },
	        buttons: [
	            // {
	            //     text: 'Выбрать всё',
	            //     action: function () {
	            //         table.rows().select();

	            //         table_responsive_view.find('.spin-loader').addClass('top');
	            //         table_responsive_view.find('.table-responsive-overlay').fadeIn();
	            //         table_responsive_view.find('.spin-loader').fadeIn();

	            //         if ( $('.settings_form_by_cat').length && $('.alice_use_cat').length ) {
	            //         	var _this = $('.settings_form_by_cat'),
	            //         		myFormData = [];

	            //         	$('.alice_use_cat').prop('checked', true);

	            //         	_this.find('.alice_use_cat').each(function(index, value){
	            //         		if ( value.checked ) {
	            //         			myFormData.push(value.name);
	            //         		}
	            //         	});
	                    	
	            //         	var data = {
	            //         		'action' : 'settings_form_by_cat_all',
	            //         		'data' : myFormData
	            //         	};
	                    	
	            //         	$.ajax({
	            //         		type: 'POST',
	            //         		url: '/wp-admin/admin-ajax.php',
	            //         		data : data,
	            //         		beforeSend: function(){
	            //         		},
	            //         		success: function(response) {
	            //         			// _this.find('input[type="submit"]').removeClass('proccessing');
	            //         		},
	            //         		complete: function(){
	            //         			table_responsive_view.find('.spin-loader').removeClass('top');
	            //         			table_responsive_view.find('.table-responsive-overlay').fadeOut();
	            //         			table_responsive_view.find('.spin-loader').fadeOut();
	            //         		}
	            //         	});
	            //         }
	            //     }
	            // },
	            // {
	            //     text: 'Отменить выбор',
	            //     action: function () {
	            //         table.rows().deselect();

	            //         table_responsive_view.find('.spin-loader').addClass('top');
	            //         table_responsive_view.find('.table-responsive-overlay').fadeIn();
	            //         table_responsive_view.find('.spin-loader').fadeIn();
	                    
	            //         if ( $('.settings_form_by_cat').length && $('.alice_use_cat').length ) {
	            //         	var _this = $('.settings_form_by_cat');

	            //         	$('.alice_use_cat').prop('checked', false);
	                    	
	            //         	var data = {
	            //         		'action' : 'settings_form_by_cat_deselect_all',
	            //         	};
	                    	
	            //         	$.ajax({
	            //         		type: 'POST',
	            //         		url: '/wp-admin/admin-ajax.php',
	            //         		data : data,
	            //         		beforeSend: function(){
	            //         		},
	            //         		success: function(response) {
	            //         		},
	            //         		complete: function(){
	            //         			table_responsive_view.find('.spin-loader').removeClass('top');
	            //         			table_responsive_view.find('.table-responsive-overlay').fadeOut();
	            //         			table_responsive_view.find('.spin-loader').fadeOut();
	            //         		}
	            //         	});
	            //         }

	            //     }
	            // }
	        ],
    		columnDefs: [{
    			"orderable": false,
    			"className": 'select-checkbox',
    			"targets":   0
    		}],
            order: [[ 1, 'asc' ]],
	       	language: {
    		    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json",
    		    select : {
		            rows: "отмечено %d"
		        }
    		},
    		deferRender:true,
    		sDom: '<"top"lfBi><bottom p>',
    		// sDom:  '<"top"i>rt<"bottom"flp><"clear">',
	        "initComplete": function( settings, json ) {

	        	var wpadminbar = 0;
	        	var max_width = $('.alice-stat-table-stat').width();

	        	function make_fixed_header()
	        	{
	        		var top_table = $('.alice-stat-table-stat').offset().top;
	        		
		        	if ( typeof $('#wpadminbar') != 'undefined' ) {
		        		wpadminbar = $('#wpadminbar').height();
		        	}

	        		if ( top_table <= $(window).scrollTop()+wpadminbar ) {
	        			$('.fixed_head').show();
	        		}else{
	        			$('.fixed_head').hide();
	        		}
	        	}

	        	function resize_fixed_header()
	        	{
	        		var origin_th = $('.not_fixed_head th');
		        	origin_th.each(function(i, v){
		        		let y = i+1;
		        		$('.fixed_head th:nth-child('+y+')').width($(v).width());
		        	});
	        	}

	        	make_fixed_header();
	        	resize_fixed_header();
	        	
	        	$('.fixed_head').width(max_width);
	        	
	        	$(window).on('resize', function(e){
	        		resize_fixed_header();
	        	});

	        	$(window).on('scroll', function(e){
	        		make_fixed_header();
	        	});

	        	$('#select_all_keywords').on('change', function(e){

	        		if ( this.checked ) {
	        			$('[for="select_all_keywords"]').addClass('selected_custom_check');
	        			table.rows().select();
	        			table_responsive_view.find('.spin-loader').addClass('top');
	        			table_responsive_view.find('.table-responsive-overlay').fadeIn();
	        			table_responsive_view.find('.spin-loader').fadeIn();

	        			if ( $('.settings_form_by_cat').length && $('.alice_use_cat').length ) {
	        				var _this = $('.settings_form_by_cat'),
	        					myFormData = [];

	        				$('.alice_use_cat').prop('checked', true);

	        				_this.find('.alice_use_cat').each(function(index, value){
	        					if ( value.checked ) {
	        						myFormData.push(value.name);
	        					}
	        				});
	        				
	        				var data = {
	        					'action' : 'settings_form_by_cat_all',
	        					'data' : myFormData
	        				};
	        				
	        				$.ajax({
	        					type: 'POST',
	        					// url: '/wp-admin/admin-ajax.php',
	        					url: ajaxurl,
	        					data : data,
	        					beforeSend: function(){
	        					},
	        					success: function(response) {
	        						// _this.find('input[type="submit"]').removeClass('proccessing');
	        					},
	        					complete: function(){
	        						table_responsive_view.find('.spin-loader').removeClass('top');
	        						table_responsive_view.find('.table-responsive-overlay').fadeOut();
	        						table_responsive_view.find('.spin-loader').fadeOut();
	        					}
	        				});
	        			}
	        		}else{
	        			$('[for="select_all_keywords"]').removeClass('selected_custom_check');
	        			table.rows().deselect();

	        			table_responsive_view.find('.spin-loader').addClass('top');
	        			table_responsive_view.find('.table-responsive-overlay').fadeIn();
	        			table_responsive_view.find('.spin-loader').fadeIn();
	        			
	        			if ( $('.settings_form_by_cat').length && $('.alice_use_cat').length ) {
	        				var _this = $('.settings_form_by_cat');

	        				$('.alice_use_cat').prop('checked', false);
	        				
	        				var data = {
	        					'action' : 'settings_form_by_cat_deselect_all',
	        				};
	        				
	        				$.ajax({
	        					type: 'POST',
	        					url: ajaxurl,
	        					// url: '/wp-admin/admin-ajax.php',
	        					data : data,
	        					beforeSend: function(){
	        					},
	        					success: function(response) {
	        					},
	        					complete: function(){
	        						table_responsive_view.find('.spin-loader').removeClass('top');
	        						table_responsive_view.find('.table-responsive-overlay').fadeOut();
	        						table_responsive_view.find('.spin-loader').fadeOut();
	        					}
	        				});
	        			}
	        		}
	        	});


    			$('.dataTables_length select').select2({
    	    		minimumResultsForSearch: -1
    			});

    			$('.custom-selected td a').on('click', function(e){
    				e.stopPropagation();
    			});

	            table.rows().every (function ( rowIdx, tableLoop, rowLoop ) {

	            	if (  $(table.row(':eq('+rowIdx+')', { page: 'all' }).node()).hasClass('custom-selected') ) {
	            		table.row(':eq('+rowIdx+')', {page: 'all'}).select();
	            	}

	            });

	            table_responsive_view.find('.table-responsive-overlay').fadeOut(400);
	            table_responsive_view.find('.spin-loader').fadeOut(200);

	            table_responsive_view.removeClass('max_height');
	        }
	    } );

	    var table_key = $('.alice-stat-table-key').DataTable( {
	    	lengthMenu: [[ 10, 25, 50, -1], [ 10, 25, 50, "Все"]],
	        dom: 'lBSfrtip',
	        select: { 
	        	style: 'multi', 
	        	// info: false 
	        },
	        buttons: [
	            // {
	            //     text: 'Выбрать всё',
	            //     action: function () {
	            //         table_key.rows().select();
	            //     }
	            // },
	            // {
	            //     text: 'Отменить выбор',
	            //     action: function () {
	            //         table_key.rows().deselect();
	            //     }
	            // }
	        ],
    		columnDefs: [{
    			"orderable": false,
    			"className": 'select-checkbox',
    			"targets":   0
    		}],
            order: [[ 1, 'asc' ]],
	       	language: {
    		    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json",
    		     select : {
		            rows: "отмечено %d"
		        }
    		},
    		sDom: '<"top"lfBi><bottom p>',
    		deferRender:true,
	        "initComplete": function( settings, json ) {

	        	var wpadminbar = 0;
	        	var max_width = $('.alice-stat-table-key').width();

	        	function make_fixed_header()
	        	{
	        		var top_table = $('.alice-stat-table-key').offset().top;
	        		
		        	if ( typeof $('#wpadminbar') != 'undefined' ) {
		        		wpadminbar = $('#wpadminbar').height();
		        	}

	        		if ( top_table <= $(window).scrollTop()+wpadminbar ) {
	        			$('.fixed_head').show();
	        		}else{
	        			$('.fixed_head').hide();
	        		}
	        	}

	        	function resize_fixed_header()
	        	{
	        		var origin_th = $('.not_fixed_head th');
		        	origin_th.each(function(i, v){
		        		let y = i+1;
		        		$('.fixed_head th:nth-child('+y+')').width($(v).width());
		        	});
	        	}

	        	make_fixed_header();
	        	resize_fixed_header();
	        	
	        	$('.fixed_head').width(max_width);
	        	
	        	$(window).on('resize', function(e){
	        		resize_fixed_header();
	        	});

	        	$(window).on('scroll', function(e){
	        		make_fixed_header();
	        	});
	        	
	        	$('#select_all_keywords').on('change', function(e){
	        		if ( this.checked ) {
	        			$('[for="select_all_keywords"]').addClass('selected_custom_check');
	        			table_key.rows().select();
	        		}else{
	        			$('[for="select_all_keywords"]').removeClass('selected_custom_check');
	        			table_key.rows().deselect();
	        		}
	        	});

    			$('.dataTables_length select').select2({
    	    		minimumResultsForSearch: -1
    			});

	            table_key.rows().every (function ( rowIdx, tableLoop, rowLoop ) {

	            	if ( rowIdx == 0 ) {
	            		// $(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).find('.alice-modal-wrap').addClass('open-first');
	            		$(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).find('.alice-modal-wrap').css({'display':'block'});
	            	}

	            	if (  $(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).hasClass('custom-selected') ) {
	            		table_key.row(':eq('+rowIdx+')', {page: 'all'}).select();
	            	}

	            });

	            table_responsive_view.find('.table-responsive-overlay').fadeOut(400);
	            table_responsive_view.find('.spin-loader').fadeOut(200);

	            table_responsive_view.removeClass('max_height');

	            var admin_url = ajaxurl.split('/')[1];

	            // $('.dt-buttons').append($('<button class="event_all_accordion" data-event="open" tabindex="0" aria-controls="DataTables_Table_0" type="button"><span>Открыть всё</span></button>'));
	            // $('.dt-buttons').append($('<button class="event_all_accordion" data-event="close" tabindex="0" aria-controls="DataTables_Table_0" type="button"><span>Закрыть всё</span></button>'));
	        	$('.dt-buttons').append(
	        		$('<form style="display: inline-block;" id="form1" action="/'+admin_url+'/admin.php?page=wp-alice-keywords&noheader=true" method="POST"></form><button class="event_all_accordion update_keywords" type="submit" name="update_keywords" form="form1" data-event="update_keywords" tabindex="0" aria-controls="DataTables_Table_0" title="Обновить данные из сервиса"></button>')
	        	);
	        	$('.dt-buttons').append($('<button class="event_all_accordion red" data-event="delete_all_duplicate" tabindex="0" aria-controls="DataTables_Table_0" type="button"><span>Удалить повторяющиеся слова</span></button>'));

	        	$('#open_all').on('change', function(e){
	        		e.stopPropagation();
	        		// e.preventDefault();

	        		table_responsive_view.find('.spin-loader').addClass('top');
	        		table_responsive_view.find('.table-responsive-overlay').fadeIn();
	        		table_responsive_view.find('.spin-loader').fadeIn();

	        		setTimeout( () => {
		        		if ( !this.checked ) {

		        			$('.alice-modal-wrap').slideUp('300', function(){
		        				table_responsive_view.find('.spin-loader').removeClass('top');
		        				table_responsive_view.find('.table-responsive-overlay').fadeOut();
		        				table_responsive_view.find('.spin-loader').fadeOut();
		        			});
		        			
		        			$('.alice-modal-wrap').removeClass('open_e');
		        			$('.alice-modal-wrap').addClass('close_e');

		        		}else{

		        			$('.alice-modal-wrap').slideDown('300', function(){
		        				table_responsive_view.find('.spin-loader').removeClass('top');
		        				table_responsive_view.find('.table-responsive-overlay').fadeOut();
		        				table_responsive_view.find('.spin-loader').fadeOut();
		        			});
		        			$('.alice-modal-wrap').removeClass('close_e');
		        			$('.alice-modal-wrap').addClass('open_e');
		        		}
	        		},1000);
	        	});

	        	$('.event_all_accordion:not(.update_keywords)').on('click', function(e){
	        		e.preventDefault();

	        		table_responsive_view.find('.spin-loader').addClass('top');
	        		table_responsive_view.find('.table-responsive-overlay').fadeIn();
	        		table_responsive_view.find('.spin-loader').fadeIn();

	        		setTimeout( () => {
		        		if ( $(this).data('event') == 'delete_all_duplicate' ) {

		        			var all_reds = [];
		        			
		        			table_key.rows().every (function ( rowIdx, tableLoop, rowLoop ) {
		        				$(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).find('.keyword_in_modal_close.red').removeClass('red').not('.first').remove();
		        				var count_of_keywords = $(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).find('.keyword_in_modal_close').length;
		        				$(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).find('td:nth-child(5)').text(count_of_keywords);
		        			});

		        			table_responsive_view.find('.spin-loader').removeClass('top');
		        			table_responsive_view.find('.table-responsive-overlay').fadeOut();
		        			table_responsive_view.find('.spin-loader').fadeOut();
		        		}
	        		},1000);
	        	});  

	        	$('.update_keywords').on('click', function(e){
	        		var state = confirm('Обновится в случае изменений в настройках сервиса');
	        		if ( !state ) {
	        			return false;
	        		}
	        	});
	        }
	    } );

	   	var table_relink = $('.alice-stat-table-relink').DataTable( {
	    	lengthMenu: [[ 10, 25, 50, -1], [ 10, 25, 50, "Все"]],
	       	language: {
    		    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json",
    		},
    		deferRender: true,
    		sDom: '<"top"lfi><bottom p>',
	        "initComplete": function( settings, json ) {

    			$('.dataTables_length select').select2({
    	    		minimumResultsForSearch: -1
    			});

	            table_responsive_view.find('.table-responsive-overlay').fadeOut(400);
	            table_responsive_view.find('.spin-loader').fadeOut(200);
	        }
		} );
		
		var import_table = $('.alice-stat-table-import').DataTable({
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Все"]],
			language: {
				"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json",
			},
			deferRender: true,
			sDom: '<"top"lfi><bottom p>',
			"initComplete": function (settings, json) {

				$('.dataTables_length select').select2({
					minimumResultsForSearch: -1
				});

				table_responsive_view.find('.table-responsive-overlay').fadeOut(400);
				table_responsive_view.find('.spin-loader').fadeOut(200);
			}
		});

	    var table_click_stat = $('.alice-stat-table-click-stat').DataTable({
	    	lengthMenu: [[ 10, 25, 50, -1], [ 10, 25, 50, "Все"]],
	        dom: 'lBSfrtip',
	        select: {
	        	style: 'multi',
	        	// info: false
	        },
	        buttons: [
	           
	        ],
    		columnDefs: [{
    			"orderable": false,
    			// "className": 'select-checkbox',
    			"targets":   0
    		}],
            // order: [[ 1, 'asc' ]],
	       	language: {
    		    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json",
    		    select : {
		            rows: "отмечено %d"
		        }
    		},
    		deferRender:true,
    		sDom: '<"top"lfBi><bottom p>',
    		// sDom:  '<"top"i>rt<"bottom"flp><"clear">',
	        "initComplete": function( settings, json ) {

	        	var wpadminbar = 0;
	        	var max_width = $('.alice-stat-table-click-stat').width();

	        	function make_fixed_header()
	        	{
	        		var top_table = $('.alice-stat-table-click-stat').offset().top;
	        		
		        	if ( typeof $('#wpadminbar') != 'undefined' ) {
		        		wpadminbar = $('#wpadminbar').height();
		        	}

	        		if ( top_table <= $(window).scrollTop()+wpadminbar ) {
	        			$('.fixed_head').show();
	        		}else{
	        			$('.fixed_head').hide();
	        		}
	        	}

	        	function resize_fixed_header()
	        	{
	        		var origin_th = $('.not_fixed_head th');
		        	origin_th.each(function(i, v){
		        		let y = i+1;
		        		$('.fixed_head th:nth-child('+y+')').width($(v).width());
		        	});
	        	}

	        	make_fixed_header();
	        	resize_fixed_header();
	        	
	        	$('.fixed_head').width(max_width);
	        	
	        	$(window).on('resize', function(e){
	        		resize_fixed_header();
	        	});

	        	$(window).on('scroll', function(e){
	        		make_fixed_header();
	        	});

	        	// $('#select_all_keywords').on('change', function(e){
	        	// 	if ( this.checked ) {
	        	// 		$('[for="select_all_keywords"]').addClass('selected_custom_check');
	        	// 		table.rows().select();
	        	// 		table_responsive_view.find('.spin-loader').addClass('top');
	        	// 		table_responsive_view.find('.table-responsive-overlay').fadeIn();
	        	// 		table_responsive_view.find('.spin-loader').fadeIn();

	        	// 		if ( $('.settings_form_by_cat').length && $('.alice_use_cat').length ) {
	        	// 			var _this = $('.settings_form_by_cat'),
	        	// 				myFormData = [];

	        	// 			$('.alice_use_cat').prop('checked', true);

	        	// 			_this.find('.alice_use_cat').each(function(index, value){
	        	// 				if ( value.checked ) {
	        	// 					myFormData.push(value.name);
	        	// 				}
	        	// 			});
	        				
	        	// 			var data = {
	        	// 				'action' : 'settings_form_by_cat_all',
	        	// 				'data' : myFormData
	        	// 			};
	        				
	        	// 			$.ajax({
	        	// 				type: 'POST',
	        	// 				url: '/wp-admin/admin-ajax.php',
	        	// 				data : data,
	        	// 				beforeSend: function(){
	        	// 				},
	        	// 				success: function(response) {
	        	// 					// _this.find('input[type="submit"]').removeClass('proccessing');
	        	// 				},
	        	// 				complete: function(){
	        	// 					table_responsive_view.find('.spin-loader').removeClass('top');
	        	// 					table_responsive_view.find('.table-responsive-overlay').fadeOut();
	        	// 					table_responsive_view.find('.spin-loader').fadeOut();
	        	// 				}
	        	// 			});
	        	// 		}
	        	// 	}else{
	        	// 		$('[for="select_all_keywords"]').removeClass('selected_custom_check');
	        	// 		table.rows().deselect();

	        	// 		table_responsive_view.find('.spin-loader').addClass('top');
	        	// 		table_responsive_view.find('.table-responsive-overlay').fadeIn();
	        	// 		table_responsive_view.find('.spin-loader').fadeIn();
	        			
	        	// 		if ( $('.settings_form_by_cat').length && $('.alice_use_cat').length ) {
	        	// 			var _this = $('.settings_form_by_cat');

	        	// 			$('.alice_use_cat').prop('checked', false);
	        				
	        	// 			var data = {
	        	// 				'action' : 'settings_form_by_cat_deselect_all',
	        	// 			};
	        				
	        	// 			$.ajax({
	        	// 				type: 'POST',
	        	// 				url: '/wp-admin/admin-ajax.php',
	        	// 				data : data,
	        	// 				beforeSend: function(){
	        	// 				},
	        	// 				success: function(response) {
	        	// 				},
	        	// 				complete: function(){
	        	// 					table_responsive_view.find('.spin-loader').removeClass('top');
	        	// 					table_responsive_view.find('.table-responsive-overlay').fadeOut();
	        	// 					table_responsive_view.find('.spin-loader').fadeOut();
	        	// 				}
	        	// 			});
	        	// 		}
	        	// 	}
	        	// });


    			$('.dataTables_length select').select2({
    	    		minimumResultsForSearch: -1
    			});

    			$('.custom-selected td a').on('click', function(e){
    				e.stopPropagation();
    			});

	            table.rows().every (function ( rowIdx, tableLoop, rowLoop ) {

	            	if (  $(table.row(':eq('+rowIdx+')', { page: 'all' }).node()).hasClass('custom-selected') ) {
	            		table.row(':eq('+rowIdx+')', {page: 'all'}).select();
	            	}

	            });

	            table_responsive_view.find('.table-responsive-overlay').fadeOut(400);
	            table_responsive_view.find('.spin-loader').fadeOut(200);

	            table_responsive_view.removeClass('max_height');
	        }
	    } );

		$(document).on('change', '.working_class', function(e){
			
			$('.table-responsive').find('.spin-loader').addClass('top');
			$('.table-responsive').find('.table-responsive-overlay').fadeIn();
			$('.table-responsive').find('.spin-loader').fadeIn();
			
			var state = $(this).prop('checked'),
				post_id = $(this).data('post_id'),
				class_name = $(this).prop('value'),
				index = $(this).data('index'),
				link_text = $(this).data('link_text');

				var data = { 
				'action' : 'change_class_link', 
				'state' : state,
				'post_id': post_id,
				'link_text': link_text,
				'class_name': class_name,
				'global': false,
				'index' : index
			};

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				beforeSend: function () {
				},
				success: function (response) {
					
				},
				complete: function(){
					$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
					$('.table-responsive').find('.spin-loader').fadeOut(200);
				}
			});
		});

		$('[name="class_manage"]').on('change', function (e) {

			$('.table-responsive').find('.spin-loader').addClass('top');
			$('.table-responsive').find('.table-responsive-overlay').fadeIn();
			$('.table-responsive').find('.spin-loader').fadeIn();

			var state = $(this).prop('checked'),
				class_name = $(this).prop('value'),
				posts_per_page = 10,
				offset = 0;

			var data = {
				'action': 'change_class_link',
				'state': state,
				'global': true,
				'class_name': class_name,
				'offset': offset,
				'posts_per_page': posts_per_page
			};

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				beforeSend: function () {
				},
				success: function (response) {
					var response = JSON.parse(response),
						continue_ajax = response.continue_ajax,
						checked_links = response.posts,
						offset = response.offset;
					
					if (checked_links != undefined && checked_links.length) {
						checked_links.forEach(function (element) {
							var id = element[0],
								context = element[1],
								class_name = element[2];
							var row = $(import_table.row('[data-post_id="' + id + '"][data-link_text="' + context + '"]', { page: 'all' }).node());	
							var input_class = $(row.find('[name="on_off_class"][data-post_id="' + id + '"][data-link_text="' + context + '"][value="' + class_name +'"]'));

							input_class.each(function (i, v) {
								if ( state == false && $(v).prop('checked') == true ) {
									$(v).prop("checked", false);
								} else if (state == true && $(v).prop('checked') == false){
									$(v).prop("checked", true);
								}
							});

						});
					}

					if ( continue_ajax ) {
						offset += 10;
						setTimeout(()=>{
							send_ajax_get_checked_links(data, offset);
						}, 200);
					}else{
						$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
						$('.table-responsive').find('.spin-loader').fadeOut(200);
					}
				}
			});
		});

		function send_ajax_get_checked_links(data, offset)
		{
			data.offset = offset;
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				beforeSend: function () {
				},
				success: function (response) {
					var response = JSON.parse(response),
						checked_links = response.posts,
						continue_ajax = response.continue_ajax;

					if (checked_links != undefined && checked_links.length) {
						checked_links.forEach(function (element) {
							var id = element[0],
								context = element[1],
								class_name = element[2];
							var row = $(import_table.row('[data-post_id="' + id + '"][data-link_text="' + context + '"]', { page: 'all' }).node());
							var input_class = $(row.find('[name="on_off_class"][data-post_id="' + id + '"][data-link_text="' + context + '"][value="' + class_name+'"]'));
							
							input_class.each(function (i, v) {
								if (data.state == false && $(v).prop('checked') == true) {
									$(v).prop("checked", false);
								} else if (data.state == true && $(v).prop('checked') == false) {
									$(v).prop("checked", true);
								}
							});
						});
					}

					if (continue_ajax) {
						offset += 10;
						setTimeout(() => {
							send_ajax_get_checked_links(data, offset);
						}, 200);
					} else {
						$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
						$('.table-responsive').find('.spin-loader').fadeOut(200);
					}
				}
			});
		}

		$(document).on('submit', '.add_class:not(.global)', function(e){
			e.preventDefault();

			$('.table-responsive').find('.spin-loader').addClass('top');
			$('.table-responsive').find('.table-responsive-overlay').fadeIn();
			$('.table-responsive').find('.spin-loader').fadeIn();

			var _this = $(this),
				post_id =_this.parents('tr').data('post_id'),
				link_text =_this.parents('tr').data('link_text'),
				index =_this.parents('tr').data('index');

			var data = {
				'action': 'add_class_link',
				'class_name_to_add': this.class_name_to_add.value,
				'post_id': post_id,
				'link_text': link_text,
				'index': index,
			};
			
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (response) {
					var response = JSON.parse(response);
					if ( !Object.keys(response).length )
						return false;

					var class_list = _this.parent().find('.class_list_control');
					var li = $('<li>'+
								'<span>'+response.class_name+'</span>'+
									'<input value="'+response.class_name+'" data-index="'+response.index_number+'" data-link_text="'+response.link_text+'" data-post_id="'+response.post_id+'" type="checkbox" name="on_off_class" class="working_class" checked="">'+
							'</li>');
					class_list.append(li);

					_this.find('[name="class_name_to_add"]').val('');

				},
				complete: function() {
					$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
					$('.table-responsive').find('.spin-loader').fadeOut(200);
				}
			});
		});

		$(document).on('click', '.class_name_to_add', function(e){
			e.preventDefault();

			$('.table-responsive').find('.spin-loader').addClass('top');
			$('.table-responsive').find('.table-responsive-overlay').fadeIn();
			$('.table-responsive').find('.spin-loader').fadeIn();

			var _this = $(this);
				// this_class = this.class_name_to_add.value,
				// class_name = $(this).parent().find('[name="class_manage"]').val();

			var data = {
				'action': 'add_class_link_global',
				// 'class_name_to_add': this.class_name_to_add.value,
				// 'for_class': class_name,
				"posts_per_page" : 50
			};
			
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function (response) {

					var response = JSON.parse(response),
						continue_ajax = response.continue_ajax,
						checked_links = response.posts,
						offset = response.offset;
					
					if ( checked_links != undefined && checked_links.length ) {
						checked_links.forEach(function (element) {
							if ( element[3] == true ) {
								var id = element[0],
									context = element[1],
									class_name = element[2];
								var row = $(import_table.row('[data-post_id="' + id + '"][data-link_text="' + context + '"]', { page: 'all' }).node());	
								// var input_class = $(row.find('[name="on_off_class"][data-post_id="' + id + '"][data-link_text="' + context + '"][value="' + class_name +'"]'));
								var li = $('<li>'+
											'<span>'+class_name+'</span>'+
												'<input value="'+class_name+'" data-index="'+$(row).attr('data-index')+'" data-link_text="'+context+'" data-post_id="'+id+'" type="checkbox" name="on_off_class" class="working_class" checked="">'+
										'</li>');
								var input_class = $(row).find('.class_list_control').append(li);

							}

						});
					}

					if ( continue_ajax ) {
						offset += 50;
						setTimeout(()=>{
							send_ajax_add_new_class_global(data, offset);
						}, 500);
					}else{
						$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
						$('.table-responsive').find('.spin-loader').fadeOut(200);
					}
				},
				complete: function() {
					$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
					$('.table-responsive').find('.spin-loader').fadeOut(200);
				}
			});
		});	

		function send_ajax_add_new_class_global(data, offset)
		{
			data.offset = offset;
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				beforeSend: function () {
				},
				success: function (response) {
					var response = JSON.parse(response),
						checked_links = response.posts,
						continue_ajax = response.continue_ajax;

					if (checked_links != undefined && checked_links.length) {
						checked_links.forEach(function (element) {
							if ( element[3] == true ) {
								var id = element[0],
									context = element[1],
									class_name = element[2];
								var row = $(import_table.row('[data-post_id="' + id + '"][data-link_text="' + context + '"]', { page: 'all' }).node());
								var li = $('<li>'+
											'<span>'+class_name+'</span>'+
												'<input value="'+class_name+'" data-index="'+$(row).attr('data-index')+'" data-link_text="'+context+'" data-post_id="'+id+'" type="checkbox" name="on_off_class" class="working_class" checked="">'+
										'</li>');
								var input_class = $(row).find('.class_list_control').append(li);
							}	
						});
					}

					if (continue_ajax) {
						offset += 50;
						setTimeout(() => {
							send_ajax_add_new_class_global(data, offset);
						}, 500);
					} else {
						$('.table-responsive').find('.table-responsive-overlay').fadeOut(400);
						$('.table-responsive').find('.spin-loader').fadeOut(200);
					}
				}
			});
		}

		$(document).on("input[name='class_name_to_add']",{
			keydown: function(e) {
			if (e.which === 32)
				return false;
			},
			change: function() {
				this.value = this.value.replace(/\s/g, "");
			}
		});

	   	$('.close_setting_message').on('click', function(e){
	   		e.preventDefault();

	   		$('.send_posts_to_server').find('input[type="submit"]').removeAttr('disabled');
	   		$(this).parents('.setting_message').fadeOut();
	   	});

		$(document).on('submit', '.send_posts_to_server', function(e) {
			e.preventDefault();
			var _this = $(this),
				submit_btn = _this.find('input[type="submit"]');
			
			submit_btn.attr('disabled', 'disabled');

			table_responsive_view.find('.table-responsive-overlay').fadeIn();
			table_responsive_view.find('.spin-loader').fadeIn();

			var ids = [];
			var trs = $('.alice-stat-table').find('tr.selected');

			var nodes = table.rows( { selected: true } ).nodes();
			nodes.each( function(v, i) {
				ids[i] = {id: $(v).data('id')};
				// ids[i] = {id: $(v).data('id'), type: $(v).data('post-category')};
			});

			// console.log( ids.length );			
			// console.log( memorySizeOf(ids) );			
			var data = {
				'action' : 'send_posts_to_server',
				'ids': ids,
				'id_request' : Math.random()
			}
			table.page.len( 10 ).draw();

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				beforeSend: function(){
				},
				success: function( response ) {
					var response = JSON.parse(response),
						count = response.all_count,
						setting_message = response.setting_message;

					if ( count !== undefined && count !== 0 ) {
						setTimeout( () => {
							check_processing_on_the_server(  'keywords', count, submit_btn  );
						}, 200);
					}

					table_responsive_view.find('.spin-loader').fadeOut(200);
					
					if ( response.posts_arr_size !== undefined && response.posts_arr_size > 1 ) {
						table_responsive_view.find('.progress-bar-parent-wrap').fadeIn();
						table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : 1+'%'});
						setTimeout(()=>{
							send_posts_to_server_by_parts( response, data, submit_btn );
						}, 5000);
					}else{
						submit_btn.removeAttr('disabled');
						$('.alice-sidebar-inner ul li.keywords').removeClass('disabled_link');

						table_responsive_view.append($('<div class="ms-keyword-error">'+response.status+'</div>')).fadeIn(300);
						table_responsive_view.append($('<a href="#" class="close-ms-keyword-error"></div>')).fadeIn(300);

						setTimeout(() => {
							window.location.href = window.location.origin+window.location.pathname+'?page=wp-alice-keywords';
						}, 1000);
					}
				}
			});
		});
		function memorySizeOf(obj) {
		    var bytes = 0;

		    function sizeOf(obj) {
		        if(obj !== null && obj !== undefined) {
		            switch(typeof obj) {
		            case 'number':
		                bytes += 8;
		                break;
		            case 'string':
		                bytes += obj.length * 2;
		                break;
		            case 'boolean':
		                bytes += 4;
		                break;
		            case 'object':
		                var objClass = Object.prototype.toString.call(obj).slice(8, -1);
		                if(objClass === 'Object' || objClass === 'Array') {
		                    for(var key in obj) {
		                        if(!obj.hasOwnProperty(key)) continue;
		                        sizeOf(obj[key]);
		                    }
		                } else bytes += obj.toString().length * 2;
		                break;
		            }
		        }
		        return bytes;
		    };

		    function formatByteSize(bytes) {
		        if(bytes < 1024) return bytes + " bytes";
		        else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KiB";
		        else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MiB";
		        else return(bytes / 1073741824).toFixed(3) + " GiB";
		    };

		    return formatByteSize(sizeOf(obj));
		};
		function send_posts_to_server_by_parts( response, data, input )
		{
			// table_responsive_view.find('.progress-bar-parent-wrap').fadeIn();
			// table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : 1+'%'});

			data.flag = parseInt(response.number_request) + 1;

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function( response ) {
					var response = JSON.parse(response);

					var percent_done = ((parseInt(response.number_request) + 1)*100)/response.posts_arr_size;

					if ( percent_done <= 0 )
						percent_done = 1;

					table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : percent_done+'%'});

					if ( response.number_request + 1 < response.posts_arr_size ) {
						setTimeout(()=>{
							send_posts_to_server_by_parts( response, data, input );
						}, 5000);
					}else if (  response.number_request + 1 === response.posts_arr_size ) {

						input.removeAttr('disabled');
						table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : '100%'});
						table_responsive_view.find('.progress-bar-parent-wrap').fadeOut(400);
						$('.alice-sidebar-inner ul li.keywords').removeClass('disabled_link');
						table_responsive_view.append($('<div class="ms-keyword-error">'+response.status+'</div>')).fadeIn(400);
						table_responsive_view.append($('<a href="#" class="close-ms-keyword-error"></div>')).fadeIn(400);

						setTimeout(() => {
							window.location.href = window.location.origin+window.location.pathname+'?page=wp-alice-keywords';
						}, 1000);

					}
				}
			});
		}

		function check_processing_on_the_server ( slug, count, input ) {

			var data = {
				'action' : 'check_processing_on_the_server',
				'slug' : slug
			}

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function( response ) {
				}
			});
		}

		function check_processing( slug ) {

			var data = {
				'action' : 'check_processing',
				'slug' : slug
			}

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function( response ) {
					var response = JSON.parse(response);

					if ( response.state == '1' ) {
						$('.alice-sidebar-inner ul li.' + slug).removeClass('disabled_link');
						table_responsive_view.find('.spin-loader').fadeOut(200);
						table_responsive_view.append($('<div class="ms-keyword-error">'+response.status+'</div>')).fadeIn(200);
						table_responsive_view.append($('<a href="#" class="close-ms-keyword-error"></div>')).fadeIn(200);
					}else{
						setTimeout( function(){
							check_processing(slug);
						}, 200 );
					}
				}
			});
		}

		$(document).on('mouseover', 'input[name="alice-token-submit-button"]', function(e){
			var input = $(this).parent().find('input[name="alice-access-token"]');
			input.addClass('hover');
		});

		$(document).on('mouseleave', 'input[name="alice-token-submit-button"]', function(e){
			var input = $(this).parent().find('input[name="alice-access-token"]');
			input.removeClass('hover');
		});

		$(document).on('submit', '.keyword-form-continue', function(e) {
			console.log('keyword-form-continue', this);
			find_connection_in_posts( e, $(this) );
		});

		$(document).on('submit', '.keyword-form', function(e) {
			console.log('keyword-form', this);
			find_connection_in_posts( e, $(this) );
		});

		function find_connection_in_posts( e, _this )
		{
			e.preventDefault();

			var continue_proc = false;
			if ( _this.hasClass('keyword-form-continue') )
				continue_proc = true;

			$('.alice-modal-wrap').slideUp();

			table_responsive_view.find('.table-responsive-overlay').fadeIn();
			table_responsive_view.find('.spin-loader').fadeIn();

			// var _this = $(this);
			var ids = [];
			var trs = $('.alice-stat-table-key').find('tr.selected');

			var nodes = table_key.rows( { selected: true } ).nodes();
			
			var input = _this.find('input[type="submit"]');
			input.attr('disabled', 'disabled');

			nodes.each( function(v, i) {
				ids[i] = {  id: $(v).data('id'), type: $(v).data('post-category')};
			});

			var data = {
				'action' : 'send_connexion_to_server',
				'ids': ids,
				'continue_proc' : continue_proc
			}

			$('.alice-modal-wrap').slideUp();
			
			table_key.page.len( 10 ).draw();	

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function( response ) {
					if ( response != undefined ) {
						var res = JSON.parse( response );
						// console.log( 'res', res );
						
						if ( res.code == 404 || res.code == 201) {
							_this.find('input[type="submit"]').removeAttr('disabled');
							table_responsive_view.find('.spin-loader').fadeOut(200);
							table_responsive_view.find('.progress-bar-parent-wrap').fadeOut(200);

							table_responsive_view.append($('<div class="ms-keyword-error">'+res.status+'</div>')).fadeIn(200);
							table_responsive_view.append($('<a href="#" class="close-ms-keyword-error"></div>')).fadeIn(200);

						}else if (res.code == 200) {
							_this.find('input[type="submit"]').removeAttr('disabled');
							check_processing( 're-linking' );
						}
						else if (res.code == 401 || res.code == 1) {
							table_responsive_view.find('.spin-loader').fadeOut(200);
							table_responsive_view.find('.progress-bar-parent-wrap').fadeIn();
							
							var data = {
								'action' : 'send_connexion_to_server',
								'ids': ids,
								'time':'not_first',
								'continue_proc' : continue_proc
							};

							// console.log("res.code", res.code);
							if ( res.code == 1 ) {
								data.same_post = true;
							}else{
								data.same_post = false;
							}

							setTimeout(() => {
								send_ajax_for_keywords( data, input );
							}, 200);
						}
					}
				},
				statusCode: {
			        502: function () {
			        	console.log('Fail! 502', data);
			            setTimeout(() => {
							send_ajax_for_keywords( data, input );
						}, 200)
			        },
			         504: function () {
			        	console.log('Fail! 504', data);
			            setTimeout(() => {
							send_ajax_for_keywords( data, input );
						}, 200)
			        }
			    },
			    error: function (xhr, ajaxOptions, thrownError) {
			    	console.log('xhr.status', xhr.status);
					// if(xhr.status == 404) {
					// // some error
					// }
				}
			});			
		}

		function send_ajax_for_keywords ( data, input ) {
			var submit_btn = input;
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				success: function( response ) {
					if ( response != undefined ) {
						var res = JSON.parse( response );

						if ( res.code == 404 || res.code == 201) {

							$('.keyword-form').find('input[type="submit"]').removeAttr('disabled');
							table_responsive_view.find('.spin-loader').fadeOut(200);
							table_responsive_view.find('.progress-bar-parent-wrap').fadeOut(200);
							table_responsive_view.append($('<div class="ms-keyword-error">'+res.status+'</div>')).fadeIn(200);
							table_responsive_view.append($('<a href="#" class="close-ms-keyword-error"></div>')).fadeIn(200);

						}else if (res.code == 200) {

							$('.keyword-form').find('input[type="submit"]').removeAttr('disabled');
							
							$('.alice-sidebar-inner ul li.re-linking').removeClass('disabled_link');
							table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : '100%'});
							table_responsive_view.find('.progress-bar-parent-wrap').fadeOut(200);
							table_responsive_view.append($('<div class="ms-keyword-error">'+res.status+'</div>')).fadeIn(200);
							table_responsive_view.append($('<a href="#" class="close-ms-keyword-error"></div>')).fadeIn(200);
							table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : '1%'});
							table_responsive_view.find('.untreated_posts').text(0);
							table_responsive_view.find('.progress-count').text('0%');

							if ( $('.toplevel_page_wp-alice').length ) {
								$('.toplevel_page_wp-alice').removeClass('disable_fourth_stage');
							}

						} else if (res.code == 401 || res.code == 1 ) {

							// console.log("res.code", res.code);

							if ( res.code == 1 ) {
								data.same_post = true;
							}else{
								data.same_post = false;
							}
								
							var all_count = data.ids.length;

							// console.log("res.continue", res.continue);
							// console.log("res.all_count", res.all_count);

							if (res.continue == 'true') {
								var all_count = res.all_count;
							}

							var untreated_posts = res.untreated_posts;

							var percent_done = ((all_count - untreated_posts)*100)/all_count;
							table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : percent_done+'%'});
							table_responsive_view.find('.progress-count').text( Math.round(percent_done)+'%');
							table_responsive_view.find('.untreated_posts').text(untreated_posts);
							
							setTimeout(() => {
								send_ajax_for_keywords( data );
							}, 200);

						}
					}						
				},
				statusCode: {
			        502: function () {
			        	console.log('Fail 2!', data);
			            setTimeout(() => {
							send_ajax_for_keywords( data, input );
						}, 200)
			        },
			        504: function () {
			        	console.log('Fail! 504', data);
			            setTimeout(() => {
							send_ajax_for_keywords( data, input );
						}, 200)
			        }
			    },
			    error: function (xhr, ajaxOptions, thrownError) {
			    	console.log('xhr.status2', xhr.status);
					// if(xhr.status == 404) {
					// // some error
					// }
				}
			});	
		}

		$(document).on('click', '.close-ms-keyword-error', function(e){
			e.preventDefault();
			table_responsive_view.find('.table-responsive-overlay').fadeOut(400);
			table_responsive_view.find(".ms-keyword-error").fadeOut(200);
			table_responsive_view.find(".close-ms-keyword-error").fadeOut(200);
			table_responsive_view.find('.progress-bar-parent-wrap').fadeOut(200);
			table_responsive_view.find('.progress-bar-parent-wrap .custom-progress-bar').css({'width' : '0'});
		});

		$(document).on('click', '.close-alert-ms', function(e) {
			e.preventDefault();
			$(this).parents('.alert-ms').slideUp();
		});

		function get_reds( table_key, data_id, keyword_text )
		{
			reds = [];
			table_key.rows().every (function ( rowIdx, tableLoop, rowLoop ) {

				let elem = $(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node());
				let keywords = elem.find('.alice-modal-main .keyword_in_modal_close.red');

				if ( elem.data('id') != data_id  ) {
					keywords.each(function(i,v) {
						if ( $(v).text() === keyword_text ) {
							reds.push( $(v) );
						}
					});
					
				}		

			});

			return reds;
		}

		table_key_jq = $('.alice-stat-table-key');

	    $('.alice-stat-table-key tr').on('click', '.show-keywords .keywords-title', function(e){
	    	e.preventDefault();
	    	e.stopPropagation();

	    	var wrap = $(this).parents('.show-keywords').find('.alice-modal-wrap'),
	    		td = $(this).parents('.show-keywords');

	    	wrap.slideToggle();
	    	td.toggleClass('open');
	    });

	    $('.alice-stat-table-key tr .alice-modal-btn.close').on('click', function(e){
	    	e.preventDefault();
	    	e.stopPropagation();

	    	if ( $(this).parents('.alice-modal-wrap').hasClass('open-first') ) {
	    		$(this).parents('.alice-modal-wrap').removeClass('open-first')
	    	}
		    	$(this).parents('.show-keywords').removeClass('open');
		    	$(this).parents('.alice-modal-wrap').slideUp();
	    });

		$('.alice-stat-table-key tr').on('click', '.keyword_in_modal_close .close_icon', function(e) {
			e.preventDefault();
			e.stopPropagation();

			let keyword_text = $(this).parents('.keyword_in_modal_close.red').find('> span').text();
			
			let data_id = $(this).parents('tr').data('id');

			if ( $(this).parents('.keyword_in_modal_close').hasClass('red') ) {

				var reds = get_reds( table_key, data_id, keyword_text );

				if (reds.length === 1 ) {
					reds[0].removeClass('red');
				}
			}
			var parent_tr = $(this).parents('tr');
			$(this).parents('.keyword_in_modal_close').remove();
			var count_of_keywords = parent_tr.find('.keyword_in_modal_close').length;
			parent_tr.find('td:nth-child(5)').text(count_of_keywords);
		});

		$('.alice-stat-table-key tr .add_keyword').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			var id = $(this).parents('tr').data('id');
			// var elem = $('<span class="keyword_in_modal_close focus"><span class="edit_elem" contenteditable="true">ключевое слово</span><i class="close_icon"><img src="http://ishtory.ru/wp-content/plugins/alice/templates/../img/cancel.svg"></i></span>');
			var elem = $('<a class="overlay_textara_wrap" href="#"></a><div id="'+id+'" class="textarea_wrap"><div class="textarea_inner_wrap"><h3 style="margin-top:0">Добавить ключевые слова</h3><form class="save_keywords_form"><textarea></textarea><input type="submit" class="btn btn-primary" name="submit_btn" value="Сохранить"></form></div></div>');
			elem.appendTo('body');
		});

		$(document).on('submit', '.save_keywords_form', function(e){
			e.preventDefault();
			var elems = '';
			var keywords = $(this).find('textarea').val().split("\n");
			
			var id = $(this).parents('.textarea_wrap').attr('id');
			
			var all_keywords = $('tr[data-id="'+id+'"]').find('.alice-modal-main .keyword_in_modal_close');
			var all_keywords_text = [];
			all_keywords.each(function(i, v){
				all_keywords_text.push( $(v).text() );
			});
			keywords.forEach(function(item, i, keywords) {
				// console.log( $.inArray(item, all_keywords_text) );
				if ( item.length && $.inArray(item, all_keywords_text) < 0) {
					elems += '<span class="keys_from_step keyword_in_modal_close focus"><span class="edit_elem" contentedtable="true"</span>'+item+'<i class="close_icon"><img src="/wp-content/plugins/alice/templates/../img/remove_keyword.png"></i></span></span>';
				}	
			});

			// console.log("elems", elems);

			$('tr[data-id="'+id+'"]').find('.alice-modal-main').prepend( elems );
			var count_of_keywords = $('tr[data-id="'+id+'"]').find('.alice-modal-main .keyword_in_modal_close').length;
			$('tr[data-id="'+id+'"] td:nth-child(5)').text(count_of_keywords);

			 	$(this).parents('.textarea_wrap').prev('.overlay_textara_wrap').remove();
			 	$(this).parents('.textarea_wrap').remove();
		});

		$(document).on('click', '.overlay_textara_wrap', function(e){
			$('.textarea_wrap').remove();
			$(this).remove();
		});

		$('.alice-stat-table-key tr').on('click', '.edit_elem', function(e){
			e.stopPropagation();
		});

		$('.alice-stat-table-key tr').on('click', '.keyword_title_link', function(e){
			e.stopPropagation();
		});

		$(document).on('mouseenter', '.keyword_in_modal_close.red', function(e) {
			e.preventDefault();

			$(this).addClass('hover');
			var tooltip = $('<div class="keyword-tooltip"></div>');
			var info = $('<div class="keyword-info">Где повторяются:</div>');

			let keyword_text = $(this).text();
			let data_id = $(this).parents('tr').data('id');

			var reds = get_reds( table_key, data_id, keyword_text );

			if ( reds.length ) {
				for (var i = 0; i < reds.length; i++) {
					var y = i+1;
					var title = $(reds[i]).parents('tr').find('.keywords-title').text();
					var row = $('<div><b>'+y+') ID статьи:</b> '+$(reds[i]).parents('tr').data('id')+'<br><b>Заголовок:</b> '+title+'</div>');
					info.append( row );
				}
			}
			
			if ( !$(this).find('.keyword-tooltip').length ) {
				tooltip.append( info );
				$(this).append( tooltip );
				var mCustomScrollbar = tooltip.find('.keyword-info').mCustomScrollbar({
				 	theme:"light"
				});
			}
		});

		$(document).on('mouseleave', '.keyword_in_modal_close.red', function(e) {
			e.preventDefault();
			
			$(this).find('.keyword-tooltip').remove();
			$(this).removeClass('hover');
		});

		$(".alice-modal-btn.submit-btn").on('click', function(e) {
			e.preventDefault();
			e.stopPropagation();
			
			var id = $(this).parents('tr').data('id');
			var _this = $(this);
			var new_keywords = [];

			var keywords = {
				keys_from_server : [],
				keys_from_admin : [],
				keys_from_step : [],
				custom_user_keywords : []
			};
			
			var keys_from_server = $(this).parents('tr').find('.keyword_in_modal_close.keys_from_server'),
				keys_from_admin = $(this).parents('tr').find('.keyword_in_modal_close.keys_from_admin'),
				keys_from_step = $(this).parents('tr').find('.keyword_in_modal_close.keys_from_step'),
				custom_user_keywords = $(this).parents('tr').find('.keyword_in_modal_close.custom_user_keywords');

			keys_from_server.each(function(i, v){
				keywords.keys_from_server.push($(v).text());
			});

			keys_from_admin.each(function(i, v){
				keywords.keys_from_admin.push($(v).text());
			});

			keys_from_step.each(function(i, v){
				keywords.keys_from_step.push($(v).text());
			});

			custom_user_keywords.each(function(i, v){
				keywords.custom_user_keywords.push($(v).text());
			});

			var data = {
				action : 'save_keywords_for_post',
				post_id : id,
				new_keywords : keywords 
			}

			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: data,
				beforeSend: function() {
					_this.addClass('proccessing');
				},
				success: function( response ) {
					_this.removeClass('proccessing');
					_this.addClass('green');

					table_key.rows().every (function ( rowIdx, tableLoop, rowLoop ) {

						if (  $(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).data('id') == _this.parents('tr').data('id') ) {
							// console.log('ascac', $(table_key.row(':eq('+rowIdx+')', { page: 'all' }).node()).data('id'));
							table_key.row(':eq('+rowIdx+')', {page: 'all'}).select();
						}

					});

					_this.parents('.alice-modal-wrap').slideUp();
					_this.parents('.show-keywords').removeClass('open');
				}
			});
		});
		
		$('.settings_form_by_cat input[type="checkbox"]').on('change', function(e) {
			e.preventDefault();
				
			table_responsive_view.find('.spin-loader').addClass('top');
			table_responsive_view.find('.table-responsive-overlay').fadeIn();
			table_responsive_view.find('.spin-loader').fadeIn();
			var _this_checkboxes = $('.settings_form_by_cat input[type="checkbox"]');
			_this_checkboxes.attr('disabled', true);
			_this_checkboxes.parent().css({'opacity':0.5});
			setTimeout(()=>{
					var _this = $(this).parents('.settings_form_by_cat'),
					myFormData = [],
					ids = [];
				
				_this.find('.alice_use_cat').each(function(index, value){
					if ( value.checked ) {
						myFormData.push(value.name);
					}
				});
				
				if ( myFormData.length === 0 ) {
					table.rows().deselect();
				}

				table.rows().every (function ( rowIdx, tableLoop, rowLoop ) {
					let categories = $(table.row(':eq('+rowIdx+')', { page: 'all' }).node()).data('categories');
					var not_in_cat = true;

					for ( var i = 0; i < categories.length; i++ ) {
						
						if ( myFormData.indexOf( categories[i] ) !== -1 ) {
							not_in_cat = false;
							let id = parseInt($(table.row(':eq('+rowIdx+')', { page: 'all' }).node()).data('id'));
							ids.push( id );
							table.row(':eq('+rowIdx+')', {page: 'all'}).select();
							break;	
						}

					}

					if ( not_in_cat ) {
						table.row(':eq('+rowIdx+')', {page: 'all'}).deselect();
					}
				});
				
				var data = {
					'action' : 'settings_form_by_cat',
					'data' : myFormData,
					'ids' : ids
				};

				// _this.find('input[type="submit"]').addClass('proccessing');
				$.ajax({
					type: 'POST',
					// url: '/wp-admin/admin-ajax.php',
					url: ajaxurl,
					data : data,
					beforeSend: function(){
					},
					success: function(response) {
						// _this.find('input[type="submit"]').removeClass('proccessing');
					},
					complete: function(){
						table_responsive_view.find('.spin-loader').removeClass('top');
						table_responsive_view.find('.table-responsive-overlay').fadeOut();
						table_responsive_view.find('.spin-loader').fadeOut();
						_this_checkboxes.attr('disabled', false);
						_this_checkboxes.parent().css({'opacity':1});
					}
				});
			},1000);
		});


		$('.settings_form input[type="checkbox"]').on('change', function(e) {	
			$( ".settings_form" ).trigger( "submit" );
		});


		$('#enable_recent_post_checkbox').on('change', function(e) {

			if ( this.checked ) {
				$(this).parents('.custom_checkbox_wrap').addClass('checked');
			}else{
				$(this).parents('.custom_checkbox_wrap').removeClass('checked');
			}

		});

		$('[name="enable_preview"]').on('change', function(e){

			if ( this.checked ) {
				$('.preview_block').slideDown();
			}else{
				$('.preview_block').slideUp();

			}

		});

		$('.open_choose').on('click', function(e){
			$(this).toggleClass('show_list');
			// $(this).addClass('show_list');
			$('.open_choose').not($(this)).removeClass('show_list');
			// $(this).next('.select_list').toggleClass('hidden');
		});

	});

})(jQuery)