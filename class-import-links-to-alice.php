<?php

class ImportLinksToAlice
{
	public $links = array();
	public $links_view = array();
	public $all_classes_of_links = array();
	public $dom;
	public $wpdb;
	public 	$table_prev_links;
	public 	$table_general_classes;

	function __construct()
	{
		global $wpdb;
		$this->wpdb = $wpdb;
		$this->table_prev_links = $this->wpdb->prefix . 'alice_prev_links';
		$this->table_general_classes = $this->wpdb->prefix . 'alice_links_general_classes';
		
		if ( class_exists('DOMDocument') ) {
			$this->dom = new DOMDocument();
			
			if ( isset($_POST['find_links']) && !empty($_POST['find_links']) ) {
				$this->findLinks();
				$this->saveLinksToDataBase( $this->links );
				$this->saveGeneralClasses();
			}
			
			$this->sendLinksToView();
			$this->showTable( $this->links_view, $this->all_classes_of_links );
		}else{
			$this->showErrorMessage();
		}
	}

	public function findLinks()
	{	
		$max_execution_time = ini_set('max_execution_time', 0);	
		$posts = $this->getPosts('post', 'page');
		// $all_classes_of_links = array();

		if ( $posts->have_posts() ) :
			while ( $posts->have_posts() ) : $posts->the_post();
				$post_id = get_the_ID();
				$content_post = get_the_content();
				$this->dom->loadHTML('<?xml encoding="utf-8" ?>'.$content_post);
				$links = $this->dom->getElementsByTagName('a');
				$count_links = $links->length - 1;
					// echo '<pre>';
					// var_dump( $post_id );
					for ($i=0; $i <= $count_links; $i++) { 
						if ( $links->item( $i ) !== null ) {
							$href = $links->item( $i )->getAttribute('href');
							$links_details = explode('.', parse_url($href)['path']);
							$attachment_format = $links_details[count($links_details)-1];
							$formats = array('xml', 'png', 'jpg', 'jpeg', 'svg', 'txt', 'pdf');
							
							if ( strpos($href, '/wp-content/uploads/') === false && !in_array($attachment_format, $formats) ){
								$class = trim($links->item( $i )->getAttribute('class'));
								$id = $links->item( $i )->getAttribute('id');
								$link['post_url'] = get_the_permalink($post_id);
								$link['post_id'] = $post_id;
								$link['post_title'] = get_the_title($post_id);
								$link['link_text'] = $links->item( $i )->nodeValue;
								$link['link_href'] = $href;
								
								$class = (!empty($class)) ? explode( ' ', $class ) : '';

								if ( !empty($class) ) {
									foreach ($class as $key => $class_name) {
										$key = array_search($class_name, $this->all_classes_of_links);
										if ( $key === false && !empty($class_name) )
											$this->all_classes_of_links[] = $class_name;
									}

									$class = array_map(function($elem) {
										if ( !empty($elem) )
											return array( $elem, 1 );
									}, $class);
								}

								// file_put_contents(__DIR__."/log.txt", 'all_classes_of_links - ' . var_export( $this->all_classes_of_links, true ) . "\n", FILE_APPEND);

								$link['link_classes'] = empty($class) ? '' : serialize($class);
								$link['link_id'] = $id;
								$index_number = 1;
		
								foreach ( $this->links as $key => $item ) {
									if ( $item['link_text'] == $link['link_text'] && $item['link_href'] == $link['link_href'] && $item['post_id'] == $link['post_id'] ) {
										$index_number++;
									}	
								}
								$link['index_number'] = $index_number;
								// file_put_contents(__DIR__."/log.txt", 'index_number - ' . var_export( $index_number, true ) . "\n", FILE_APPEND);
								// file_put_contents(__DIR__."/log.txt", 'nodeValue - ' . var_export( $links->item( $i )->nodeValue, true ) . "\n\n", FILE_APPEND);
								array_push($this->links, $link);
								// echo '<br>';
							}
						}
					}
					// echo '</pre><br>';
			endwhile;
		endif;

		usort( $this->links, function( $elem1, $elem2 ) {
			if ( $elem1['post_id'] > $elem2['post_id'] )
				return 1;
			if ( $elem1['post_id'] < $elem2['post_id'] )
				return -1;
			if ( $elem1['post_id'] == $elem2['post_id'] )
				return 0;
		} );

		return $this->links;
	}

	public function saveGeneralClasses()
	{	
		$link_classes = $this->wpdb->get_results( "SELECT `link_classes` FROM `$this->table_prev_links`", ARRAY_A );
		
		foreach ($link_classes as $key => $classes) {
			$classes = maybe_unserialize($classes['link_classes']);

			foreach ($classes as $key => $class) {
				$key = array_search($class[0], $this->all_classes_of_links);
				if ( $key === false && !empty($class[0]) )
					$this->all_classes_of_links[] = $class[0];
			}
		}

		$result = $this->wpdb->get_results( "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{$this->table_general_classes}'", ARRAY_A)[0];
		if ( !isset($result) && !isset($result['TABLE_NAME']) ) {
		 	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			$sql_request = "CREATE TABLE " . $this->table_general_classes . " (
				id int(11) NOT NULL AUTO_INCREMENT,
				class_name text NOT NULL,
				class_status int(11) NOT NULL,
				PRIMARY KEY(id))
				ENGINE = INNODB
				DEFAULT CHARACTER SET = utf8
				COLLATE = utf8_general_ci";
			$dbDelta = dbDelta($sql_request);
		}else{
			// $existing_classes = $this->wpdb->get_results("SELECT `class_name` FROM `$table_name`", ARRAY_A);
		}	
		// $prepare_classes = serialize($this->all_classes_of_links);
		foreach ($this->all_classes_of_links as $key => $value) {
				$insert = $this->wpdb->query("INSERT INTO `$this->table_general_classes` (`class_name`, `class_status`) SELECT * FROM (SELECT '{$value}', 1) AS tmp WHERE NOT EXISTS ( SELECT `class_name` FROM `$this->table_general_classes` WHERE `class_name` = '$value')");			
		}

		// return $this->all_classes_of_links;
	}

	public function saveLinksToDataBase( $links )
	{
		$table_status = $this->isTableExists( $this->table_prev_links );
		
		$flag = time();
		$column_flag = 'flag';

		$checkcolumn_flag = $this->wpdb->get_results("SELECT column_name FROM information_schema.columns WHERE table_name = '$this->table_prev_links' AND column_name = '$column_flag'", ARRAY_A);

		if( empty($checkcolumn_flag) ) 
		    $this->wpdb->query("ALTER TABLE `$this->table_prev_links` ADD flag text NOT NULL");	

		if ( $table_status ) {
			foreach ($links as $key => $link) {
				$link_text = $link['link_text'];
				$link_href = $link['link_href'];
				$link_classes = $link['link_classes'];
				$link_id = $link['link_id'];
				$post_id =  (int)$link['post_id'];
				$post_url = $link['post_url'];
				$post_title = $link['post_title'];
				$index_number = $link['index_number'];

				$insert = $this->wpdb->query("INSERT INTO `$this->table_prev_links` (`link_text`, `link_href`, `link_classes`, `link_id`, `post_id`, `post_url`, `post_title`, `index_number`, `flag`) SELECT * FROM (SELECT '{$link_text}' AS link_text,'{$link_href}' AS link_href,'{$link_classes}' AS link_classes,'{$link_id}' AS link_id,'{$post_id}' AS post_id,'{$post_url}' AS post_url,'{$post_title}' AS post_title,'{$index_number}' AS index_number,'{$flag}' AS flag) AS tmp WHERE NOT EXISTS ( SELECT `post_id`, `link_href`, `link_text`, `index_number` FROM `$this->table_prev_links` WHERE `post_id` = {$post_id} AND `link_href`= '{$link_href}' AND `link_text` = '{$link_text}' AND `index_number` = {$index_number})");
				
				$update = $this->wpdb->query("UPDATE `$this->table_prev_links` SET `flag` = '{$flag}' WHERE `post_id` = {$post_id} AND `link_href`= '{$link_href}' AND `link_text` = '{$link_text}' AND `index_number` = {$index_number}");
			}

			$this->wpdb->query("DELETE FROM `$this->table_prev_links` WHERE `flag` != '{$flag}'");
		}
	}

	public function sendLinksToView()
	{
		$this->links_view = $this->wpdb->get_results( "SELECT * FROM `$this->table_prev_links`", ARRAY_A);
		$this->all_classes_of_links = $this->wpdb->get_results( "SELECT * FROM `$this->table_general_classes`", ARRAY_A);
	}

	public function isTableExists( $table_name )
	{
		$result = $this->wpdb->get_results( "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{$table_name}'", ARRAY_A)[0];
		if ( !isset($result) && !isset($result['TABLE_NAME']) ) {
		 	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			$sql_request = "CREATE TABLE " . $table_name . " (
				id int(11) NOT NULL AUTO_INCREMENT,
				link_text text NOT NULL,
				link_href text NOT NULL,
				link_classes text NOT NULL,
				link_id text NOT NULL,
				post_id int(11) NOT NULL,
				post_url text NOT NULL,
				post_title text NOT NULL,
				index_number int(11) NOT NULL,
				PRIMARY KEY(id))
				ENGINE = INNODB
				DEFAULT CHARACTER SET = utf8
				COLLATE = utf8_general_ci";
			$dbDelta = dbDelta($sql_request);	
				
			if ( isset( $dbDelta[$table_name] ) && $dbDelta[$table_name] == 'Created table '. $table_name ) {
				return true;
			}else{
				return false;
			}

		}else if( isset($result) && $result['TABLE_NAME'] == $table_name  ) {

			// $this->updateDataBase();
			// $this->wpdb->query("TRUNCATE TABLE $table_name");

			return true;
		}

		return true;
	}

	public function updateDataBase()
	{
	}

	public function getPosts(...$post_type)
	{
		$args = array(
			'post_type' => $post_type,
			'posts_per_page' => -1,
			'fields' => 'ids',
			'post_status' => 'publish',
			'post__not_in' => [30],
		);

		$posts = new WP_Query($args);

		return $posts;
	}

	public static function change_class_link()
	{
		global $wpdb;
		$table = $wpdb->prefix . 'alice_prev_links';
		$table_classes = $wpdb->prefix . 'alice_links_general_classes';
		
		$dom = new DOMDocument();
		$dom->formatOutput = true;

		$state = $_POST['state'];
		$post_id = $_POST['post_id'];
		$class_name = $_POST['class_name'];
		$link_text = $_POST['link_text'];
		$is_global = $_POST['global'];
		
		if ( $is_global == 'false' ) {
			$index_number = (int)$_POST['index'];

			// echo '<pre>'; var_dump('$index_number', $index_number); echo '</pre>'; die();

			$post = get_post( $post_id, ARRAY_A, 'edit');
			$content = $post['post_content'];
			$link_status = 0;
			if ( $state == 'true' )
				$link_status = 1;
			
			$dom->loadHTML('<?xml encoding="utf-8" ?>'.$content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
			// loadXML
			$classes_state = $wpdb->get_results("SELECT `link_classes` FROM `{$table}` WHERE {$post_id} = `post_id` AND `link_text` = '{$link_text}' AND `index_number` = {$index_number}", ARRAY_A)[0]['link_classes'];
			if ( !empty($classes_state) )
				$classes_state = maybe_unserialize($classes_state);
			
			$new_link_classes = array_map( function($elem) use ($class_name, $link_status){
				if ( $elem[0] == $class_name ) {
					$elem[1] = $link_status;
				}
				return $elem;
			}, $classes_state );
	
			$new_link_classes_ser = serialize($new_link_classes);
			$wpdb->query("UPDATE `{$table}` SET `link_classes` = '{$new_link_classes_ser}' WHERE {$post_id} = `post_id` AND `link_text` = '{$link_text}' AND `index_number` = {$index_number}");
	
			$links = $dom->getElementsByTagName('a');
			$count_links = $links->length - 1;
			
			$index_of_links = array();
			
			for ($i=0; $i <= $count_links; $i++) { 
				if ( $links->item( $i ) !== null ) {
					$node = $links->item( $i );
					$all_classes = explode(' ', $node->getAttribute('class'));

					// $index_of_links[$i] = $node->nodeValue;
					// if ( $index_of_links )
					if ( $node->nodeValue == $link_text ) {
						$index_of_links[] = $node->nodeValue;
						if ( count($index_of_links) == $index_number ) {
							if ( $link_status ) {
								if ( !in_array($class_name, $all_classes) ) {
									array_push($all_classes, $class_name );
								}
							}else{
								if ( ( $key = array_search($class_name, $all_classes) ) !== false ) {
									unset($all_classes[$key]);
								}
							}
						}
	
						$node->setAttribute('class', implode(' ', $all_classes));
					}
				}
			}
			
			// dirty fix
			foreach ($dom->childNodes as $item) {
			    if ($item->nodeType == XML_PI_NODE){
			        $dom->removeChild($item); // remove hack
			    }
			}
			$dom->encoding = 'UTF-8'; // insert proper

			// $new_content = $dom->saveHtml();
			$new_content = $dom->saveXML();
			$new_content = str_replace( array('<?xml encoding="utf-8" ?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"??>'), array('','',''), $new_content );
	
			$my_post = array();
			$my_post['ID'] = $post_id;
			$my_post['post_content'] = $new_content;
	
			$wp_update = wp_update_post( wp_slash($my_post) );

		}else{

			if ( $state == 'true' ){
				$state = 1;
			}else if( $state == 'false' ){
				$state = 0;
			}
		
			$count_posts = wp_count_posts( 'post', 'readable' )->publish;
			$count_pages = wp_count_posts( 'page', 'readable' )->publish;
			$count_all_posts = $count_posts + $count_pages;

			$offset = (int)$_POST['offset'];
			$posts_per_page = (int)$_POST['posts_per_page'];
			$continue = true;
			
			if ( $offset == 0 )			
				$wpdb->query("UPDATE `{$table_classes}` SET `class_status` = '{$state}' WHERE `class_name` = '{$class_name}'");

			$search = '%:"' . $class_name . '";%';	
			$found_links = $wpdb->get_results("SELECT `post_id`, `link_classes`, `index_number`, `link_text` FROM `{$table}` WHERE `link_classes` LIKE '{$search}' ORDER BY `post_id` ASC LIMIT {$posts_per_page} OFFSET {$offset}", ARRAY_A);
			
			foreach ($found_links as $link) {
				$found_links_sort[$link['post_id']][] = $link;
			}
			
			$proccesed_posts = array();
			foreach ( $found_links_sort as $post_id => $links ) {
				$post = get_post( $post_id, ARRAY_A, 'edit');
				$content = $post['post_content'];
				$dom->loadHTML('<?xml encoding="utf-8" ?>'.$content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
				$links_in_post = $dom->getElementsByTagName('a');
				$count_links = $links_in_post->length - 1;
				
				foreach ( $links as $key => $link ) {
					
					$link_text = $link['link_text'];
					$proccesed_posts[] = array($post_id, $link_text, $class_name);

					for ($i=0; $i <= $count_links; $i++) { 
						if ( $links_in_post->item( $i ) !== null ) {
							$node = $links_in_post->item( $i );
							$all_classes = explode(' ', $node->getAttribute('class'));
							if ( $node->nodeValue == $link_text ) {
								if ( $state ) {
									if ( !in_array($class_name, $all_classes) ) {
										array_push($all_classes, $class_name );
									}
								}else{
									if ( ( $key = array_search($class_name, $all_classes) ) !== false ) {
										unset($all_classes[$key]);
									}
								}
			
								$node->setAttribute('class', implode(' ', $all_classes));
							}
						}
					}

					$link_classes_arr = maybe_unserialize( $link['link_classes'] );
					array_walk( $link_classes_arr, function(&$item, $key) use ($class_name, $state){
						if ( in_array($class_name, $item) )
							$item[1] = $state;
					} );

					// var_dump($link_classes_arr);
					$link_classes_arr_ser = serialize($link_classes_arr);
					$wpdb->query("UPDATE `{$table}` SET `link_classes` = '{$link_classes_arr_ser}' WHERE {$post_id} = `post_id` AND `link_text` = '{$link_text}'");
				}

				// dirty fix
				foreach ($dom->childNodes as $item) {
				    if ($item->nodeType == XML_PI_NODE){
				        $dom->removeChild($item); // remove hack
				    }
				}
				$dom->encoding = 'UTF-8'; // insert proper

				$new_content = $dom->saveXML();
				$new_content = str_replace( array('<?xml encoding="utf-8" ?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"??>'), array('','',''), $new_content );

				$my_post = array();
				$my_post['ID'] = $post_id;
				$my_post['post_content'] = $new_content;
				// file_put_contents(__DIR__."/log.txt", "$post_id - " . var_export( $new_content, true) . "\n\n\n", FILE_APPEND );
				$wp_update = wp_update_post( wp_slash($my_post) );

			}	

			$count_posts = count($found_links);

			if ( $posts_per_page > $count_posts ) 
				$continue = false;

			echo json_encode(['continue_ajax' => $continue, 'count_posts' => $count_posts, 'posts' => $proccesed_posts, 'offset' => $offset]);
		}

		wp_die();
	}

	public static function add_class_link()
	{
		global $wpdb;

		$class_name = $_POST['class_name_to_add'];
		$post_id = $_POST['post_id'];
		$index_number = (int)$_POST['index'];
		$link_text = $_POST['link_text'];

		$table = $wpdb->prefix . 'alice_prev_links';
		$table_classes = $wpdb->prefix . 'alice_links_general_classes';
		
		$link_status = 1;

		$dom = new DOMDocument();
		$dom->formatOutput = true;

		if ( !empty($class_name) ) {

			$post = get_post( $post_id, ARRAY_A, 'edit');
			$content = $post['post_content'];
			$dom->loadHTML('<?xml encoding="utf-8" ?>'.$content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

			$classes_state = $wpdb->get_results("SELECT `link_classes` FROM `{$table}` WHERE {$post_id} = `post_id` AND `link_text` = '{$link_text}' AND `index_number` = {$index_number}", ARRAY_A)[0]['link_classes'];
			if ( !empty($classes_state) ) {
				$classes_state = maybe_unserialize($classes_state);
				$classes_state[] = [$class_name, $link_status];
			}
			
			$new_link_classes_ser = serialize($classes_state);
			$wpdb->query("UPDATE `{$table}` SET `link_classes` = '{$new_link_classes_ser}' WHERE {$post_id} = `post_id` AND `link_text` = '{$link_text}' AND `index_number` = {$index_number}");

			$links = $dom->getElementsByTagName('a');
			$count_links = $links->length - 1;
			
			$index_of_links = array();
			
			for ($i=0; $i <= $count_links; $i++) { 
				if ( $links->item( $i ) !== null ) {
					$node = $links->item( $i );
					$all_classes = explode(' ', $node->getAttribute('class'));

					if ( $node->nodeValue == $link_text ) {
						$index_of_links[] = $node->nodeValue;
						if ( count($index_of_links) == $index_number ) {
							if ( $link_status ) {
								if ( !in_array($class_name, $all_classes) ) {
									array_push($all_classes, $class_name );
								}
							}else{
								if ( ( $key = array_search($class_name, $all_classes) ) !== false ) {
									unset($all_classes[$key]);
								}
							}
						}
			
						$node->setAttribute('class', implode(' ', $all_classes));
					}
				}
			}
			
			// dirty fix
			foreach ($dom->childNodes as $item) {
			    if ($item->nodeType == XML_PI_NODE){
			        $dom->removeChild($item); // remove hack
			    }
			}
			$dom->encoding = 'UTF-8'; // insert proper

			// $new_content = $dom->saveHtml();
			$new_content = $dom->saveXML();
			$new_content = str_replace( array('<?xml encoding="utf-8" ?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"??>'), array('','',''), $new_content );
			
			$my_post = array();
			$my_post['ID'] = $post_id;
			$my_post['post_content'] = $new_content;
			// echo '<pre>'; var_dump($new_content); echo '</pre>';
			
			$wp_update = wp_update_post( wp_slash($my_post) );

			echo json_encode(['result' => 'done', 'post_id' => $post_id, 'class_name' => $class_name, 'index_number' => $index_number, 'link_text' => $link_text]);
		}else{
			echo json_encode(array('error'));
		}

		wp_die();
	}

	public static function add_class_link_global()
	{
		global $wpdb;
		$table = $wpdb->prefix . 'alice_prev_links';
		$table_classes = $wpdb->prefix . 'alice_links_general_classes';
		
		$dom = new DOMDocument();
		$dom->formatOutput = true;

		// $class_name_to_add = $_POST['class_name_to_add'];
		$class_name_to_add = 'alice_link';
		$for_class = $_POST['for_class'];

		if ( empty($class_name_to_add) || $class_name_to_add == $for_class){
			echo json_encode(array('error'));
			wp_die();
		}

		$count_posts = wp_count_posts( 'post', 'readable' )->publish;
		$count_pages = wp_count_posts( 'page', 'readable' )->publish;
		$count_all_posts = $count_posts + $count_pages;

		$offset = (int)$_POST['offset'];
		$posts_per_page = (int)$_POST['posts_per_page'];
		$continue = true;
		
		if ( $offset == 0 )			
			//$wpdb->query("UPDATE `{$table_classes}` SET `class_status` = 1 WHERE `class_name` = '{$class_name_to_add}'");
			$wpdb->query("INSERT INTO `{$table_classes}` (`class_status`, `class_name`)  SELECT * FROM  ( SELECT 1 AS one, '{$class_name_to_add}' AS two) AS tmp WHERE NOT EXISTS (SELECT `class_name` FROM `{$table_classes}` WHERE `class_name` = '{$class_name_to_add}'");

		// query("INSERT INTO `$table_cross_links` (`id_post_in`, `id_post_out`, `keyword`, `start`, `finish`, `context`, `status`, `state`, `date_post`, `token`) SELECT * FROM (SELECT {$id} AS one, {$post_id} AS two, '{$found_keyword}' AS three, {$start} AS four, {$finish} AS five, '{$result}' AS six, 0 AS seven, 0 AS eight, '{$date_post}' AS nine, '{$token}' AS ten) AS tmp WHERE NOT EXISTS ( SELECT `id_post_in`, `id_post_out`, `keyword` FROM `$table_cross_links` WHERE `id_post_in` = {$id} AND `id_post_out`= {$post_id} AND `keyword` = '{$found_keyword}')");

		$search = '%:"' . $for_class . '";%';	
		// $found_links = $wpdb->get_results("SELECT `post_id`, `link_classes`, `index_number`, `link_text` FROM `{$table}` WHERE `link_classes` LIKE '{$search}' ORDER BY `post_id` ASC LIMIT {$posts_per_page} OFFSET {$offset}", ARRAY_A);
		$found_links = $wpdb->get_results("SELECT `post_id`, `link_classes`, `index_number`, `link_text` FROM `{$table}` ORDER BY `post_id` ASC LIMIT {$posts_per_page} OFFSET {$offset}", ARRAY_A);
		
		foreach ($found_links as $link) {
			$found_links_sort[$link['post_id']][] = $link;
		}
		
		$proccesed_posts = array();
		foreach ( $found_links_sort as $post_id => $links ) {
			$post = get_post( $post_id, ARRAY_A, 'edit');
			$content = $post['post_content'];
			$dom->loadHTML('<?xml encoding="utf-8" ?>'.$content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
			$links_in_post = $dom->getElementsByTagName('a');
			$count_links = $links_in_post->length - 1;
			
			foreach ( $links as $key => $link ) {
				
				$link_text = $link['link_text'];
				$post_res = array($post_id, $link_text, $class_name_to_add, false);
				$added = false;

				for ($i=0; $i <= $count_links; $i++) { 
					if ( $links_in_post->item( $i ) !== null ) {
						$node = $links_in_post->item( $i );
						$all_classes = explode(' ', $node->getAttribute('class'));
						if ( $node->nodeValue == $link_text ) {
							if ( !in_array($class_name_to_add, $all_classes) ) {
								$post_res[3] = true;
								$added = true;
								array_push($all_classes, $class_name_to_add );
							}
							
							$node->setAttribute('class', implode(' ', $all_classes));
						}
					}
				}
				
				$proccesed_posts[] = $post_res;

				if ( $added === true ) {
					$link_classes_arr = maybe_unserialize( $link['link_classes'] );
					$link_classes_arr[] = [$class_name_to_add, 1];
					$link_classes_arr_ser = serialize($link_classes_arr);
					$wpdb->query("UPDATE `{$table}` SET `link_classes` = '{$link_classes_arr_ser}' WHERE {$post_id} = `post_id` AND `link_text` = '{$link_text}'");
				}

			}

			// dirty fix
			foreach ($dom->childNodes as $item) {
			    if ($item->nodeType == XML_PI_NODE){
			        $dom->removeChild($item); // remove hack
			    }
			}
			$dom->encoding = 'UTF-8'; // insert proper

			$new_content = $dom->saveXML();
			$new_content = str_replace( array('<?xml encoding="utf-8" ?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"??>'), array('','',''), $new_content );

			$my_post = array();
			$my_post['ID'] = $post_id;
			$my_post['post_content'] = $new_content;
			// file_put_contents(__DIR__."/log.txt", "$post_id - " . var_export( $new_content, true) . "\n\n\n", FILE_APPEND );
			$wp_update = wp_update_post( wp_slash($my_post) );
		}	

		$count_posts = count($found_links);

		if ( $posts_per_page > $count_posts ) 
			$continue = false;

		echo json_encode(['continue_ajax' => $continue, 'count_posts' => $count_posts, 'posts' => $proccesed_posts, 'offset' => $offset]);

		wp_die();
	}

	public function disableLinkClass()
	{
	}

	public function showTable( $links, $general_classes )
	{
		require_once 'templates/import.php';
	}

	public function showErrorMessage()
	{
		require_once 'templates/errors/import-error.php';
	}
}