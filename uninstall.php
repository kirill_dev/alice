<?php 
if ( ! defined('WP_UNINSTALL_PLUGIN') ) exit;
global $wpdb;

$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_recent_posts_settings" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_clicks_count_sort" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_click_stat" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_connections_from_server" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_cross_links" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_filter_settings" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_page_stats" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}alice_plugin_menu" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}recent_posts_settings" );

delete_option('alice_access_token');
delete_option('custom_css_class');
delete_option('link_size');
delete_option('color1');
delete_option('enable_preview');
delete_option('preview_border_color');
delete_option('preview_bg_color');
delete_option('preview_font_size');
delete_option('enable_preview');
delete_option('preview_border_color');
delete_option('preview_font_color');
delete_option('preview_img_size');